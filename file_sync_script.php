<?php

require_once("php-file-sync/file_synchronizer.php");

$aSettings = 'Another Test';
$aSettings = array();
$aSettings["debug_mode"] = true;
$aSettings["simulate"]  = false;
$aSettings["skip_hidden"] = true;
$aSettings["use_checksum"] = true;
$aSettings["path_a"] = "application";
$aSettings["path_b"] = "testserver/application";

$oFileSynchronizer = new File_Synchronizer($aSettings);

try
{
    // Start the synchronization
    $oFileSynchronizer->start_sync();
}
catch(Synchronization_Exception $oSyncException)
{
echo '<pre>';    
print_r($oSyncException);
echo '</pre>';
}

?>