<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Utils Helper
 */

function preg_replace_nth($pattern, $replacement, $subject, $nth=1) {
	return preg_replace_callback($pattern,
		function($found) use (&$pattern, &$replacement, &$nth) {
			$nth--;
			if ($nth >= 0) return preg_replace($pattern, $replacement, reset($found) );
			return reset($found);
		}, $subject,$nth  );
}

/**
 * Php in_array recursive
 * @recursively check if a value is in array
 * @param string $string (needle)
 * @param array $array (haystack)
 * @param bool $type (optional)
 * @return bool
 */
function in_array_recursive($string, $array, $type=false)
{
    /*** an recursive iterator object ***/
    $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($array));

    /*** traverse the $iterator object ***/
    while($it->valid())
    {
        /*** check for a match ***/
        if( $type === false )
        {
            if( $it->current() == $string )
            {
                return true;
            }
        }
        else
        {
            if( $it->current() === $string )
            {
                return true;
            }
        }
        $it->next();
    }
    /*** if no match is found ***/
    return false;
}