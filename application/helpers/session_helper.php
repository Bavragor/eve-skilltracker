<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Session Helper
 */

function setUserDataFromView($sName, $sValue)
{
	$CI =& get_instance();

	if($CI->session->set_userdata($sName, $sValue))
	{
		return true;
	}
	else
	{
		return false;
	}
}