<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User Helper
 */

function getLastLogin($sUserID)
{
	$CI =& get_instance();

	$CI->load->model('usermodel');

	$aUser = $CI->usermodel->getUser($sUserID);

	return date('d.m.Y G:i', $aUser['lastLogin']);
}

function isAllowed2View($sCharacterID)
{
	$CI =& get_instance();

	$aCurCharacterData = $CI->session->all_userdata();

	$CI->load->model('usermodel');

    $oUser2View = $CI->usermodel->getCharacterAdmin($sCharacterID);

	$oUserPrivacy = $CI->usermodel->getUserPrivacy($sCharacterID);

	$aPrivacyFields = $CI->usermodel->getPrivacyFields();


    foreach($aPrivacyFields as $sPrivacyOption)
    {
        switch($sPrivacyOption)
        {
            case 'A':
                if($oUserPrivacy->$sPrivacyOption)
                {
                    return false;
                }
                break;
            case 'U':
                if($oUserPrivacy->$sPrivacyOption)
                {
                    return true;
                }
                break;
            case 'X':

                if(($aCurCharacterData['corporationID'] == $oUser2View->corporationID ) && $oUserPrivacy->$sPrivacyOption)
                {
                    return true;
                }
                elseif(!empty($aCurCharacterData['allianceID']) && !empty($oUser2View->allianceID) && $aCurCharacterData['allianceID'] == $oUser2View->allianceID)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                break;
            case 'C':
                if($this->usermodel->hasUserRole($aCurCharacterData['characterID'], 'CEO') && $oUserPrivacy->$sPrivacyOption)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                break;
            case 'R':
                if($this->usermodel->hasUserRole($aCurCharacterData['characterID'], 'RECRUITER') && $oUserPrivacy->$sPrivacyOption)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                break;
            case 'I':
                if($this->usermodel->hasUserRole($aCurCharacterData['characterID'], 'INSTRUCTOR') && $oUserPrivacy->$sPrivacyOption)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                break;

        }
    }


}

function getPrivacy($oCharacter, $sPrivacyType)
{
    $CI =& get_instance();

    $CI->load->model('usermodel');

    $oUserPrivacy = $CI->usermodel->getUserPrivacy($oCharacter->characterID);

    return $oUserPrivacy->$sPrivacyType;
}