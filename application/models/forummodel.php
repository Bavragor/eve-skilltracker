<?php

class Forummodel extends CI_Model {

    function addTopic($userID, $topic) {
        $sql = "INSERT INTO st_topics (topicID, topic, started, userID,sticky) VALUES (NULL,?,?,?,0)";
        $this->db->query($sql, array($topic, time(), $userID));
        return $this->db->insert_id();
    }

    function addComment($userID, $topicID, $text) {
        $sql = "INSERT INTO st_comments (commentID, topicID,userID, date,text) VALUES (NULL,?,?,?,?)";
        $this->db->query($sql, array($topicID, $userID, time(), $text));
        return $this->db->insert_id();
    }

    function stickyTopic($topicID) {
        $sql = "UPDATE st_topics SET sticky = 1 - sticky WHERE topicID = ?";
        $this->db->query($sql, $topicID);
        return $this->db->affected_rows();
    }

    function deleteTopic($topicID) {
        $sql = "DELETE FROM st_topics WHERE topicID = ?";
        $this->db->query($sql, $topicID);
        $sql = "DELETE FROM st_comments WHERE topicID = ?";
        $this->db->query($sql, $topicID);
    }

    function deleteComment($commentID) {
        $sql = "DELETE FROM st_comments WHERE commentID = ?";
        $this->db->query($sql, $commentID);
    }

    function getTopics() {
        $sql = "SELECT u.userID, ch.name AS characterName, u.mainCharacter,t.topicID, t.started, t.topic, t.sticky, COUNT(c.commentID) AS count FROM st_topics AS t INNER JOIN st_users AS u ON (u.userID = t.userID) INNER JOIN st_characters AS ch ON (u.mainCharacter = ch.characterID) INNER JOIN st_comments AS c ON (c.topicID = t.topicID) GROUP BY c.topicID ORDER BY sticky DESC, MAX(c.date) DESC";
        return $this->db->query($sql)->result();
    }

    function getLastPost($topicID) {
        $sql = "SELECT c.commentID, u.userID, u.mainCharacter, ch.name AS characterName, c.date FROM st_comments AS c INNER JOIN st_users AS u USING (userID) INNER JOIN st_characters AS ch ON (u.mainCharacter = ch.characterID) WHERE c.topicID = ? ORDER BY c.date DESC LIMIT 1";
        return $this->db->query($sql, $topicID)->row();
    }

    function getTopic($topicID) {
        $sql = "SELECT topicID, topic, sticky FROM st_topics WHERE topicID = ?";
        return $this->db->query($sql, $topicID)->row();
    }

    public function getTopicName($topicID)
    {
        $sql = "SELECT topic FROM st_topics WHERE topicID = ?";
        return $this->db->query($sql, $topicID)->row()->topic;
    }

    function getComments($topicID) {
        $sql = "SELECT c.commentID, c.text, c.date, c.userID, ch.name AS characterName,u.mainCharacter FROM st_comments AS c INNER JOIN st_users AS u ON (u.userID = c.userID) INNER JOIN st_characters AS ch ON (ch.characterID = u.mainCharacter) WHERE c.topicID = ? ORDER BY date ASC";
        return $this->db->query($sql, $topicID)->result();
    }

	/**
	 * Liefert die Kommentare eines einzelnen Badges
	 * @param $badgeID
	 * @return mixed
	 * @author Kevin Mauel <kevin.mauel2@gmail.com>
	 */
	public function getCommentsByBadge($badgeID)
	{
		$sql = "SELECT c.commentID, c.text, c.date, c.userID, ch.name AS characterName,u.mainCharacter FROM st_comments AS c INNER JOIN st_users AS u ON (u.userID = c.userID) INNER JOIN st_characters AS ch ON (ch.characterID = u.mainCharacter) WHERE c.badgeID = ? ORDER BY date ASC";
		return $this->db->query($sql, $badgeID)->result();
	}

	/**
	 * Kommentar zu einem Badge hinzufuegen
	 * @param $userID
	 * @param $badgeID
	 * @param $text
	 * @return mixed
	 * @author Kevin Mauel <kevin.mauel2@gmail.com>
	 */
	function addCommentForBadge($userID, $badgeID, $text) {
		$sql = "INSERT INTO st_comments (commentID, badgeID,userID, date,text) VALUES (NULL,?,?,?,?)";
		$this->db->query($sql, array($badgeID, $userID, time(), $text));
		return $this->db->insert_id();
	}

	/**
	 * Liefert die letzten Comments, standardmäßig der letzen 7 Tage
	 * @param int $sTimeInDays
	 * @param bool $blBadgesOnly
	 * @return mixed
	 * @author Kevin Mauel <kevin.mauel2@gmail.com>
	 */
	public function getLastComments($sTimeInDays = 7, $blBadgesOnly = true)
	{
		if($blBadgesOnly)
		{
			$sSql = 'SELECT a.*, b.mainCharacter, c.name as characterName FROM st_comments as a LEFT JOIN st_users as b ON(b.admin != 1 AND a.userID = b.userID) INNER JOIN st_characters AS c ON (c.characterID = b.mainCharacter) WHERE a.badgeID != "" AND from_unixtime(a.date) > date_sub(now(), interval ? DAY) ORDER BY a.date DESC';
		}
		else
		{
			$sSql = 'SELECT a.*, b.mainCharacter, c.name as characterName FROM st_comments as a LEFT JOIN st_users as b ON(b.admin != 1 AND a.userID = b.userID) INNER JOIN st_characters AS c ON (c.characterID = b.mainCharacter) WHERE from_unixtime(a.date) > date_sub(now(), interval ? DAY) ORDER BY a.date DESC';
		}

		$oQuery = $this->db->query($sSql, $sTimeInDays);

		return $oQuery->result();
	}

}