<?php

class Apimodel extends CI_Model {

    function getAPIKeyInfo($keyID, $vCode) {
        $returnClass = new stdClass();
        require_once('vendor/autoload.php');

        Pheal\Core\Config::getInstance()->cache = new \Pheal\Cache\FileStorage('application/cache/');

	    Pheal\Core\Config::getInstance()->access = new \Pheal\Access\StaticCheck();

        $pheal = new Pheal\Pheal($keyID, $vCode, 'account');

        $result = $pheal->APIKeyInfo();
        $returnClass->accessMask = $result->key->accessMask;
        $returnClass->expires = $result->key->expires;
        $returnClass->type = $result->key->type;
        $returnClass->characters = $result->key->characters;

        return $returnClass;
    }

    function updateCharacter($keyID, $vCode, $characterID) {
        require_once('vendor/autoload.php');
        try {
	        Pheal\Core\Config::getInstance()->cache = new \Pheal\Cache\FileStorage('application/cache/');

	        Pheal\Core\Config::getInstance()->access = new \Pheal\Access\StaticCheck();

            $pheal = new Pheal\Pheal($keyID, $vCode, 'char');

            $charSheet = $pheal->CharacterSheet(array('characterID' => $characterID));

            $update = array(
                'allianceID' => $charSheet->allianceID,
                'allianceName' => $charSheet->allianceName,
                'ancestry' => $charSheet->ancestry,
                'balance' => $charSheet->balance,
                'bloodLine' => $charSheet->bloodLine,
                'cloneName' => $charSheet->cloneName,
                'cloneSkillPoints' => $charSheet->cloneSkillPoints,
                'corporationID' => $charSheet->corporationID,
                'corporationName' => $charSheet->corporationName,
                'DoB' => $charSheet->DoB,
                'gender' => $charSheet->gender,
                'name' => $charSheet->name,
                'race' => $charSheet->race
            );

            $this->db->where('characterID', $characterID);
            $this->db->update('st_characters', $update);

            //$sql1 = "DELETE FROM st_characterskills WHERE ownerID = ?";
            //$this->db->query($sql1, $characterID);

            $aChangedorNewSkills = array();
            foreach ($charSheet->skills as $s) {
                $aInsert = array('level' => $s->level,
                    'ownerID' => $characterID,
                    'skillpoints' => $s->skillpoints,
                    'typeID' => $s->typeID,
                    'published' => $s->published
                );
                $sInsert = $this->db->insert_string('st_characterskills', $aInsert).' ON DUPLICATE KEY UPDATE skillpoints = IF(VALUES(skillpoints) > skillpoints, VALUES(skillpoints), skillpoints), level = IF(VALUES(level) > level, VALUES(level), level)';
                $this->db->query($sInsert);

                $aChangedorNewSkills[$characterID][$s->typeID] = $this->db->affected_rows();

            }


            $sit = $pheal->SkillInTraining(array('characterID' => $characterID));
            $sql2 = "DELETE FROM st_skillintraining WHERE ownerID = ?";
            $this->db->query($sql2, $characterID);
            if ($sit->skillInTraining == 0) {
                $insert = array(
                    'ownerID' => $characterID,
                    'currentTQTime' => 0,
                    'trainingEndTime' => 0,
                    'trainingStartTime' => 0,
                    'trainingTypeID' => 0,
                    'trainingStartSP' => 0,
                    'trainingDestinationSP' => 0,
                    'trainingToLevel' => 0,
                    'skillInTraining' => $sit->skillInTraining
                );
            } else {
                $insert = array(
                    'ownerID' => $characterID,
                    'currentTQTime' => $sit->currentTQTime->_value,
                    'trainingEndTime' => $sit->trainingEndTime,
                    'trainingStartTime' => $sit->trainingStartTime,
                    'trainingTypeID' => $sit->trainingTypeID,
                    'trainingStartSP' => $sit->trainingStartSP,
                    'trainingDestinationSP' => $sit->trainingDestinationSP,
                    'trainingToLevel' => $sit->trainingToLevel,
                    'skillInTraining' => $sit->skillInTraining,
                );
            }
            $this->db->insert('st_skillintraining', $insert);
        } catch (Pheal\Exceptions\APIException $ex) {
            return 'API-Error ' . $ex->code . ': ' . $ex->getMessage() . ' <br>CachedUntil: ' . $ex->cached_until;
        } catch (Pheal\Exceptions\PhealException $ex) {
            return 'Exception: ' . get_class($ex) . ' Error: ' . $ex->getMessage();
        }
        return $aChangedorNewSkills;
    }

	/**
	 * Function to retrieve public CorpSheet
	 * @param $sCorpID
	 * @return array|int|string
	 * @author Kevin Mauel <kevin.mauel@twt.de>
	 */
	public function getCorpInfo($sCorpID)
	{
		require_once('vendor/autoload.php');
		try {
			Pheal\Core\Config::getInstance()->cache = new \Pheal\Cache\FileStorage('application/cache/');

			Pheal\Core\Config::getInstance()->access = new \Pheal\Access\StaticCheck();

			$pheal = new Pheal\Pheal('', '', 'corp');

			$oCorpSheet = $pheal->CorporationSheet(array('corporationID' => $sCorpID));

			$aResAssignments = array(
				'corporationID' => $oCorpSheet->corporationID,
				'corporationName' => $oCorpSheet->corporationName,
				'ticker'    => $oCorpSheet->ticker,
				'ceoID' => $oCorpSheet->ceoID,
				'ceoName' => $oCorpSheet->ceoName,
				'description' => $oCorpSheet->description,
				'allianceID' => $oCorpSheet->allianceID,
				'allianceName' => $oCorpSheet->allianceName,
				'memberCount' => $oCorpSheet->memberCount
			);

			return $aResAssignments;

		} catch (Pheal\Exceptions\APIException $ex) {
			return 'API-Error ' . $ex->code . ': ' . $ex->getMessage() . ' <br>CachedUntil: ' . $ex->cached_until;
		} catch (Pheal\Exceptions\PhealException $ex) {
			return 'Exception: ' . get_class($ex) . ' Error: ' . $ex->getMessage();
		}
		return 1;
	}

}

?>