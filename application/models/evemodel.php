<?php

class Evemodel extends CI_Model {
    
    public function typeID($typeName) {
        $sql = "SELECT t.typeID FROM eve_invtypes AS t INNER JOIN eve_invgroups AS g USING (groupID) WHERE g.categoryID = 16 AND typeName LIKE ?";
        $query = $this->db->query($sql, $typeName);
        return $query->row()->typeID;
    }
    
    public function ships() {
        $sql = "SELECT t.typeID,t.typeName FROM eve_invtypes AS t INNER JOIN eve_invgroups AS g USING (groupID) WHERE g.categoryID = 6 AND t.published = 1 ORDER BY t.typeName;";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getSkillInfo($sTypeID)
    {
        $sSql = 'SELECT * FROM eve_invtypes WHERE typeID = ?';
        $oQuery = $this->db->query($sSql, $sTypeID);
        return $oQuery->row();
    }
    
}
?>
