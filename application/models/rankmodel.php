<?php

class Rankmodel extends CI_Model {
    public function getRanks() {
        $sql = "SELECT rankID, rankName FROM st_ranks;";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function saveRank($rn) {
        $sql = "INSERT INTO st_ranks (rankID, rankName) VALUES (NULL, ?);";
        $this->db->query($sql,$rn);
        return $this->db->affected_rows();
    }
    public function deleteRank($rid) {
        $sql = "DELETE FROM st_ranks WHERE rankID = ?";
        $this->db->query($sql,$rid);
        return $this->db->affected_rows();
    }
}
?>
