<?php

class Usermodel extends CI_Model {

	var $aUserRoles = array(
		0 => 'CEO',
		1 => 'Recruiter',
		2 => 'Instructor'
	);

    function getLoginMatch($username, $password) {
        $sql = "SELECT u.userID, u.username, u.password, u.admin, u.lastCharacter, u.mainCharacter FROM st_users AS u WHERE username LIKE ? AND password LIKE SHA(?)";
        $query = $this->db->query($sql, array($username, $password));
        return $query;
    }

    function getCharacter($userID, $characterID) {
        $sql = "SELECT uk.userID, c.name as characterName,c.race, c.corporationID, c.corporationName,k.cachedUntil, s.trainingToLevel,s.skillInTraining,s.trainingEndTime,s.trainingTypeID,t.typeName,k.keyID,k.valid,k.lastError,k.accessMask FROM st_characters AS c INNER JOIN st_skillintraining AS s ON (s.ownerID = c.characterID) INNER JOIN eve_invtypes AS t ON (t.typeID = s.trainingTypeID) INNER JOIN st_keyscharacters as b ON (b.characterID = c.characterID) INNER JOIN st_userskeys AS uk ON (uk.keyID = b.keyID AND uk.userID  = ?) INNER JOIN st_keys AS k ON (k.keyID = uk.keyID) WHERE c.characterID = ?";
        $query = $this->db->query($sql, array($userID, $characterID));
        if ($query->num_rows > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getCharacterAdmin($characterID) {
        $sql = "SELECT c.name as characterName,c.race, c.corporationID, c.corporationName,k.cachedUntil, s.trainingToLevel,s.skillInTraining,s.trainingEndTime,s.trainingTypeID,t.typeName,k.keyID,k.valid,k.lastError,k.accessMask FROM st_characters AS c INNER JOIN st_skillintraining AS s ON (s.ownerID = c.characterID) INNER JOIN eve_invtypes AS t ON (t.typeID = s.trainingTypeID) INNER JOIN st_keyscharacters as b ON (b.characterID = c.characterID) INNER JOIN st_keys AS k ON (k.keyID = b.keyID) WHERE c.characterID = ?";
        $query = $this->db->query($sql, $characterID);
        if ($query->num_rows > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

	function getCharacterID($sCharacterName)
	{
		$sCharacterName = urldecode($sCharacterName);
		$sql = "SELECT c.characterID FROM st_characters AS c INNER JOIN st_skillintraining AS s ON (s.ownerID = c.characterID) INNER JOIN eve_invtypes AS t ON (t.typeID = s.trainingTypeID) INNER JOIN st_keyscharacters as b ON (b.characterID = c.characterID) INNER JOIN st_keys AS k ON (k.keyID = b.keyID) WHERE c.name = ?";
		$query = $this->db->query($sql, $sCharacterName);
		if ($query->num_rows > 0) {
			return $query->row()->characterID;
		} else {
			return false;
		}
	}

    function updateLogin($userID) {
        $sql = "UPDATE st_users SET lastLogin = ? WHERE userID = ?";
        $this->db->query($sql, array(time(), $userID));
    }

    function updateLastCharacter($userID, $characterID) {
        $sql = "UPDATE st_users SET lastCharacter = ? WHERE userID = ?";
        $this->db->query($sql, array($characterID, $userID));
    }

    function addUser($username, $password, $characterID) {
        $sql = "INSERT INTO st_users (userID,username,password,admin,creation,lastLogin,lastCharacter,mainCharacter) VALUES (NULL,?,SHA(?),0,?,?,?,?)";
        $this->db->query($sql, array($username, $password, time(), time(), $characterID, $characterID));
    }

    function addKey($userID, $keyID, $vCode, $accessMask, $expires, $type, $cachedUntil) {
        $sql = "INSERT INTO st_keys (keyID, accessMask, expires, type, vCode, cachedUntil, valid, lastError) VALUES (?,?,?,?,?,?,1,'') ON DUPLICATE KEY UPDATE cachedUntil = ?, valid = 1";
        $this->db->query($sql, array($keyID, $accessMask, $expires, $type, $vCode, $cachedUntil, $cachedUntil));
        $sql = "INSERT INTO st_userskeys (userID, keyID) VALUES (?,?)";
        $this->db->query($sql, array($userID, $keyID));
    }

    function updateKey($keyID, $accessMask, $expires, $type, $cachedUntil, $valid, $lastError) {
        $update = array('accessMask' => $accessMask,
            'accessMask' => $accessMask,
            'expires' => $expires,
            'type' => $type,
            'cachedUntil' => $cachedUntil,
            'valid' => $valid,
            'lastError' => $lastError);
        $this->db->where('keyID', $keyID);
        $this->db->update('st_keys', $update);
    }

    function addRawCharacter($keyID, $characterID) {
        $sql = "INSERT INTO st_characters (characterID) VALUES (?) ON DUPLICATE KEY UPDATE characterID = ?";
        $this->db->query($sql, array($characterID, $characterID));
        $sql = "INSERT INTO st_characterprivacy SET characterID = ? ON DUPLICATE KEY UPDATE characterID = ?";
        $this->db->query($sql, array($characterID, $characterID));
        $sql = "INSERT INTO st_keyscharacters (keyID, characterID) VALUES (?,?) ON DUPLICATE KEY UPDATE keyID = ?";
        $this->db->query($sql, array($keyID, $characterID, $keyID));
    }

    function getKeys($userID) {
        $sql = "SELECT userID, keyID,accessMask, type, expires, valid, lastError FROM st_userskeys INNER JOIN st_keys USING (keyID) WHERE userID = ?";
        $query = $this->db->query($sql, $userID);
        return $query->result();
    }
    
    function getKey($keyID) {
        $sql = "SELECT keyID, vCode,accessMask,type,expires,valid,lastError FROM st_keys WHERE keyID = ?";
        $query = $this->db->query($sql,$keyID);
        return $query->row();
    }

    function deleteAccount($userID) {
        $keys = $this->getKeys($userID);
        $this->db->trans_start();
        foreach ($keys as $k) {
            $this->deleteKey($k->keyID);
        }
        $sql = "DELETE FROM st_users WHERE userID = ?";
        $this->db->query($sql, $userID);

        $this->db->trans_complete();
    }

    function deleteKey($keyID) {
        $sql = "SELECT characterID, (SELECT COUNT(*) FROM st_keyscharacters WHERE characterID = k.characterID) AS c FROM st_keyscharacters AS k WHERE keyID = ?";
        $characters = $this->db->query($sql, $keyID)->result();

        foreach ($characters as $c) {
            if ($c->c > 1) {
                // If the character has multiple keys associated to it, don't delete it.
                continue;
            }
            $sql = "DELETE FROM st_characterssummary WHERE characterID = ?";
            $this->db->query($sql, $c->characterID);
            $sql = "DELETE FROM st_badgeskillpoints WHERE characterID = ?";
            $this->db->query($sql, $c->characterID);
            $sql = "DELETE FROM st_characters WHERE characterID = ?";
            $this->db->query($sql, $c->characterID);
            $sql = "DELETE FROM st_characterskills WHERE ownerID = ?";
            $this->db->query($sql, $c->characterID);
            $sql = "DELETE FROM st_skillintraining WHERE ownerID = ?";
            $this->db->query($sql, $c->characterID);
        }
        $sql = "DELETE FROM st_userskeys WHERE keyID = ?";
        $this->db->query($sql, $keyID);
        $sql = "SELECT COUNT(userID) AS c FROM st_userskeys GROUP BY keyID HAVING keyID = ?";
        $query = $this->db->query($sql, $keyID);
        if ($query->num_rows() == 0 || $query->row()->c <= 1) {
            $sql = "DELETE FROM st_keys WHERE keyID = ? ";
            $this->db->query($sql, $keyID);
            $sql = "DELETE FROM st_keyscharacters WHERE keyID = ?";
            $this->db->query($sql, $keyID);
        }
    }

    function getCharacters($userID) {
        $sql = "SELECT userID, k.keyID,c.characterID,name AS characterName,c.corporationID, c.corporationName,c.anonymous FROM st_characters AS c INNER JOIN st_keyscharacters AS kc ON (c.characterID = kc.characterID) INNER JOIN st_userskeys AS k ON (k.keyID = kc.keyID) WHERE k.userID = ? ORDER BY characterName ASC ";
        $query = $this->db->query($sql, $userID);
        return $query->result();
    }

    function getCharactersForKey($keyID) {
        $sql = "SELECT k.keyID,c.characterID,c.name AS characterName,c.corporationID, c.corporationName,c.anonymous FROM st_keyscharacters AS k INNER JOIN st_characters AS c ON (k.characterID = c.characterID) WHERE keyID = ?";
        $query = $this->db->query($sql, $keyID);
        return $query->result();
    }

    function getCharacterSummary($characterID) {
        $sql = "SELECT basicBadges, mediumBadges, expertBadges, skillCount, skillpoints, skillFiveCount, badgeRankPoints, skillRankPoints, skilltrackerPoints FROM st_characterssummary WHERE characterID = ?";
        $query = $this->db->query($sql, $characterID);
        return $query->row();
    }

    function getSkills($characterID) {
        $sql = "SELECT s.typeID,t.typeName, ss.trainingTimeMultiplier, s.level, t.groupID, g.groupName,SUM(s.skillpoints) AS sum,COUNT(*) AS count FROM st_characterskills AS s INNER JOIN eve_invtypes AS t ON (t.typeID = s.typeID) INNER JOIN eve_invgroups AS g ON (g.groupID = t.groupID) INNER JOIN st_skills AS ss ON (ss.typeID = s.typeID) WHERE s.ownerID = ? GROUP BY groupName,typeName WITH ROLLUP";
        $query = $this->db->query($sql, $characterID);
        return $query->result();
    }

	function getSkillCount($characterID)
	{
		$sql = "SELECT DISTINCT COUNT(s.typeID) as skillCount FROM st_characterskills AS s INNER JOIN eve_invtypes AS t ON (t.typeID = s.typeID) INNER JOIN eve_invgroups AS g ON (g.groupID = t.groupID) INNER JOIN st_skills AS ss ON (ss.typeID = s.typeID) WHERE s.ownerID = ? GROUP BY s.ownerID WITH ROLLUP";
		$query = $this->db->query($sql, $characterID);
		return $query->row()->skillCount;
	}

	function getTotalSkillpoints($characterID)
	{
		$sql = "SELECT DISTINCT SUM(s.skillpoints) AS totalSkillpoints FROM st_characterskills AS s INNER JOIN eve_invtypes AS t ON (t.typeID = s.typeID) INNER JOIN eve_invgroups AS g ON (g.groupID = t.groupID) INNER JOIN st_skills AS ss ON (ss.typeID = s.typeID) WHERE s.ownerID = ? GROUP BY s.ownerID WITH ROLLUP";
		$query = $this->db->query($sql, $characterID);
		return $query->row()->totalSkillpoints;
	}

    function getCharacterRank($field, $value) {
        $sql = "SELECT COUNT(characterID) AS count FROM st_characterssummary INNER JOIN st_characters AS c USING (characterID) WHERE $field > ?";
        $query = $this->db->query($sql, $value);
        return $query->row()->count + 1;
    }

    function getRankTable($field) {
        $sql = "SELECT c.characterID, c.name AS characterName, c.anonymous, s.$field FROM st_characterssummary AS s INNER JOIN st_characters AS c USING (characterID) ORDER BY s.$field DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getCharacterCount() {
        $sql = "SELECT COUNT(*) AS c FROM st_characters";
        $query = $this->db->query($sql);
        return $query->row()->c;
    }

    public function setMainCharacter($userID, $characterID) {
        $sql = "UPDATE st_users SET mainCharacter = ? WHERE userID = ?";
        $this->db->query($sql, array($characterID, $userID));
        return $this->db->affected_rows();
    }

    /**
     * @deprecated
     * @see Usermodel::setUserPrivacy
     */
    public function setCharacterAnonymous($characterID) {
        $sql = "UPDATE st_characters SET anonymous = NOT anonymous WHERE characterID = ?";
        $this->db->query($sql, $characterID);
        return $this->db->affected_rows();
    }

    function updateCharacterSummary($characterID) {
        $sql = "INSERT INTO st_characterssummary (characterID) VALUES (?) ON DUPLICATE KEY UPDATE characterID = characterID";
        $this->db->query($sql, $characterID);

        $sql = "UPDATE st_characterssummary AS s SET s.basicBadges = (SELECT COUNT(*) FROM st_badgeskillpoints WHERE characterID = s.characterID AND rankID >= 1), s.mediumBadges = (SELECT COUNT(*) FROM st_badgeskillpoints WHERE characterID = s.characterID AND rankID >= 2),s.expertBadges = (SELECT COUNT(*) FROM st_badgeskillpoints WHERE characterID = s.characterID AND rankID >= 3), skillCount = (SELECT COUNT(*) FROM st_characterskills WHERE ownerID = s.characterID), skillpoints = (SELECT SUM(skillpoints) FROM st_characterskills WHERE ownerID = s.characterID), skillFiveCount = (SELECT COUNT(*) FROM st_characterskills WHERE level = 5 AND ownerID = s.characterID) WHERE s.characterID = ?";
        $this->db->query($sql, $characterID);

        $sql = "UPDATE st_characterssummary SET badgeRankPoints = basicBadges + mediumBadges * 3 + expertBadges * 8, skillRankPoints = skillCount + skillFiveCount * 10 + (skillpoints / 1000000) * 5 WHERE characterID = ?";
        $this->db->query($sql, $characterID);

        $sql = "UPDATE st_characterssummary SET skilltrackerPoints = badgeRankPoints + skillRankPoints WHERE characterID = ?;";
        $this->db->query($sql, $characterID);
    }

    function getAllCharacters() {
        $sql = "SELECT characterID FROM st_characters;";
        return $this->db->query($sql)->result();
    }

    function getUsers($offset, $limit, $fieldShort, $orderShort) {
        $fieldLookup['cr'] = 'creation';
        $fieldLookup['u'] = 'username';
        $fieldLookup['l'] = 'lastLogin';
        $fieldLookup['cc'] = 'count';
        $orderLookup['d'] = 'DESC';
        $orderLookup['a'] = 'ASC';

        $field = $fieldLookup[$fieldShort];
        $order = $orderLookup[$orderShort];

        $sql = "SELECT userID, username, admin, creation, lastLogin, lastCharacter,mainCharacter, COUNT(characterID) AS count FROM st_users LEFT JOIN st_userskeys USING (userID) LEFT JOIN st_keyscharacters USING (keyID) GROUP BY userID ORDER BY $field $order LIMIT ?,?";
        $query = $this->db->query($sql, array($offset, $limit));
        return $query->result();
    }

    function getUsersWithName($name) {
        $sql = "SELECT userID, username, admin, creation, lastLogin, lastCharacter,mainCharacter FROM st_users WHERE username LIKE ? ORDER BY username ASC ";
        $query = $this->db->query($sql, $name);
        return $query->result();
    }

    function getUsersWithCharacter($name) {
        $sql = "SELECT userID, username, admin, creation, lastLogin, lastCharacter,mainCharacter FROM st_characters AS c INNER JOIN st_keyscharacters AS kb USING (characterID) INNER JOIN st_userskeys AS k USING (keyID) INNER JOIN st_users AS u USING (userID) WHERE name LIKE ? GROUP BY userID";
        $query = $this->db->query($sql, $name);
        return $query->result();
    }

    function getUserCount() {
        $sql = "SELECT COUNT(*) AS count FROM st_users";
        return $this->db->query($sql)->row()->count;
    }

    function getUser($userID) {
        $sql = "SELECT userID, username, admin, creation, lastLogin, lastCharacter,mainCharacter FROM st_users WHERE userID = ?";
        $query = $this->db->query($sql, $userID);
        return $query->row_array();
    }

    function resetPassword($userID) {
        $sql = "UPDATE st_users SET password = SHA1(username) WHERE userID = ?";
        $this->db->query($sql, $userID);
        return $this->db->affected_rows();
    }

	/**
	 * @return mixed
	 * @author Kevin Mauel <kevin.mauel2@gmail.com>
	 */
	public function getCurrentUsersOnline()
	{
		$sSql = 'SELECT from_unixtime(last_activity) as last_activity, user_data FROM st_sessions WHERE user_data != "" AND from_unixtime(last_activity) > date_sub(now(), interval 2 minute) ORDER BY last_activity DESC';
		$oQuery = $this->db->query($sSql);
		return $oQuery->result();
	}

	/**
	 * @return mixed
	 * @author Kevin Mauel <kevin.mauel2@gmail.com>
	 */
	public function getLastUsersOnline()
	{
		$sSql = 'SELECT from_unixtime(last_activity) as last_activity, user_data FROM st_sessions WHERE user_data != "" AND from_unixtime(last_activity) > date_sub(now(), interval 1 DAY) AND session_id NOT IN(SELECT session_id FROM st_sessions WHERE user_data != "" AND from_unixtime(last_activity) > date_sub(now(), interval 2 minute)) ORDER BY last_activity DESC LIMIT 20';
		$oQuery = $this->db->query($sSql);
		return $oQuery->result();
	}

	public function hasUserRole($sCharacterID, $sUserRole)
	{
		if(!in_array($sUserRole, $this->aUserRoles))
		{
			return false;
		}

		$sSql = 'SELECT typeName FROM st_characterroles WHERE characterID = ? AND typeName ?';
		$oQuery = $this->db->query($sSql, array($sCharacterID, $sUserRole));

		return $oQuery->num_rows();

	}

    public function setUserPrivacy($sCharacterID, $sPrivacyType)
    {
        $sSql = "UPDATE st_characterprivacy SET ".$sPrivacyType." = NOT ".$sPrivacyType." WHERE characterID = ?";

        $this->db->query($sSql, array($sCharacterID));

        return $this->db->affected_rows();
    }

	public function getUserPrivacy($sCharacterID)
	{

		$sSql = 'SELECT A,U,X,C,R,I FROM st_characterprivacy WHERE characterID = ?';

		return $this->db->query($sSql, $sCharacterID)->row();
	}

	public function getPrivacyFields()
	{
		$aFields = $this->db->list_fields('st_characterprivacy');

		unset($aFields[0]);

		return $aFields;
	}

}

?>