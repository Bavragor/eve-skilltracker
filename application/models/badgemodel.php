<?php

class Badgemodel extends CI_Model {
    // GETTING BADGES

    /**
     * Returns a list of all badges (categories, level, name, icon)
     * @return array of objects
     */
    public function getBadges() {
        $sql = "SELECT c1.categoryID, c1.categoryName, c1.rgt, c1.lft,COUNT(*)-1 AS level, b.badgeID, b.badgeName,b.typeID, b.iconFile FROM st_categories AS c1 LEFT JOIN st_badges AS b ON (b.categoryID = c1.categoryID), st_categories AS c2  WHERE c1.lft BETWEEN c2.lft AND c2.rgt GROUP BY c1.lft, b.badgeID ORDER BY c1.lft,b.sortName;";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /**
     * Returns a simple list of all badges (id, name)
     * @return array of objects
     */
    public function getBadgeList() {
        $sql = "SELECT b.badgeID, b.badgeName FROM st_badges AS b ORDER BY b.badgeName;";
        $query = $this->db->query($sql);
        $result = $query->result();
        $query->free_result();
        return $result;
    }

    // SAVING AND EDITING BADGES

    public function saveBadge($bn, $ci) {
        $sql = "INSERT INTO st_badges (badgeName,categoryID) VALUES (?,?);";
        $this->db->query($sql, array($bn, $ci));
        return $this->db->affected_rows();
    }

    public function deleteBadge($bid) {
        $sql = "DELETE FROM st_badges WHERE badgeID = ?";
        $this->db->query($sql, $bid);
        return $this->db->affected_rows();
    }

    public function editBadgeName($badgeID, $badgeName) {
        $sql = "UPDATE st_badges SET badgeName = ? WHERE badgeID = ?";
        $this->db->query($sql, array($badgeName, $badgeID));
        echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    public function editBadgeCategory($badgeID, $categoryID) {
        $sql = "UPDATE st_badges SET categoryID = ? WHERE badgeID = ?";
        $this->db->query($sql, array($categoryID, $badgeID));
        echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    public function editSortName($badgeID, $sortName) {
        $sql = "UPDATE st_badges SET sortName = ? WHERE badgeID = ?";
        $this->db->query($sql, array($sortName, $badgeID));
        echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    public function setIcon($badgeID, $fileName) {
        $sql = "UPDATE st_badges SET iconFile = ? WHERE badgeID = ?;";
        $this->db->query($sql, array($fileName, $badgeID));
        return $this->db->affected_rows();
    }

    public function setShipTypeID($badgeID, $typeID) {
        $sql = "UPDATE st_badges SET typeID = ? WHERE badgeID = ?";
        $this->db->query($sql, array($typeID, $badgeID));
        return $this->db->affected_rows();
    }

    public function autoRelateShips() {
        $sql = "UPDATE st_badges AS b SET typeID = (SELECT typeID FROM eve_invtypes AS t INNER JOIN eve_invgroups AS g USING (groupID) WHERE typeName LIKE b.badgeName AND g.categoryID = 6)";
        $this->db->query($sql);
        echo $this->db->last_query() . '</br>';
        return $this->db->affected_rows();
    }

    public function getBadgeForShip($typeID) {
        $sql = "SELECT badgeID FROM st_badges WHERE typeID = ?";
        $query = $this->db->query($sql, $typeID);
        return $query->row()->badgeID;
    }
    
    public function getBadgeTitle($badgeID, $rankID) {
        $sql = "SELECT badgeName FROM st_badges WHERE badgeID = ?";
        $query = $this->db->query($sql,$badgeID);
        $badgeName = $query->row()->badgeName;
        
        $sql = "SELECT rankName FROM st_ranks WHERE rankID = ?";
        $query = $this->db->query($sql,$rankID);
        $rankName = $query->row()->rankName;
        
        return $badgeName . ' - ' . $rankName;
    }

	public function getBadgeName($badgeID) {
		$sql = "SELECT badgeName FROM st_badges WHERE badgeID = ?";
		$query = $this->db->query($sql,$badgeID);
		$badgeName = $query->row()->badgeName;

		return $badgeName;
	}

    public function getBadgeID($sBadgeName)
    {
        $sBadgeName = urldecode($sBadgeName);
        $sql = "SELECT badgeID FROM st_badges WHERE badgeName LIKE ?";
        $query = $this->db->query($sql, $sBadgeName);
        $badgeID = $query->row()->badgeID;
        return $badgeID;
    }

	// DETAILED INFORMATIONS
    public function getBadge($badgeID) {
        $sql = "SELECT b.badgeID, b.badgeName, b.sortName, b.categoryID, c.categoryName, b.typeID, b.iconFile,COALESCE(t.typeName,'') AS typeName,COALESCE(t.description,'') AS description FROM st_badges AS b INNER JOIN st_categories AS c USING (categoryID) LEFT JOIN eve_invtypes AS t USING (typeID) WHERE b.badgeID = ?";
        $query = $this->db->query($sql, $badgeID);
        return $query->row();
    }

    // SKILLS

    public function getSkills($badgeID, $rankID) {
        $sql = "SELECT s.typeID, s.rankID, s.badgeID, s.level, s.skillGroup,t.typeName FROM st_badgeskills AS s INNER JOIN eve_invtypes AS t USING (typeID) WHERE s.badgeID = ? AND s.rankID = ? ORDER BY s.skillGroup";
        $query = $this->db->query($sql, array($badgeID, $rankID));
        return $query->result();
    }

    public function saveSkill($badgeID, $rankID, $typeID, $level, $or) {
        $sql = "INSERT INTO st_badgeskills (badgeID, rankID, typeID, level, skillGroup) SELECT ?,?,?,?, COALESCE((SELECT MAX(skillGroup) + $or FROM st_badgeskills WHERE rankID = ? AND badgeID = ? GROUP BY rankID, badgeID),1)";
        $this->db->query($sql, array($badgeID, $rankID, $typeID, $level, $rankID, $badgeID));
        echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    public function deleteSkill($badgeID, $rankID, $typeID) {
        $sql = "DELETE FROM st_badgeskills WHERE badgeID = ? AND rankID = ? AND typeID = ?";
        $this->db->query($sql, array($badgeID, $rankID, $typeID));
        return $this->db->affected_rows();
    }

    public function getSkillsWithSkillpoints($badgeID, $rankID) {
        $sql = "SELECT badgeID, rankID, b.typeID,s.typeName, (s.trainingTimeMultiplier * sp1.skillpoints) AS sp, b.level, skillGroup FROM st_badgeskills AS b INNER JOIN st_skills AS s ON (b.typeID = s.typeID) INNER JOIN st_skilllevelpoints AS sp1 ON (b.level = sp1.level) WHERE badgeID = ? AND rankID = ? ORDER BY skillGroup ASC";
        $query = $this->db->query($sql, array($badgeID, $rankID));
        return $query->result();
    }

    public function getUniqueSkills($badgeID, $rankID) {
        $sql = "SELECT bs.typeID, t.typeName, bs.level FROM st_badgeskills AS bs INNER JOIN eve_invtypes AS t ON (t.typeID = bs.typeID) WHERE bs.badgeID = ? AND bs.rankID = ? GROUP BY bs.skillGroup HAVING COUNT(*) = 1";
        $query = $this->db->query($sql, array($badgeID, $rankID));
        return $query->result();
    }

    // SUBBADGES

    public function getSubBadges($badgeID, $rankID) {
        $sql = "SELECT s.badgeID, s.parentBadgeRank, s.parentBadgeID, s.rankID, s.badgeGroup,b.badgeName,r.rankName,m.maxSP FROM st_subbadges AS s INNER JOIN st_badges AS b USING (badgeID) INNER JOIN st_ranks AS r USING (rankID) LEFT JOIN st_badgemaxskillpoints AS m ON (s.badgeID = m.badgeID AND s.rankID = m.rankID) WHERE s.parentBadgeID = ? AND s.parentBadgeRank = ? ORDER BY s.badgeGroup";
        $query = $this->db->query($sql, array($badgeID, $rankID));
        $result = $query->result();
        $query->free_result();
        return $result;
    }

    public function saveSubBadge($parentBadgeID, $parentRankID, $badgeID, $rank, $or) {
        $sql = "INSERT INTO st_subbadges (parentBadgeID, parentBadgeRank, badgeID, rankID, badgeGroup) SELECT ?,?,?,?, COALESCE((SELECT MAX(badgeGroup) + $or FROM st_subbadges WHERE parentBadgeRank = ? AND parentBadgeID = ? GROUP BY parentBadgeRank, parentBadgeID),1)";
        $this->db->query($sql, array($parentBadgeID, $parentRankID, $badgeID, $rank, $parentRankID, $parentBadgeID));
        echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    public function deleteSubBadge($parentBadgeID, $rankID, $badgeID) {
        $sql = "DELETE FROM st_subbadges WHERE parentBadgeID = ? AND parentBadgeRank = ? AND badgeID = ?";
        $this->db->query($sql, array($parentBadgeID, $rankID, $badgeID));
        return $this->db->affected_rows();
    }

    // USER-DATA

    public function getBadgesForUser($characterID) {
        $sql = "SELECT c1.categoryID, c1.categoryName, c1.rgt, c1.lft,COUNT(*)-1 AS level, b.badgeID, b.badgeName,b.typeID, b.iconFile,sp.skillpoints,r.rankID, r.rankName, max.maxSP FROM (st_categories AS c1, st_categories AS c2) LEFT JOIN st_badges AS b ON (b.categoryID = c1.categoryID) LEFT JOIN st_badgemaxskillpoints AS max ON (max.badgeID = b.badgeID AND max.rankID = 3) LEFT JOIN st_badgeskillpoints AS sp ON (sp.badgeID = b.badgeID AND sp.characterID = ?) LEFT JOIN st_ranks AS r ON (sp.rankID = r.rankID) WHERE c1.lft BETWEEN c2.lft AND c2.rgt GROUP BY c1.lft, b.badgeID ORDER BY c1.lft,b.sortName";
        $query = $this->db->query($sql, $characterID);
        return $query->result();
    }

	/**
	 * Get Basic Badges for User
	 * @param $sCharacterID
	 * @return mixed
	 * @author Kevin Mauel <kevin.mauel@twt.de>
	 */
	public function getNoBadgesForUser($sCharacterID)
	{
		$sql = "SELECT c1.categoryID, c1.categoryName, c1.rgt, c1.lft,COUNT(*)-1 AS level, b.badgeID, b.badgeName,b.typeID, b.iconFile,sp.skillpoints,sp.rankID,r.rankID, r.rankName, max.maxSP FROM (st_categories AS c1, st_categories AS c2) LEFT JOIN st_badges AS b ON (b.categoryID = c1.categoryID) LEFT JOIN st_badgemaxskillpoints AS max ON (max.badgeID = b.badgeID AND max.rankID = 3) LEFT JOIN st_badgeskillpoints AS sp ON (sp.badgeID = b.badgeID AND sp.characterID = ?) LEFT JOIN st_ranks AS r ON (sp.rankID = r.rankID) WHERE c1.lft BETWEEN c2.lft AND c2.rgt GROUP BY c1.lft, b.badgeID ORDER BY c1.lft,b.sortName";
		$query = $this->db->query($sql, $sCharacterID);
		return $query->result();
	}

	/**
	 * Get Basic Badges for User
	 * @param $sCharacterID
	 * @return mixed
	 * @author Kevin Mauel <kevin.mauel@twt.de>
	 */
	public function getBasicBadgesForUser($sCharacterID)
	{
		$sql = "SELECT c1.categoryID, c1.categoryName, c1.rgt, c1.lft,COUNT(*)-1 AS level, b.badgeID, b.badgeName,b.typeID, b.iconFile,sp.skillpoints,sp.rankID,r.rankID, r.rankName, max.maxSP FROM (st_categories AS c1, st_categories AS c2) LEFT JOIN st_badges AS b ON (b.categoryID = c1.categoryID) LEFT JOIN st_badgemaxskillpoints AS max ON (max.badgeID = b.badgeID AND max.rankID = 3) LEFT JOIN st_badgeskillpoints AS sp ON (sp.badgeID = b.badgeID AND sp.characterID = ?) LEFT JOIN st_ranks AS r ON (sp.rankID = r.rankID) WHERE c1.lft BETWEEN c2.lft AND c2.rgt GROUP BY c1.lft, b.badgeID ORDER BY c1.lft,b.sortName";
		$query = $this->db->query($sql, $sCharacterID);
		return $query->result();
	}

	/**
	 * Get Medium Badges for User
	 * @param $sCharacterID
	 * @return mixed
	 * @author Kevin Mauel <kevin.mauel@twt.de>
	 */
	public function getMediumBadgesForUser($sCharacterID)
	{
		$sSql = "SELECT c1.categoryID, c1.categoryName, c1.rgt, c1.lft,COUNT(*)-1 AS level, b.badgeID, b.badgeName,b.typeID, b.iconFile,sp.skillpoints,sp.rankID,r.rankID, r.rankName, max.maxSP FROM (st_categories AS c1, st_categories AS c2) LEFT JOIN st_badges AS b ON (b.categoryID = c1.categoryID) LEFT JOIN st_badgemaxskillpoints AS max ON (max.badgeID = b.badgeID AND max.rankID = 3) LEFT JOIN st_badgeskillpoints AS sp ON (sp.badgeID = b.badgeID AND sp.characterID = ?) LEFT JOIN st_ranks AS r ON (sp.rankID = r.rankID) WHERE c1.lft BETWEEN c2.lft AND c2.rgt GROUP BY c1.lft, b.badgeID ORDER BY c1.lft,b.sortName";
		$oQuery = $this->db->query($sSql, $sCharacterID);
		return $oQuery->result();
	}

	/**
	 * Get Expert Badges for User
	 * @param $sCharacterID
	 * @return mixed
	 * @author Kevin Mauel <kevin.mauel@twt.de>
	 */
	public function getExpertBadgesForUser($sCharacterID)
	{
		$sSql = "SELECT c1.categoryID, c1.categoryName, c1.rgt, c1.lft,COUNT(*)-1 AS level, b.badgeID, b.badgeName,b.typeID, b.iconFile,sp.skillpoints,sp.rankID,r.rankID, r.rankName, max.maxSP FROM (st_categories AS c1, st_categories AS c2) LEFT JOIN st_badges AS b ON (b.categoryID = c1.categoryID) LEFT JOIN st_badgemaxskillpoints AS max ON (max.badgeID = b.badgeID AND max.rankID = 3) LEFT JOIN st_badgeskillpoints AS sp ON (sp.badgeID = b.badgeID AND sp.characterID = ?) LEFT JOIN st_ranks AS r ON (sp.rankID = r.rankID) WHERE c1.lft BETWEEN c2.lft AND c2.rgt GROUP BY c1.lft, b.badgeID ORDER BY c1.lft,b.sortName";
		$oQuery = $this->db->query($sSql, $sCharacterID);
		return $oQuery->result();
	}

    public function getBadgeForUser($badgeID, $characterID) {
        $sql = "SELECT b.badgeID, b.badgeName, b.sortName, b.categoryID, c.categoryName, b.typeID, b.iconFile, COALESCE(t.typeName,'') AS typeName,COALESCE(t.description,'') AS description ,sp.skillpoints, max.maxSP,sp.rankID,r.rankName  FROM st_badges AS b INNER JOIN st_categories AS c USING (categoryID) INNER JOIN st_badgeskillpoints AS sp ON (sp.badgeID = b.badgeID AND sp.characterID = ?) LEFT JOIN st_ranks AS r ON (r.rankID = sp.rankID) INNER JOIN st_badgemaxskillpoints AS max ON (max.badgeID = b.badgeID AND max.rankID = 3) LEFT JOIN eve_invtypes AS t USING (typeID) WHERE b.badgeID = ?";
        $query = $this->db->query($sql, array($characterID, $badgeID));
        return $query->row();
    }

    /**
     * @param $sTypeID
     * @return mixed
     * @author Kevin Mauel <kevin.mauel2@gmail.com>
     */
    public function getTraitsForBadge($sTypeID)
    {
        $sSql = 'SELECT bonus, bonusText, skillID FROM eve_invtraits WHERE typeID = ? ORDER BY skillID DESC';
        $oQuery = $this->db->query($sSql, array($sTypeID));

        return $oQuery->result();
    }

    /**
     * @param $sSkillID
     * @return mixed
     * @author Kevin Mauel <kevin.mauel2@gmail.com>
     */
    public function getSkillName($sSkillID)
    {
        $sSql = 'SELECT typeName FROM eve_invtypes WHERE typeID = ? ';
        $oQuery = $this->db->query($sSql, array($sSkillID));

        return $oQuery->row();
    }

    public function getSkillsForUserWithSkillpoints($badgeID, $rankID, $characterID) {
        $sql = "SELECT badgeID, rankID, b.typeID,s.typeName, (s.trainingTimeMultiplier * sp1.skillpoints) AS sp, b.level, skillGroup, cs.level AS characterLevel, (s.trainingTimeMultiplier * sp2.skillpoints) AS characterSP FROM st_badgeskills AS b INNER JOIN st_skills AS s ON (b.typeID = s.typeID) INNER JOIN st_skilllevelpoints AS sp1 ON (b.level = sp1.level) LEFT JOIN st_characterskills AS cs ON(b.typeID = cs.typeID AND cs.ownerID = ?) LEFT JOIN st_skilllevelpoints AS sp2 ON (cs.level = sp2.level) WHERE badgeID = ? AND rankID = ? ORDER BY skillGroup ASC";
        $query = $this->db->query($sql, array($characterID, $badgeID, $rankID));
        $result = $query->result();
        $query->free_result();
        return $result;
    }

    public function getSkillsForUser($badgeID, $rankID, $characterID) {
        $sql = "SELECT badgeID, rankID, b.typeID,s.typeName, b.level, skillGroup, cs.level AS characterLevel FROM st_badgeskills AS b INNER JOIN st_skills AS s ON (b.typeID = s.typeID) LEFT JOIN st_characterskills AS cs ON(b.typeID = cs.typeID AND cs.ownerID = ?) WHERE badgeID = ? AND rankID = ? ORDER BY skillGroup ASC";
        $query = $this->db->query($sql, array($characterID, $badgeID, $rankID));
        return $query->result();
    }

    public function getSubbadgesForUser($badgeID, $rankID, $characterID) {
        $sql = "SELECT s.badgeID, s.parentBadgeRank, s.parentBadgeID, s.rankID, s.badgeGroup, b.badgeName, r.rankName,m.maxSP, sp.skillpoints,sp.rankID AS characterRankID,r2.rankName AS characterRank FROM st_subbadges AS s INNER JOIN st_badges AS b USING (badgeID) INNER JOIN st_ranks AS r ON (r.rankID = s.rankID) INNER JOIN st_badgemaxskillpoints AS m ON (m.badgeID = s.badgeID AND m.rankID = s.rankID) INNER JOIN st_badgeskillpoints AS sp ON (sp.badgeID = s.badgeID AND sp.characterID = ?) LEFT JOIN st_ranks AS r2 ON (r2.rankID = sp.rankID) WHERE s.parentBadgeID = ? AND s.parentBadgeRank = ? ORDER BY s.badgeGroup";
        $query = $this->db->query($sql, array($characterID, $badgeID, $rankID));
        return $query->result();
    }

    public function isSkillInBadge($sBadgeID, $sSkillID)
    {
        $sSql = 'SELECT badgeID FROM st_badgeskills WHERE badgeID = ? AND typeID = ?';
        return $this->db->query($sSql, array($sBadgeID, $sSkillID))->num_rows();
    }

    /**
     * build table with the maximum skillpoints for each badge and rank
     */
    public function buildMaximumSkillpointTable() {
        $this->db->query("TRUNCATE st_badgemaxskillpoints");
        $this->load->model('rankmodel');

        $ranks = $this->rankmodel->getRanks();
        $badges = $this->badgemodel->getBadgeList();
        $sps = array();
        $i = 0;
        foreach ($badges as $b) {
            foreach ($ranks as $r) {
                $this->calculateMaxSkillpoints($b->badgeID, $r->rankID, $sps);
                $i++;
            }
        }
        return $i;
    }

    /**
     * Calculates the maximum skillpoints for a spezific badge. It considers skills as well as subbadges.
     *
     * @param int $badgeID
     * @param int $rankID
     * @param omt $sps an array for caching the calculated skillpoints
     * @return int calculated maximum skillpoints
     */
    public function calculateMaxSkillpoints($badgeID, $rankID, &$sps) {
        // has it been calculated yet? look it up in the array sps
        if (isset($sps[$badgeID])) {
            if (isset($sps[$badgeID][$rankID])) {
                return $sps[$badgeID][$rankID];
            }
        }

        $maxSP = 0;

        // get max skillpoints for inferior rank
        if ($rankID > 1) {
            $maxSP += $this->calculateMaxSkillpoints($badgeID, $rankID - 1, $sps);
        }


        // get skills and subbadges
        $skills = $this->getSkillsWithSkillpoints($badgeID, $rankID);
        $subbadges = $this->getSubBadges($badgeID, $rankID);


        // calculate maxSP from skills
        $lastGroup = -1;
        $groupSP = 0;
        foreach ($skills as &$s) {
            // new skillgroup?
            if ($s->skillGroup != $lastGroup) {
                // add group SP to current SP
                $maxSP += $groupSP;
                // reset group SP
                $groupSP = $s->sp;
            }
            // current SP smaller than last minimum of the group?
            if ($s->sp < $groupSP) {
                $groupSP = $s->sp;
            }

            $lastGroup = $s->skillGroup;
        } // end foreach skills
        $maxSP += $groupSP;

        // calculate maxSP from the subbadges
        $groupSP = 0;
        $lastGroup = -1;
        foreach ($subbadges as &$s) {
            // recursive call
            $s->sp = $this->calculateMaxSkillpoints($s->badgeID, $s->rankID, $sps);
            // new group?
            if ($s->badgeGroup != $lastGroup) {
                // add group SP to current SP
                $maxSP += $groupSP;
                // reset group SP
                $groupSP = $s->sp;
            }
            // current SP smaller than last minimum of the group?
            if ($s->sp < $groupSP) {
                $groupSP = $s->sp;
            }
            $lastGroup = $s->badgeGroup;
        }

        $maxSP += $groupSP;


        // is there an entry already
        $this->saveMaxSkillpoints($badgeID, $rankID, $maxSP);
        if (isset($sps[$badgeID])) {
            $sps[$badgeID][$rankID] = $maxSP;
        } else {
            $sps[$badgeID] = array($rankID => $maxSP);
        }
        return $maxSP;
    }

    public function getSkillsForBadge($sBadgeID)
    {
        $sSql = 'SELECT typeID FROM st_badgeskills WHERE badgeID = ?';
        return $this->db->query($sSql, $sBadgeID)->result_array();
    }

    /**
     * Calculates the achieved cound of skillpoints of each badge for this user.
     * @param int $characterID
     */
    public function calculateAllBadges($characterID = null, $aChangedOrNewSkills = null) {
        $badges = $this->getBadgeList();

        $sps = array();
        $count = 0;

        foreach ($badges as &$b) {
            $count++;
            $aBadgeSkills = $this->getSkillsForBadge($b->badgeID);

            if(!empty($aChangedOrNewSkills[$characterID]))
            {
                foreach($aChangedOrNewSkills[$characterID] as $sSkillID => $iUpdatedOrNew)
                {
                    if(in_array_recursive($sSkillID, $aBadgeSkills))
                    {
                        if($iUpdatedOrNew > 0)
                        {
                            $this->calculateSkillpoints($b->badgeID, $characterID, $sps);
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            else
            {
                $this->calculateSkillpoints($b->badgeID, $characterID, $sps);
            }
        }

        empty($sps);
        unset($sps);
        unset($badges);
        return $count;
    }

    /**
     * Calculates the amount of skillpoint a user has achieved in the special badge.
     * @param int $badgeID
     * @param int $characterID
     * @param array $sps
     * @return int the calculated skillpoints
     */
    public function calculateSkillpoints(&$badgeID, &$characterID, &$sps) {
        // has it been calculated yet? look it up in the array sps
        if (isset($sps[$badgeID])) {
            //echo 'loading from cache<br/>';
            return $sps[$badgeID];
        }
        $rankID = 0;
        $curSP = 0;

        $achievedRank = 0;

        do {
            $rankID++;
            //echo $rankID . '   ' . memory_get_usage(TRUE) . '</br>';
            // Get maximum skillpoints
            $maxSP = $this->getMaximumSkillpoints($badgeID, $rankID);
            //echo 'max: ' . $maxSP . '<br/>';
            // Get all skills plus skills of current character
            $skills = $this->getSkillsForUserWithSkillpoints($badgeID, $rankID, $characterID);
            $subbadges = $this->getSubBadges($badgeID, $rankID);

            // calculating skills
            $lastGroup = -1;
            $groupSP = 0;
            foreach ($skills as &$s) {
                // new skillgroup?
                if ($s->skillGroup != $lastGroup) {
                    // add group SP to current SP
                    $curSP += $groupSP;
                    // reset group SP
                    $groupSP = 0;

                    //if ($groupSP < $last->sp) {
                    //	$achievedRank = $rankID - 1;
                    //}
                }
                // character has more SP than required?
                if ($s->characterSP > $groupSP) {
                    if ($s->characterSP >= $s->sp) {
                        $groupSP = $s->sp;
                    } else {
                        $groupSP = $s->characterSP;
                    }
                }
                $lastGroup = $s->skillGroup;
            } // end foreach skills
            $curSP += $groupSP;

            // calculate subbadges
            $groupSP = 0;
            $lastGroup = -1;
            foreach ($subbadges as &$s) {
                // recursive call
                //echo 'recursion ' . $s->badgeName . '<br/>';
                $s->sp = $this->calculateSkillpoints($s->badgeID, $characterID, $sps);
                // new group?
                if ($s->badgeGroup != $lastGroup) {
                    // add group SP to current SP
                    $curSP += $groupSP;
                    // reset group SP
                    $groupSP = 0;
                }
                // current SP greater than last maximum of the group?
                if ($s->sp > $groupSP) {
                    if ($s->sp >= $s->maxSP) {
                        $groupSP = $s->maxSP;
                    }
                }
                $lastGroup = $s->badgeGroup;
            }
            $curSP += $groupSP;
            //echo 'cur:' . $curSP . '<br/>';
            if ($curSP >= $maxSP) {
                $curSP = $maxSP;
                $achievedRank = $rankID;
            }
            unset($skills);
            unset($subbadges);
        } while ($curSP == $maxSP && $rankID < 3);

        $this->saveSkillpoints($badgeID, $characterID, $curSP, $achievedRank);

        $sps[$badgeID] = $curSP;

        return $curSP;
    }

    public function getSkillpoints($badgeID, $characterID) {
        $sql = "SELECT s.skillpoints FROM st_badgeskillpoints AS s WHERE s.badgeID = ? AND characterID = ?";
        $query = $this->db->query($sql, array($badgeID, $characterID));
        if ($query->num_rows() == 1) {
            return $query->row()->skillpoints;
        } else {
            return -1;
        }
    }

    public function saveSkillpoints($badgeID, $characterID, $skillpoints, $rankID) {
        $sql = "INSERT INTO st_badgeskillpoints (badgeID, characterID, skillpoints, rankID) VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE skillpoints = VALUES(skillpoints), rankID = VALUES(rankID)";
        $this->db->query($sql, array($badgeID, $characterID, $skillpoints, $rankID));
        return $this->db->affected_rows();
    }

    public function saveMaxSkillpoints($badgeID, $rankID, $maxSP) {
        $sql = "INSERT INTO st_badgemaxskillpoints (badgeID, rankID, maxSP) VALUES (?,?,?)";
        $this->db->query($sql, array($badgeID, $rankID, $maxSP));
        return $this->db->affected_rows();
    }

    public function getMaximumSkillpoints($badgeID, $rankID) {
        $sql = "SELECT maxSP FROM st_badgemaxskillpoints WHERE badgeID = ? AND rankID = ?";
        $query = $this->db->query($sql, array($badgeID, $rankID));
        if ($query->num_rows() > 0) {
            $result = $query->row()->maxSP;
        } else {
            $result = 0;
        }
        $query->free_result();
        return $result;
    }

    public function getCharacterBadgeOverview($characterID) {
        $sql = "SELECT rankID, COUNT(rankID) AS c FROM st_badgeskillpoints AS b WHERE characterID = ? GROUP BY characterID, rankID DESC";
        $query = $this->db->query($sql, $characterID);
        return $query->result();
    }

    public function getCalculationQueue() {
        $sql = "SELECT characterID, timestamp FROM st_calculationqueue ORDER BY timestamp ASC;";
        $query = $this->db->query($sql);

        return $query->result();
    }

    public function removeFromCalculationQueue($characterID) {
        $sql = "DELETE FROM st_calculationqueue WHERE characterID = ?";
        $this->db->query($sql, $characterID);
        return $this->db->affected_rows();
    }

    public function getEvemonPlan($badgeID, $rankID, $characterID = 0) {
        if ($characterID != 0) {
            $this->load->model('usermodel');
            $rawSkills = $this->usermodel->getSkills($characterID);
            $characterSkills = array();
            foreach ($rawSkills as $s) {
                if ($s->typeName != "") {
                    $typeID = (String) $s->typeID;
                    $characterSkills[$typeID] = $s->level;
                }
            }
        } else {
            $characterSkills = array();
        }
        $skillList = array();
        $existingSkills = array();
        for ($i = 1; $i <= $rankID; $i++) {
            $this->getEvemonPlanRecursive($badgeID, $i, $skillList, $existingSkills, $characterSkills);
        }
        return $skillList;
    }

    public function getEvemonPlanRecursive($badgeID, $rankID, &$skills, &$existingSkills, &$characterSkills) {
        $subbadges = $this->getSubbadges($badgeID, $rankID);

        foreach ($subbadges as $sb) {
            $this->getEvemonPlanRecursive($sb->badgeID, $sb->rankID, $skills, $existingSkills, $characterSkills);
        }

        $badgeSkills = $this->getSkills($badgeID, $rankID);

        foreach ($badgeSkills as $s) {
            (isset($characterSkills[(String) $s->typeID])) ? $from = $characterSkills[(String) $s->typeID] + 1 : $from = 1;
            for ($i = $from; $i <= $s->level; $i++) {
                
                if (!isset($existingSkills[$s->typeID . '-' . $i])) {
                    $skill = new stdClass();
                    $skill->typeID = $s->typeID;
                    $skill->typeName = $s->typeName;
                    $skill->level = $i;
                    $skills[] = $skill;
                    $existingSkills[$s->typeID . '-' . $i] = TRUE;
                }
            }
        }
    }

}

?>
