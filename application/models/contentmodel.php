<?php

/**
 * Class Contentmodel
 * @author Kevin Mauel <kevin.mauel2@gmail.com>
 */
class Contentmodel extends CI_Model
{
    public function getAllPages()
    {
        $sSql = 'SELECT contentID, contentName FROM st_content WHERE 1';
        $oQuery = $this->db->query($sSql);
        return $oQuery->result();
    }

    public function getPage($sContentID)
    {
        $sSql = 'SELECT * FROM st_content WHERE contentID = ?';
        $oQuery = $this->db->query($sSql, $sContentID);
        return $oQuery->row();
    }

	public function getPageID($sContentName)
	{
		$sContentName = urldecode($sContentName);
		$sSql = 'SELECT * FROM st_content WHERE contentName = ?';
		$oQuery = $this->db->query($sSql, $sContentName);
		return $oQuery->row()->contentID;
	}

    public function deletePage($sContentID)
    {
        $sSql = 'DELETE FROM st_content WHERE contentID = ?';
        $this->db->query($sSql, $sContentID);
        return $this->db->affected_rows();
    }

    public function editPage($sContentID, $sContentName, $sContentText)
    {
        $sSql = 'UPDATE st_content SET contentName = ?, contentLongtext = ? WHERE contentID = ?';
        $this->db->query($sSql, array($sContentName, $sContentText,$sContentID));
        return $this->db->affected_rows();
    }

    public function createPage($sContentName = '', $sContentText = '')
    {
        $sSql = 'INSERT INTO st_content (contentName, contentLongtext, creationdate) VALUES(?,?,?);';
        $this->db->query($sSql, array($sContentName, $sContentText, time()));
        return $this->db->insert_id();
    }

}