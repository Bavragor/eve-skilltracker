<?php

class Corpmodel extends CI_Model
{
	public function buildCharacterCorps()
	{
		$this->load->model('apimodel');

		$sSql = 'SELECT DISTINCT a.corporationID, a.characterID FROM st_characters as a LEFT JOIN eve_crpNPCCorporations as b ON(a.corporationID = b.corporationID) WHERE b.corporationID IS NULL';
		$aQueryResult = $this->db->query($sSql)->result();

		foreach($aQueryResult as $oSingleRec)
		{
			$aCorpInfo = $this->apimodel->getCorpInfo($oSingleRec->corporationID);

			$sCorpSql = str_replace('INSERT', 'INSERT IGNORE', $this->db->insert_string('st_corporations', $aCorpInfo));

			$aRoleData = array(
				'characterID' => $aCorpInfo['ceoID'],
				'typeName'  => 'CEO'
			);
			$sCharacterRoleSql = str_replace('INSERT', 'INSERT IGNORE', $this->db->insert_string('st_characterroles', $aRoleData));

			$this->db->query($sCorpSql);
			$this->db->query($sCharacterRoleSql);
		}

	}
}