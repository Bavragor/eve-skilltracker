<?php

class Categorymodel extends CI_Model {

    public function getCategories() {
        $sql = "SELECT c1.categoryID, c1.categoryName, c1.rgt, c1.lft,COUNT(*)-1 AS level FROM st_categories AS c1, st_categories AS c2 WHERE c1.lft BETWEEN c2.lft AND c2.rgt GROUP BY c1.lft ORDER BY c1.lft;";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function saveCategory($cn, $parentRgt, $parentLft) {
        $sql = "UPDATE st_categories SET rgt=rgt+2 WHERE rgt >= ?;";
        $this->db->query($sql, $parentRgt);

        $sql = "UPDATE st_categories SET lft=lft+2 WHERE lft > ?;";
        $this->db->query($sql, $parentRgt);

        $sql = "INSERT INTO st_categories (categoryID, categoryName, lft, rgt) VALUES(NULL,?,?,?);";
        $this->db->query($sql, array($cn, $parentRgt, $parentRgt + 1));

        return $this->db->affected_rows();
    }

    public function deleteCategory($cid, $rgt, $lft) {
        $sql = "DELETE FROM st_categories WHERE lft BETWEEN ? AND ? OR categoryID = ?";
        $this->db->query($sql, array($lft, $rgt, $cid));

        $sql = "UPDATE st_categories SET lft=lft-? WHERE lft > ?;";
        $this->db->query($sql, array($rgt - $lft + 1, $rgt));

        $sql = "UPDATE st_categories SET rgt=rgt-? WHERE rgt > ?;";
        $this->db->query($sql, array($rgt - $lft + 1, $rgt));

        return $this->db->affected_rows();
    }

    public function editCategoryName($categoryID, $categoryName) {
        $sql = "UPDATE st_categories SET categoryName = ? WHERE categoryID = ?";
        $this->db->query($sql, array($categoryName, $categoryID));
        return $this->db->affected_rows();
    }

    public function moveCategory($lft, $rgt, $targetLft) {
        // calculate position adjustment variables
        $width = $rgt - $lft + 1;
        $distance = $targetLft - $lft;
        $tmppos = $lft;

        // backwards movement must account for new space
        if ($distance < 0) {
            $distance -= $width;
            $tmppos += $width;
        }

        $sql = "UPDATE st_categories SET lft = lft + ? WHERE lft >= ?";
        $this->db->query($sql, array($width, $targetLft));
        //echo $this->db->last_query();
        $sql = "UPDATE st_categories SET rgt = rgt + ? WHERE rgt >= ?";
        $this->db->query($sql, array($width, $targetLft));
        //echo $this->db->last_query();

        $sql = "UPDATE st_categories SET lft = lft + ?,rgt = rgt + ? WHERE lft >= ? AND rgt < ? + ?";
        $this->db->query($sql, array($distance, $distance, $tmppos, $tmppos, $width));
        $r = $this->db->affected_rows();
        //echo $this->db->last_query();

        $sql = "UPDATE st_categories SET lft = lft - ? WHERE lft > ?";
        $this->db->query($sql, array($width, $rgt));
        //echo $this->db->last_query();
        $sql = "UPDATE st_categories SET rgt = rgt - ? WHERE rgt > ?";
        $this->db->query($sql, array($width, $rgt));
        //echo $this->db->last_query();

        return $r;
    }

    /*
     *  -- create new space for subtree
     *  UPDATE tags SET lpos = lpos + :width WHERE lpos >= :newpos
     *  UPDATE tags SET rpos = rpos + :width WHERE rpos >= :newpos
     * 
     *  -- move subtree into new space
     *  UPDATE tags SET lpos = lpos + :distance, rpos = rpos + :distance
     *           WHERE lpos >= :tmppos AND rpos < :tmppos + :width
     * 
     *  -- remove old space vacated by subtree
     *  UPDATE tags SET lpos = lpos - :width WHERE lpos > :oldrpos
     *  UPDATE tags SET rpos = rpos - :width WHERE rpos > :oldrpos
     */
}

?>
