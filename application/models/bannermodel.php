<?php

class Bannermodel extends CI_Model {

    public function getRandomBanner() {
        $sql = "SELECT name, link, text, filename FROM st_banners WHERE active = 1";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $i = rand(0, $query->num_rows()-1);
        return $result[$i];
    }
    
    public function getBanners() {
        $sql = "SELECT bannerID, name, filename, link, text, active FROM st_banners;";
        $query = $this->db->query($sql);
        
        return $query->result();
    }
    
    public function addBanner($name, $filename, $link, $text) {
        $sql = "INSERT INTO st_banners (bannerID, name, filename, link, text, active) VALUES (NULL,?,?,?,?,1)";
        $this->db->query($sql,array($name, $filename, $link, $text));
        return $this->db->affected_rows();
    }
    
    public function deleteBanner($bannerID) {
        $sql = "DELETE FROM st_banners WHERE bannerID = ?";
        $this->db->query($sql, $bannerID);
        return $this->db->affected_rows();
    }
    public function activateBanner($bannerID) {
        $sql = "UPDATE st_banners SET active = NOT active WHERE bannerID = ?";
        $this->db->query($sql, $bannerID);
        return $this->db->affected_rows();
    }

}

?>
