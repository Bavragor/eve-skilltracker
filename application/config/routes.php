<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "overview";
$route['404_override'] = '';

$route['badges/showRank/(:any)'] = 'badges/showRank/$1';
$route['badges/evemon/(:num)/(:num)'] = 'badges/evemon/$1/$2';
$route['badges/calculateAllBadges'] = 'badges/calculateAllBadges';
$route['badges/basic'] = 'badges/basic';
$route['badges/medium'] = 'badges/medium';
$route['badges/expert'] = 'badges/expert';
$route['badges/(:any)/(:num)'] = 'badges/$1/$2';
$route['badges/(:any)'] = 'badges/show/$1';

$route['content/edit/(:num)'] = 'content/edit/$1';
$route['content/delete/(:num)'] = 'content/delete/$1';
$route['content/create'] = 'content/create';
$route['content/savePage'] = 'content/savePage';
$route['content/(:any)'] = 'content/show/$1';

$route['users/(:any)/skills'] = 'overview/skills/$1';

$route['overview/success'] = 'overview/success';

//$route['(:any)'] = 'info/offline';


/* End of file routes.php */
/* Location: ./application/config/routes.php */