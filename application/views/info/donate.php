<div class="info">
    <h3><img src="img/pas-dot.png"/>Donate</h3>
    
    <p>We had and still have a lot of work with this tool. Also the monthly expenses for web space and domain are not irrelevant. Therefore we depend on your donations to keep the site up, in return the Skilltracker will remain free of charge.</p>
    <p>So we are asking you for a contribution you can effort, every amount is welcome. Don’t worry, we don’t request donations in a real currency for our service, we just beg you for some ISKs.</p>
    
    <p style="text-align: center;">
        <img src="badgeIcons/7_64_12.png" alt="ISK" height="64" width="64" />
        <br>Donations please exclusively to the character<br/><span class="orange">Barkkor</span> with the comment <span class="orange-heavy">SKILLTRACKER</span>.
    </p>
    <p>Thank you very much, it is just with your help we can keep this service alive.</p>
    <p>Fly safe, <br>
        the Skilltracker Team.</p>
</div>