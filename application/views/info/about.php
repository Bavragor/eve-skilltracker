<div class="info">
<h3><img src="img/pas-dot.png"/>EVE-Skilltracker.com <span class="orange">//</span> Open Beta</span></h3>

<p>My name is Barkkor, passionate EVE capsule pilot and <a href="http://barkkor.blogspot.de/" target="_blank">blogger</a>.
    I consider myself as being a community guy and would say I am somewhat noted for my blog, guides and overviews, at least in the german speaking community.</p>

<p>The idea and concept of the Skilltracker have been adopted by Mort Sinious, who programmed the first and absolutely genius Skilltracker and made it publically available. 
    Unfortunately however, Mort stopped his service in September 2010 and completely removed the site from internet in July 2012.<br/>
    After CCP implemented new ships (e.g. Noctis, Tier 3 Battlecruisers), new skills (Planetary Interaction, Target Breaker,
    Armor Resistance Phasing) and a new API system into the game, the original Skilltracker simply was of no use anymore. 
    This made me think of either overtaking the old Skilltracker, or re-writing it on my own.<p>

<p>I opted for a personal solution. In close contact to Mort, who supported me a lot, this version of the Skilltracker has been developed.</p>

<p>After some early complications with programming and web space, all obstacles have been overcome and together with Mikokoel
    (<a href="http://zap.de.com/" target="_blank">ZAP-Advanced Programming</a>) we took the challenge. 
    Because I had no knowledge of websites and programming at all, I was glad to have found Mikokoel.
    With ease he implemented all my wishes and, with a lot of self-initiative as well, he brought this wonderful website to life.</p>
<p>
Fly safe, <br/>
the Skilltracker Team.</p>
</div>