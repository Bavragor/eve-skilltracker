<div class="info">
<h3><img src="img/pas-dot.png"/>Manual</h3>

<p>The EVE Skilltracker is an web based tool for the game <a href="http://www.eveonline.com/" target="_blank">EVE Online</a>,
    that allows you to see your ability to fly ships and manage fittings.</p>

<p>This tool in general is designed to be a guide with skilling for new and inexperienced pilots. 
    EVE online is so incredibly complex that we want to help you here with finding the adequate Skills for your actual ship.</p>

<p>This website is <b>not</b> oriented to some weird fitting variations or any FOTM PvP-fleetfitting. 
    We designed the badges considering the given attributes and bonuses of the specific ships.</p>

<p>While EFT and EVEMon only show what skills are required as a minimum to be able to fly the ships or use the fittings, 
    this website goes a step further and lists the skills you need stepwise to fly a ship from basic over medium to expert.</p>

<p><span class="orange-heavy">Badges definition:</span><br>

<ul>
        <li><span class="orange">Basic:</span> You are able to fly the ship with minimal requirements in ship and fitting skills to move through empire. Not more.</li>
        <li><span class="orange">Medium:</span> You can fly and fit the ship quite good, with acceptable skills but only for fleet operations. Not alone.</li>
        <li><span class="orange">Expert:</span> You have all the skills needed to use and fit the ship at its optimum with T2 modules. Ready for action.</li>
</ul>
</p>

<p>But this doesn’t mean other tools like EFT and EVEMon will be superfluous. Quite the contrary, both tools still remain important while using this website, 
    they have their focus on specific scopes and so their individual use for you.</p>

<p>To be able to use the EVE Skilltracker, you simply have to <a href="users/signup">sign up</a> on this website, using your API key. 
    On the <a href="badges">Badges</a> site you will see quickly how it is working. Everything more is self-explanatory.</p>

<p><span class="orange-heavy">In development:</span><br>
The Skilltracker is not only interesting for younger pilots but also for whole corporations or even alliances. It is possible to create CEO, FC or Recruiter accounts, from which you will have access to your members or check out others' APIs.
This will give you a quick overview on how good which member can fly what ship. You will also be able to create internal badges for corp/alliance relevant FOTM ships and fittings and quickly see how many pilots meet the criterias for those.
This makes the Skilltracker a valuable tool for the whole EVE community.</p>

<p>
Fly safe, <br>
the Skilltracker Team.
</p>
</div>