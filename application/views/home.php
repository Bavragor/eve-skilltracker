<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta charset="utf-8">
<meta property="og:title" content="EVE Skilltracker">
<meta property="og:description" content="The Skilltracker is an web based tool for EVE Online, that allows you to see your ability to fly ships and manage fittings.">
<meta name="author" content="Barkkor | Timo Schnitzler">
<meta name="google-site-verification" content="vMSnyZRkM3ZfVa02I3g2y17kafp-vFQEQT4e1YJWbzs" />
<meta name="copyright" content="Timo Schnitzler">
<meta name="description" content="The Skilltracker is an web based tool for EVE Online, that allows you to see your ability to fly ships and manage fittings.">
<meta name="keywords" content="EVE Online Kronos, EVE Online, EVE Beginner, CCP Games, EVE Skill, EVE Tracker, EVE Fitting, EVE Planner, EVE Skillplaner, EVE Skillpoints">
<base href="<?= base_url() ?>"/>
<link href="style.css" rel="stylesheet" type="text/css">


<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700' rel='stylesheet' type='text/css'>
<title><?= (isset($title)) ? $title . ' :: ' : '' ?>EVE Skilltracker</title>
<?php
	/**
	 * User Tracking - Zuletzt aufgerufene Seite
	 */
	if(isset($title))
	{
		setUserDataFromView('last_request', '<a href="'.$_SERVER['REQUEST_URI'].'">'.$title.' :: EVE Skilltracker</a>');
	}
?>

<link rel="icon" href="favicon.ico" type="image/x-icon"/>

<!-- add your meta tags here -->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.min.js" type="text/javascript"></script>-->
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<script src="js/jquery.address.min.js" type="text/javascript"></script>
<script src="js/jquery.tools.min.js" type="text/javascript"></script>
<!--<script src="js/jquery-ui.min.js" type="text/javascript"></script>-->
<!--[if lte IE 7]>
<link href="css/patches/patch_my_layout.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>

<body>
<div id="headline"><img src="img/headertext.png"></div>



<div id="navbar">
<ul>

					<?php 
					
					$url = $_SERVER['REQUEST_URI'];
					$url = explode('/',$url);
					$current = array_pop($url);
					
					?>
					
					<span class="	<?php if (($current == NULL) && ($this->session->userdata('userID') !=0)){echo "activenav";} ?>	">
					<?php if ($this->session->userdata('userID') != 0) { ?><li><a href="">Overview</a></li><?php } ?></span>
                    
                    <span class="	<?php if ($current == 'badges'){echo "activenav";} ?>	">
                    <li><a href="badges">Badges</a></li></span>
                    
                    <li class="seperator"></li>
                    
                    <span class="	<?php if (($current == '1') || (($current == NULL) && ($this->session->userdata('userID') == 0))){echo "activenav";} ?>	">
                    <li><a href="/content/info">Info</a></li></span>
                    
                    <span class="	<?php if ($current == '4'){echo "activenav";} ?>	">
                    <li><a href="/content/manual">Manual</a></li></span>
                    
                    <span class="	<?php if ($current == '2'){echo "activenav";} ?>	">
                    <li><a href="/content/contact">Contact</a></li></span>
                    
                    <span class="	<?php if ($current == '3'){echo "activenav";} ?>	">
                    <li><a href="/content/donate">Donate</a></li></span>
                    
					<?php if ($this->session->userdata('userID') != 0) { ?><li class="seperator"></li>
                    
                    <span class="	<?php if ($current == 'forum'){echo "activenav";} ?>	"><li><a href="forum">Forum</a></li><?php } ?></span>
                    
                    <!-- <li><a href="statistics">Statistics</a></li> -->
                    
					<span class="	<?php if ($current == 'admin'){echo "activenav";} ?>	">
					<?php if ($this->session->userdata('admin') == 1) { ?><li><a href="admin">Admin</a></li><?php } ?></span>
                    
                    <li class="seperator"></li>
                    
					<?php if ($this->session->userdata('userID') == 0) { ?><li><span class="link_fake_underline loginlink">Login</span></li><?php } ?>
                    
					<span class="<?php if ($current == 'signup'){echo "activenav";} ?>	">
					<?php if ($this->session->userdata('userID') == 0) { ?><li><a href="users/signup">Sign Up</a></li><?php } ?></span>
                    
					<span class="<?php if ($current == 'settings'){echo "activenav";} ?>	">
					<?php if ($this->session->userdata('userID') != 0) { ?><li><a href="users/settings">Settings</a></li><?php } ?></span>
                    
					<?php if ($this->session->userdata('userID') != 0) { ?><li><a href="users/logout">Logout</a></li><?php } ?>


</ul>






</div>
<?php 

$race = 'jove';

if ($this->session->userdata('userID') != 0 && $this->session->userdata('race') != ''){
$race = strtolower($this->session->userdata('race'));
}


?>


<div id="core" style="background-image:url(img/bg/<?php echo $race; ?>.jpg);background-size:cover;">

<div id="wrapper">

<div id="main"><div style="float: left;width:630px;"><?= (isset($mainContent)) ? $mainContent : '' ?></div>

	<div id="panel">
		<div id="sidecontent1">
			<?= (isset($smallContent)) ? $smallContent : '' ?>
		</div>
		<div id="sidecontent2">
			<?= (isset($navigation)) ? $navigation : '' ?>
		</div>

	</div>
</div>

	<div id="poweredby" class="noprint" style="padding-bottom: 0px;">
		<a href="http://www.php.net" target="_blank"><img src="http://www.eve-skilltracker.com/minibuttons/php-power.png" width="80" height="15" alt="PHP" border="0"></a>
		<a href="http://www.mysql.com" target="_blank"><img src="http://www.eve-skilltracker.com/minibuttons/mysql.png" width="80" height="15" alt="MySQL" border="0"></a>
		<a href="http://github.com/ppetermann/pheal/" target="_blank"><img src="http://www.eve-skilltracker.com/minibuttons/pheal.png" width="80" height="15" alt="Pheal API Libary" border="0"></a>
		<a href="http://www.getfirefox.com" target="_blank"><img src="http://www.eve-skilltracker.com/minibuttons/firefox.png" width="80" height="15" alt="Firefox" border="0"></a>
		<a target="_blank" href="http://www.google.com/chrome"><img border="0" src="http://www.eve-skilltracker.com/minibuttons/chrome.png" alt="Get Chrome"></a>
		<a href="http://www.eve-online.com" target="_blank"><img src="http://www.eve-skilltracker.com/minibuttons/eve-online.jpg" width="80" height="15" alt="EVE Onlline" border="0"></a>
		<a href="https://twitter.com/Skilltracker" target="_blank"><img src="http://www.eve-skilltracker.com/minibuttons/twitter.png" width="80" height="15" alt="Twitter @wollari" border="0"></a>
		<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FEveSkilltracker&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=21&amp;appId=1507343589494414" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;width: 165px;position: relative;top: 6px;" allowtransparency="true"></iframe>
	</div>
	<div id="copyright" style="padding: 5px; text-align: center;">&copy; 2011-2014 by <a href="http://barkkor.blogspot.de/" title="Barkkor" target="_blank">Barkkor</a> &amp; Beowulf588 | <a href="/content/about" title="About">About</a> | <a href="/content/faq" title="FAQ">FAQ</a> | <a href="/content/contact" title="Contact">Contact</a> | <a href="/content/changelog" title="Changelog">Changelog</a> | <a href="/content/roadmap" title="Legal">Roadmap</a> | <a href="/content/legal disclosure" title="Legal">Legal</a> | <a href="/content/copyright notice" title="Copyright">Copyright</a></div>
</div>

</div>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-51391783-1', 'eve-skilltracker.com', {'alwaysSendReferrer': true});
    ga('set', 'anonymizeIp', true);
    ga('require', 'linkid', 'linkid.js');
    ga('send', 'pageview', {
        'title': '<?= (isset($title)) ? $title . ' :: ' : '' ?>EVE Skilltracker'
    });

</script>
</body>
</html>