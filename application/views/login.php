<script type="text/javascript">
$(function() {
   $('input:submit').button(); 
});
</script>
<?php echo form_open('users/loginSubmit'); ?>
<?php echo validation_errors('<p class="error">', '</p>'); ?>
<fieldset>
    <legend> Login</legend>
    <table>
        <tr>
            <td>
                <label for="username">Username: &nbsp;</label>
            </td>
            <td>
                <input type="text" name="username" id="username"/>
            </td>
        </tr>
        <tr>
            <td>
                <label for="password">Password: &nbsp;</label>
            </td>
            <td>
                <input type="password" name="password" id="password"/>
            </td>
        </tr>

    </table>
    <input type="submit" name="submit" id="submit" value="Login"/>
</fieldset>
<?php echo form_close(); ?>