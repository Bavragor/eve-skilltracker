<h3><img src="img/act-dot.png" /> Bannermanagement</h3>
<table class="banners">
    <colgroup>
        <col width="260">
        <col width="*">
        <col width="80">
    </colgroup>
    <?php foreach ($banners as $b) { ?>
        <tr>
            <td rowspan="2">
                <a href="<?= $b->link ?>" target="_blank" ><img src="<?= base_url() ?>img/banner/<?= $b->filename ?>"></a>
            </td>
            <td><?= $b->name ?></td>
            <td rowspan="2">
                <span class="link_fake delete" data-bannerID="<?= $b->bannerID ?>">Delete</span><br>
                <span class="link_fake toggleActivate" data-bannerID="<?= $b->bannerID ?>"><?= ($b->active == 1) ? 'deactivate' : 'activate' ?></span>
            </td>

        </tr>
        <tr>
            <td><?= $b->filename ?></td>
        </tr>
        <tr>
            <td colspan="3"><?= $b->link ?></td>
        </tr>
        <tr>
            <td colspan="3"><?= $b->text ?></td>
        </tr>
    <?php } ?>
</table>
<?= form_open('admin/addBanner') ?>
<fieldset>
    <legend><span class="orange">Add Banner</span></legend>
    <table>
        <tr>
            <td><label for="name">Name</label>
            </td>
            <td><input type="text" name="name" id="name" />
            </td>
        </tr>
        <tr>
            <td><label for="filename">Filename</label>
            </td>
            <td><input type="text" name="filename" id="filename" />
            </td>
        </tr>
        <tr>
            <td><label for="link">Link</label>
            </td>
            <td><input type="text" name="link" id="link" />
            </td>
        </tr>
        <tr>
            <td><label for="text">Text</label>
            </td>
            <td><input type="text" name="text" id="text" />
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <input type="submit" value="Add Banner" id="addKey"/>
            </td>
        </tr>
    </table>
</fieldset>
<?= form_close(); ?>
<script type="text/javascript">
    $(function() {
        $('span.delete').click(function() {
           bannerID = $(this).attr('data-bannerid');
           $.post('<?=base_url()?>admin/deleteBanner',{bannerID:bannerID},function(data) {
              if(data == 1) {
                  window.location.reload();
              } else {
                  alert(data);
              }
           });
        });
        $('span.toggleActivate').click(function() {
           bannerID = $(this).attr('data-bannerid');
           span = $(this);
           $.post('<?=base_url()?>admin/activateBanner',{bannerID:bannerID},function(data) {
              if(data == 1) {
                  if($(span).html() == 'activate') {
                      $(span).html('deactivate');
                  } else {
                      $(span).html('activate');
                  }
              } else {
                  alert(data);
              }
           });
        });
    });
</script>