<table>
    <?php foreach ($characters as $c) { ?>
        <tr>
            <td>
                <input type="radio" class="character" name="character" value="<?= $c->characterID?>"/>
            </td>
            <td>
                <img src="<?= $this->evelib->getCharacterImage($c->characterID, 64); ?>"/>
            </td>
            <td>
                <?= $c->characterName ?> &nbsp;
            </td>
            <td style="color:#9a9a9a">
                <?= $c->corporationName ?>
            </td>

        </tr>
    <?php } ?>
</table>
<script type="text/javascript">
    $('.character').click(function() {
        $('#signup').fadeIn(); 
    });
</script>