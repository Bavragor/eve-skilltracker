<h3 style="margin-bottom:10px;"><img src="img/pas-dot.png"/>SETTINGS</h3>

<fieldset>
<legend><span class="orange">Change Password</span></legend>
<table>
    <tr>
        <td><label for="oldPassword">Old password</label>
        </td>
        <td><input type="password" name="oldPassword" id="oldPassword" />
        </td>
    </tr>
    <tr>
        <td><label for="newPassword">New password</label>
        </td>
        <td><input type="password" name="newPassword" id="newPassword" />
        </td>
    </tr>
    <tr>
        <td><label for="newPassword2">New password (repeat)</label>
        </td>
        <td><input type="password" name="newPassword2" id="newPassword2" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <button id="savePassword">Save password</button>
            <span id="passwordSuccess" style="color: rgb(102, 126, 50); display: none;">Password changed successfully.</span>
        </td>
    </tr>
</table></fieldset>
<br>
<fieldset>
<legend><span class="orange">Set Main Character</span></legend>
Your main character, displayed in forums and badge comments: <br/><select name="mainCharacter" id="mainCharacter">
    <?php foreach ($characters as $c) { ?>
        <option <?= ($c->characterID == $this->session->userdata('mainCharacter')) ? 'selected="selected"' : '' ?>
            value="<?= $c->characterID ?>"><?= $c->characterName ?></option>
        <?php } ?>
</select><span id="mainCharacterSuccess" style="color: rgb(102, 126, 50); display: none;margin-left: 10px">Main character changed successfully.</span>

</fieldset>
<br>
<fieldset>
<legend><span class="orange">Manage API-Keys and Visibility</span></legend>
	<table width="600">
		<colgroup>
			<col width="35" />
			<col width="140" />
			<col width="35" />
			<col width="35" />
			<col width="35" />
			<col width="35" />
			<col width="35" />
			<col width="35" />
		</colgroup>
		<tr>
			<td colspan="8">Please read the <a href="/content/faq">FAQ</a> to see detailed informations about this settings!</td>
			</tr>
			<td colspan="8"><span class="hl-class-small">A</span> = You are anonym in rankings</td>
		</tr>
			<td colspan="8"><span class="hl-class-small">U</span> = Users can see my skills</td>
		</tr>
			<td colspan="8"><span class="hl-class-small">X</span> = FCs can see my skills and badges</td>
		</tr>
			<td colspan="8"><span class="hl-class-small">C</span> = CEO can see my skills and badges</td>
		</tr>
			<td colspan="8"><span class="hl-class-small">R</span> = Recruiters can see my skills and badges</td>
		</tr>
			<td colspan="8"><span class="hl-class-small">I</span> = Instructors can see my skills and badges</td>
		</tr>

	</table>
<?php foreach ($keys as &$key) { ?>
    <p>
        Characters of key
        <?= $key->keyID ?>
        (<span class="link_fake deleteKey" id="<?= $key->keyID ?>">Delete key</span>)
        <?php if(!$key->valid) {?>
        <br><span style="color:red;">Key invalid! Last Error:</span><br><span style="font-size:small;"> <?=$key->lastError?></span>
        <?php } ?>
    </p>
    <hr style="border-color:#ff6600;" />
    <table class="bordered" style="table-layout: fixed" width="605px;">
        <thead>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th class="privacyOpt">A</th>
            <th class="privacyOpt">U</th>
            <th class="privacyOpt">X</th>
            <th class="privacyOpt">C</th>
            <th class="privacyOpt">R</th>
            <th class="privacyOpt">I</th>

        </thead>
        <colgroup>
            <col width="35" />
            <col width="140" />
            <col width="35" />
            <col width="35" />
            <col width="35" />
            <col width="35" />
            <col width="35" />
            <col width="35" />
        </colgroup>


    <?php if (sizeof($key->characters) > 0) { ?>
        <?php foreach ($key->characters as $c) { ?>
            <tr>
                <td><img src="<?= $this->evelib->getCharacterImage($c->characterID, 32) ?>" />
                </td>
                <td>
                    <?= $c->characterName ?>
                </td>
                <td>
                    <button class="button updatePrivacy <?= (getPrivacy($c, 'A')) ? 'on' : 'off' ?>" id="<?= $c->characterID ?>" data-privacy="A"></button>
                </td>
                <td>
                    <button class="button updatePrivacy <?= (getPrivacy($c, 'U')) ? 'on' : 'off' ?>" id="<?= $c->characterID ?>" data-privacy="U"></button>
                </td>
                <td>
                    <button class="button updatePrivacy <?= (getPrivacy($c, 'X')) ? 'on' : 'off' ?>" id="<?= $c->characterID ?>" data-privacy="X"></button>
                </td>
                <td>
                    <button class="button updatePrivacy <?= (getPrivacy($c, 'C')) ? 'on' : 'off' ?>" id="<?= $c->characterID ?>" data-privacy="C"></button>
                </td>
                <td>
                    <button class="button updatePrivacy <?= (getPrivacy($c, 'R')) ? 'on' : 'off' ?>" id="<?= $c->characterID ?>" data-privacy="R"></button>
                </td>
                <td>
                    <button class="button updatePrivacy <?= (getPrivacy($c, 'I')) ? 'on' : 'off' ?>" id="<?= $c->characterID ?>" data-privacy="I"></button>
                </td>
            </tr>
        <?php } ?>
    <?php } else { ?>
            <tr><td colspan="5">The API-Key will be updated soon.</td></tr>
    <?php } ?>
    </table>
    <br>
<?php } ?>
</fieldset>


<br>
<fieldset>
    <legend><span class="orange">Additional API-Key</span></legend>
    <table>
        <tr>
            <td colspan="2">Get your API-Key <a
                    href="https://community.eveonline.com/support/api-key/" target="_blank">here</a><br />
                or create a new one <a
                    href="https://community.eveonline.com/support/api-key/CreatePredefined?accessMask=131080"
                    target="_blank">here</a>.
            </td>
        </tr>
        <tr>
            <td colspan="2">Only CharacterSheet and SkillInTraining is needed.
                Please, until now, set the key on 'No Expiry'. <br />
            </td>
        </tr>
        <tr>
            <td><label for="keyID">Key ID</label>
            </td>
            <td><input type="text" name="keyID" id="keyID" />
            </td>
        </tr>
        <tr>
            <td><label for="keyID">Verification Code</label>
            </td>
            <td><input type="text" name="vCode" id="vCode" size="64" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <button id="addKey">Add Key</button>
            </td>
        </tr>
    </table>
</fieldset>
<script type="text/javascript">
    $(function() {
		
       	$('span.deleteKey').click(function() {
            keyID = $(this).attr('id');
            $.post("users/deleteKey",{keyID: keyID},function(data) {
                if(data == 1) {
                    window.location.reload();
                } else {
                    alert(data);
                }
            });
        });
        
        $('span.addKey').click(function() {
            $('fieldset.addKey').fadeToggle();
        });
        $('#addKey').button({icons: {primary:"ui-icon-circle-plus"}}).click(function() {
            keyID = $('#keyID').val();
            vCode = $('#vCode').val();
            $(this).after('<img class="ajaxload" src="img/ajax-loader.gif">');
            $.post("users/addKey",{keyID: keyID, vCode: vCode},function(data) {
                $('.ajaxload').remove();
                if(data == 1) {
                    window.location.reload();
                } else {
                    alert(data);
                }
            });
        });
        $('#savePassword').button({icons: {primary:"ui-icon-refresh"}}).click(function() {
            oldPassword = $('#oldPassword').val();
            newPassword = $('#newPassword').val();
            newPassword2 = $('#newPassword2').val();
            $.post('users/changePassword',{oldPassword: oldPassword,newPassword: newPassword,newPassword2: newPassword2},function(data) {
                if(data == 1) {
                    $('#passwordSuccess').fadeIn().delay(1500).fadeOut();
                } else {
                    alert(data);
                }
            });
        });
        $('select#mainCharacter').change(function() {
            characterID = $(this).val();
            $.post('users/setMainCharacter',{characterID: characterID},function(data) {
                if(data == 1) {
                    $('#mainCharacterSuccess').fadeIn().delay(1500).fadeOut();
                } else {
                    alert(data);
                }
            });
        });
		
        $('button.button.on').button({icons: {primary:"ui-icon-check"}});
        $('button.button.off').button({icons: {primary:"ui-icon-closethick"}});
        $('button.updatePrivacy').button({
            text: false
        }).click(function() {
            var button = $(this);
            characterID = $(button).attr('id');
            sPrivacyType = $(button).attr('data-privacy');
            $.post('users/setUserPrivacy', {characterID: characterID, sPrivacyType: sPrivacyType}, function(data) {
                if(data > 0) {
                    if ($(button).hasClass('on')) {
                        $(button).removeClass('on').addClass('off').button({icons: {primary:"ui-icon-closethick"}}).children('span.ui-button-text');
                    } else {
                        $(button).removeClass('off').addClass('on').button({icons: {primary:"ui-icon-check"}}).children('span.ui-button-text');
                    }
                } else {
                    alert(data);
                }
            });
        });
    });
</script>