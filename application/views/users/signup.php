<script type="text/javascript">
    $(function() {
        $('input:button').button({
            icons: {
                primary: "ui-icon-arrowthick-1-e"
            }
        }).click(function() {
            key_ID = $('#keyID').val();
            v_Code = $('#vCode').val();
            $('#charactersDiv').html('<img src="img/load.gif"/>');
            $('#characters').fadeIn(200);
            $.post('users/signupCharacters', {keyID: key_ID,vCode: v_Code},function(data) {
                $('#charactersDiv').hide();
                $('#charactersDiv').html(data);
                $('#charactersDiv').fadeIn(1000);
            });
        });
        $('input:submit').button({
            icons: {
                primary:"ui-icon-info"
            }
        });
    });
</script>
<h3><img src="img/pas-dot.png" />Sign up</h3>
<div class="error"><?php echo validation_errors(); ?></div>
<div id="apikey">
    <?= form_open('users/signupsubmit') ?>
    <fieldset>
        <legend>Login</legend>
        <table>
            <tr>
                <td><label for="username">Username</label></td>
                <td><input type="text" name="signupUsername" id="signupUsername" autocomplete="off"/></td>
            </tr>
            <tr>
                <td><label for="password">Password</label></td>
                <td><input type="password" name="signupPassword" id="signupPassword" autocomplete="off"/></td>
            </tr>
            <tr>
                <td><label for="passwordConfirm">Confirm Password &nbsp;</label></td>
                <td><input type="password" name="signupPasswordConfirm" id="signupPasswordConfirm"/></td>
            </tr>
            <tr>
            <td></td><td>  Do NOT use your EVE username and/or password.</td>
</tr>    
            </table>
    </fieldset>
    <fieldset>
        <legend>API-Key</legend>
        <table>
            <tr>
                <td colspan="2">
                    Get your API-Key <a href="https://community.eveonline.com/support/api-key/" target="_blank">here</a><br/>
                    or create a new one <a href="https://community.eveonline.com/support/api-key/CreatePredefined?accessMask=131080" target="_blank">here</a>.
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Only <b>CharacterSheet</b> and <b>SkillInTraining</b> is needed, so you don't have to worry about spying your corp, account, wallet and assets or something else... 

                </td>
            </tr>
            <tr>
                <td>
                    <label for="keyID">Key ID</label>
                </td>
                <td>
                    <input type="text" name="keyID" id="keyID"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="keyID">Verification Code</label>
                </td>
                <td>
                    <input type="text" name="vCode" id="vCode" size="64"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="button" id="load" value="Load Characters"/>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset id="characters" style="display:none">
        <legend>Characters</legend>
        Please select your main character.
        <div id="charactersDiv">
        </div>
    </fieldset>
    <div id="signup" style="display:none;">
        <input type="submit" value="Sign Up"/>
        This may take a few seconds.
    </div>
    <?= form_close(); ?>
</div>