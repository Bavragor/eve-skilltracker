<script type="text/javascript">
	$(function() {
		$('tr.hoverable').click(function() {
			id = $(this).attr('id');
			window.location.href = '<?= base_url();?>users/changeCharacter/' + id;
		});
	});
</script>
<table class="bordered">
	<?php foreach ($characters as $c) { ?>
		<tr id="<?= $c->characterID ?>" class="hoverable <?=$c->characterID == $this->session->userdata('characterID') ? 'active' : '';?>">
			<td>
				<img src="<?= $this->evelib->getCharacterImage($c->characterID, 64); ?>" />
			</td>
			<td>
				<?= $c->characterName ?><br/>
				<span style="color: #CCCCCC; font-size: smaller;"> <?= $c->corporationName ?></span>
			</td>
		</tr>
	<?php } ?>
</table>
