<script type="text/javascript">
    $(function() {
        $("#dialogConfirmDeleteAccount").dialog({
            resizable: false,
            height:230,
            modal: true,
            autoOpen: false,
            buttons: {
                "Delete account": function() {
                    window.location.href = 'users/deleteAccount';
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                } 
            }
        });
        $('#deleteAccount').click(function() {
            $("#dialogConfirmDeleteAccount").dialog("open");
        });
    });
</script>
<div id="dialogConfirmDeleteAccount" title="Delete the account?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>You will permanently delete your account together with all options, characters and keys you set up. Are you sure?</p>
</div>
<h3><img src="img/pas-dot.png" />Navigation</h3>
<ul style="list-style:none;">
    <?php if ($this->session->userdata('userID') != 0) { ?><li><img src="img/act-dot.png" /> <span class="link_fake_underline" id='deleteAccount'>delete account</span><b><span class="text_red"> ! </span></b></li><?php } ?>
</ul>