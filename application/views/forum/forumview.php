<style type="text/css">
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
        opacity: 1.0;
    }
	table {
    background-color: transparent;
    border-collapse:collapse;
    margin: 2px 0;
}
td {
    border:none;
    padding:2px 5px;
}
td table {
    margin-left:20px;
}

table.bordered td, table.bordered th {
    border: 1px solid #7b7b7b;
    padding:2px 10px;
}
table.rowbordered tr {
    border:1px solid #7b7b7b;
}
table.rowbordered td, table.rowbordered th {
    padding:2px 10px;
}
</style>
<h3><img src="img/act-dot.png" /> Forum</h3>
<table class="rowbordered" id="topictable" style="margin-top:20px; table-layout:fixed;">
    <colgroup>
        <col width="34"/>
        <col width="300" />
        <col width="100"/>
        <col width="156"/>
        <col width="30"/>
        
    </colgroup>
    <tr style="border:none;">
        <th></th>
        <th><img src="img/pas-dot.png" /> Topic</th>
        <th><img src="img/pas-dot.png" /> Posts</th>
        <th><img src="img/pas-dot.png" /> Last post</th>
        <th></th>
    </tr>
    <?php if (sizeof($topics) == 0) { ?>
        <tr>
            <td colspan="3">
                No topics
            </td>
        </tr>
    <?php } else {
        foreach ($topics as $t) { ?>
            <tr>
                <td>
                    <button id="<?=$t->topicID?>" class="<?= ($t->sticky) ? 'sticky' : 'topic'; ?>"><?= $t->topic ?></button>
                <td>
                    <a href="forum/topic/<?= $t->topicID ?>"><?= $t->topic ?></a><br/>
                    <span style="font-size:10pt">started by <b><?= $t->characterName ?></b> (<?= date('d.m.Y G:i', $t->started) ?>)</span>
                </td>
                <td style="color:#7b7b7b;">
                     <span style="font-size:10pt"><center><?= $t->count ?></center></span>
                </td>
                <td style="padding-left:50px; color:#7b7b7b;">
                     <span style="font-size:10pt">
					 <?= (date('Y-d-m') == date('Y-d-m',$t->last->date)) ? date('G:i', $t->last->date) : date('d.m.Y G:i',$t->last->date)?>
					 by <br/>
					 <?= $t->last->characterName?>
					 </span>
                </td>
                <td style="padding:0">
					<?php if ($this->session->userdata('admin') == 1) { ?>
						<button class="delete"><?=$t->topicID?></button>
					<?php } ?>
                </td>
            </tr>
        <?php }
    } ?>
</table>
<?php if ($this->session->userdata('userID') != 0) { ?>
<p align="right"><a href=forum>&uArr; Back to top</a></p>
<?= form_open('forum/addTopic'); ?>
<fieldset style="margin-top:20px;">
    <legend>Create new topic</legend>
    <table>
        <tr>
            <td><label for="topic">Topic:</label></td>
            <td><input style="width:500px;" type="text" name="topic" id="topic"/></td>
        </tr>
        <tr>
            <td><label for="text">Text:</label></td>
            <td><textarea style="width:500px; color:black;" rows="5" name="text" id="text"></textarea></td>
        </tr>
        <tr>
            <td/>
            <td>
                <input type="submit" name="submit" id="submit" value="Create" />
            </td>
        </tr>
    </table>
    
</fieldset>

<?= form_close() ?>
<?php } ?>

<?php if ($this->session->userdata('admin') == 1) {?>
	<div id="dialogConfirmDeleteTopic" title="Delete the topic?">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>You will permanently delete the topic including all posts. Are you sure?</p>
	</div>
<?php } ?>

<script type="text/javascript">
    $(function() {
        $('#submit').button();
        $('button.topic').button({
            icons: {
                primary:"ui-icon-arrowthick-1-e"
            },
            text: false <?= (!$this->session->userdata('admin')) ? ',disabled: true' : ''; ?>
        });
        $('button.sticky').button({
            icons: {
                primary:"ui-icon-pin-s"
            },
            text: false  <?= (!$this->session->userdata('admin')) ? ',disabled: true' : ''; ?>
        });
        <?php if ($this->session->userdata('admin')) { ?>
            $('button.sticky, button.topic').click(function() {
                id = $(this).attr('id');
                $.post('forum/stickyTopic',{topicID: id}, function(data) {
                   window.location.reload(); 
                });
            });
			$('button.delete').button({
				icons: {
					primary:"ui-icon-trash"
				},
				text: false
			}).click(function() {
                id = $(this).attr('title');
				$("#dialogConfirmDeleteTopic").dialog("open");
            });
			
			$("#dialogConfirmDeleteTopic").dialog({
				resizable: false,
				height:230,
				modal: true,
				autoOpen: false,
				buttons: {
					"Delete topic": function() {
						$.post('forum/deleteTopic',{topicID: id}, function(data) {
						   window.location.reload(); 
						});
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					} 
				}
			});
        <?php } ?>
    });
</script>