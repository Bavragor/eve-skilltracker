

<?php if(!isset($blFromBadge)) { ?>
<h3><img src="img/act-dot.png" /> Forum</h3>
<p align="right"><a href="forum">&lArr; Back to forum</a></p>
<?php } ?>

<?php
    if(!isset($blFromBadge))
    {
        echo '<h3 style="color:#ff6600;"><img src="img/pas-dot.png" />'.$topic->topic.'</h3>';
    }
?>
<?php foreach ($comments as $c) { ?>
	<div class="comment-header">
		<p class="comment-header-date">
			<img src="img/pas-dot.png" /> <?= date('d.m.Y G:i', $c->date) ?>:
		</p>
		<p class="comment-header-right">
			<?php if ($this->session->userdata('admin') == 1) { ?>
				<img src="img/act-dot.png" /><span style="margin-right:20px;" class="link_fake_underline delete" title="<?= $c->commentID ?>">delete</span>
			<?php } ?>
			<a name="<?= $c->commentID ?>" href="<?php if(!isset($blFromBadge)){echo 'forum/topic/'.$topic->topicID;}else{echo 'badges/show/'.$badge->badgeID;} ?>#<?= $c->commentID ?>"/>&dArr;</a>
		</p>
	</div>
	<div class="comment-body">

		<div class="comment-left">
			<span style="color:#ff6600; font-weight:300;"><?= $c->characterName ?></span><hr style="display: block; height: 1px;
    border: 0; border-top: 1px solid #ff6600;
    margin: 1em 0; padding: 0;" />
			<img src="<?= $this->evelib->getCharacterImage($c->mainCharacter,64)?>"/>
		</div>
		<div class="comment-right">
			<?= nl2br($c->text) ?>
		</div>

	</div>
	<div style="clear:both;"></div>
<?php } ?>
<?php if(!isset($blFromBadge)) { ?>
    <p align="right"><a href="forum">&lArr; Back to forum</a></p>
<?php } ?>

<?php if ($this->session->userdata('userID') != 0) { ?>
	<?php
	if(isset($blFromBadge))
	{
		echo form_open('forum/addCommentForBadge');
	}
	else
	{
		echo form_open('forum/addComment');
	}
	?>
	<fieldset style="clear:left; margin-top:20px;">
		<legend>Add comment</legend>
		<table>
            <?php if(validation_errors() || isset($blFromBadge)){ ?>
                <tr>
                    <td colspan="2">
                        <?php
                            if(!isset($blFromBadge))
                            {
                                echo validation_errors();
                            }
                            else
                            {
                                echo $this->session->flashdata('validation_errors');
                            }
                        ?>
                    </td>
                </tr>
            <?php } ?>
			<tr>
				<td><label for="text">Text:</label></td>
				<td><textarea style="width:500px;" rows="5" name="text" id="text"></textarea></td>
			</tr>
			<tr>
				<td/>
				<td>
					<?php if(!isset($blFromBadge)) { ?>
					<input type="hidden" name="topicID" id="topicID" value="<?= $topic->topicID ?>"/>
					<?php } else { ?>
					<input type="hidden" name="badgeID" id="badgeID" value="<?= $badge->badgeID ?>"/>
					<?php } ?>
					<input type="submit" name="submit" id="submit" value="Add" />
				</td>
			</tr>
		</table>
	</fieldset>
	<?= form_close() ?>

<?php if ($this->session->userdata('admin') == 1) { ?>
	<div id="dialogConfirmDeleteComment" title="Delete the comment?">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>You will permanently delete the comment. Are you sure?</p>
	</div>
<?php } ?>

<script type="text/javascript">
	$(function() {
		$('#submit').button();

		<?php if ($this->session->userdata('admin')) { ?>
		$('span.delete').click(function() {
			id = $(this).attr('title');
			$("#dialogConfirmDeleteComment").dialog("open");
		});

		$("#dialogConfirmDeleteComment").dialog({
			resizable: false,
			height:230,
			modal: true,
			autoOpen: false,
			buttons: {
				"Delete comment": function() {
					$.post('forum/deleteComment',{commentID: id}, function(data) {
						window.location.reload();
					});
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			}
		});
		<?php } ?>
	});
</script>

<?php } ?>