<?= form_open('content/savePage') ?>
<input type="hidden" name="contentID" value="<?= $oPage->contentID ?>" />
<label for="contentName">Page Title: </label><input type="text" class="ui-corner-all" name="contentName" value="<?= $oPage->contentName ?>" />
<hr style="display: block; height: 4px;
    border: 0; border-top: 4px solid #ff6600;
    margin: 1em 0; padding: 0; " />
<textarea name="contentLongtext"><?= $oPage->contentLongtext ?></textarea>
<hr style="display: block; height: 4px;
    border: 0; border-top: 4px solid #ff6600;
    margin: 1em 0; padding: 0; " />
<button type="submit" style="float:right;">Save Page</button>
<?= form_close() ?>

<!-- TinyMCE -->
<script type="text/javascript" src="/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('button').button();
    });
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        width: 630,
        height: 400,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ]
    });
</script>