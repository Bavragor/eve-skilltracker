<h3 style="float:left;">All Pages</h3><a class="button" href="/content/create" style="float:right;color:#333333;top:10px;">Create new page</a>
<hr style="display: block; height: 4px;
    border: 0; border-top: 4px solid #ff6600;
    margin: 1em 0; padding: 0;clear:both;" />
<ul style="position: relative;">
    <?php
        foreach($mAllPages as $aSinglePage)
        {
            echo '<li style="margin-bottom: 10px;"><a href="/content/'.strtolower($aSinglePage->contentName).'" title="'.$aSinglePage->contentName.'">'.$aSinglePage->contentName.'</a><a href="/content/edit/'.$aSinglePage->contentID.'" class="button edit-page">ändern</a><a href="/content/delete/'.$aSinglePage->contentID.'" class="button delete-page">X</a></li>';
        }
    ?>
</ul>
<script type="text/javascript">
    $(document).ready(function() {
        $('.button').button();
    });
    $('.delete-page').button({
        icons: {
            primary: "ui-icon-trash"
        },
        text: false
    });
    $('.edit-page').button({
        icons: {
            primary: "ui-icon-pencil"
        },
        text: false
    });
</script>