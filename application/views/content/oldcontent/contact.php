<div class="info">
    <h3><img src="img/pas-dot.png"/>Contact</h3>

    <p>Because we are always grateful for any praise, critics or ideas for improvement, we offer multiple possibilities of getting in contact with us.</p>

    <ul>
        <li>eMail: <a href="mailto:contact@eve-skilltracker.com?Subject=Skilltracker">contact@eve-skilltracker.com</a></li>
        <li>EVEmail: <a href="https://gate.eveonline.com/Mail/Compose/Barkkor" target="_blank">Barkkor</a></li>
    </ul>

    <p>We also provide a public ingame chat channel and an ingame mailing list, which, of course, you are cordially invited to join.</p>
    <ul>
        <li>Chatchannel: <span class="orange">Skilltracker</span></li>
        <li>Mailinglist: <span class="orange">Skilltracker</span></li>
    </ul>
    
    <p>You are also welcome to visit the forum to discuss with us and other pilots about the Skilltracker, the badges or the awards.</p>
    <ul><li>Forum: <a href="forum">Skilltracker</a> <span style="color: #CCCCCC; font-size: 12px; font-weight:300;">(You must be logged in to comment)</span></li></ul>

    <p>And if this still is not enough for you, you can follow us on Facebook and Twitter:</p>
    <ul>
        <li>Facebook: <a href="https://www.facebook.com/EveSkilltracker" target="_blank">EveSkilltracker</a></u></li>
        <li>Twitter: <a href="https://twitter.com/Skilltracker" target="_blank">@Skilltracker</a></u></li>
    </ul>
  
    
    <p>Fly safe, <br>
    the Skilltracker Team.</p>
</div>