<div class="info<?= ($oPage->longversion) ? ' long' : '' ?>">
    <?php if ($this->session->userdata('admin') == 1) { ?>
        <a href="/content/edit/<?= $oPage->contentID ?>" title="Edit Page: <?= $oPage->contentName ?>" style="float:left;">Edit</a>
        <a href="/content" title="Back to Overview" style="float:right;">Back to Overview</a>
    <?php } ?>
    <div style="clear: both;"></div>
    <?php
        echo $oPage->contentLongtext;
    ?>
</div>