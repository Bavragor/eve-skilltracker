<?php
if(!$blAsAjax) {
?>
<div class="badges-wrapper">
<div id="badgeRankSelect">
<label>Filter Badges:</label>
    <select name="badgeRankSelect">
	    <option>---</option>
	    <option value="nobadges">- No Badges -</option>
        <option value="basic">Basic</option>
        <option value="medium">Medium</option>
        <option value="expert">Expert</option>
    </select>
</div>
<div class="badges">
<?php
}
else {
    echo '<div class="badges">';
}
?>
<h3 style="color:#e2e2e2; font-size:20px;"><img src="img/pas-dot.png" />Badges <span class="orange">//</span> OPEN BETA</h3><hr style="display: block; height: 1px;
    border: 0; border-top: 1px solid #ff6600;
    margin: 1em 0; padding: 0;">
<p class='toggleButtons'><button id="toggleAll">Toggle all</button></p>
<div>
    <?php
    $lastCategory = 1;
    $i = 0;
    foreach ($badges as $b) {
        if ($b->categoryID != $lastCategory) {
            ?>
        </table>
        <?= ($b->level == 1) ? '</div>' : ''; ?>
		<?php if($b->level < 3) { ?>

        <h<?= $b->level + 1 ?>
            style="margin-left:<?= $b->level * 15 ?>px; margin-bottom:0px;"
            <?php if ($b->level == 1) {?>
            	class="toggle link_fake" id="<?=pow(2,$i)?>"
            <?php $i++; }?>
			>
			<?php if ($b->level ==1){
				echo '<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e" style="display: inline-block;"></span>';
				}else if ($b->level ==2){
				echo '<img src="img/act-dot.png">';
				}

				?>

				<?= $b->categoryName ?>
        </h<?= $b->level + 1; ?>>
		<?php } ?>

        <?= ($b->level == 1) ? '<div style="display:none">' : ''; ?>








		<?php } if (isset($b->badgeID)) {

        echo '<div style="margin-left:', $b->level * 15, 'px;">';

		?>

	    <?php
	        if(isset($badgeRankType)) {
		        if($badgeRankType == $b->rankID) {
		?>
        <div style="margin-bottom:5px;">
        <a href="badges/show/<?= $b->badgeID ?>">
		<img src="<?= base_url() . $this->evelib->getIcon($b->iconFile) ?>" height="24px" style="vertical-align: bottom;"/>
		</a>
		<a class="link_invisble" href="badges/<?= strtolower($b->badgeName) ?>">
		<?= $b->badgeName ?>
		<?php if($b->level == 3) {?>
		<span class="orange">//</span> <?=$b->categoryName;?>
		<?php } ?>
		</a>
        </div>



		<?php if (isset($characterID)) {
                        foreach ($b->ranks as $r) { ?>
                            <div class="progress" style="float:left; border:1px solid #ccc; height:15px; width: <?= 40 * $r->rankID ?>px; margin-top:0px;
                            padding:0px;">
                                <div style="float:left; margin: 0px; height:15px;
                                     width: <?php
                            if ($b->skillpoints >= $r->maxSP) {
                                echo 39 * $r->rankID + $r->rankID - 0;
                            } else if ($b->maxSP == 0 || $r->maxSP == $r->lastMaxSP) {
                                echo '0';
                            } else {
                                echo round(($b->skillpoints - $r->lastMaxSP) / ($r->maxSP - $r->lastMaxSP ) * 39 * $r->rankID);
                            }
                            ?>px;
                                     background-color: <?= ($b->skillpoints >= $r->maxSP) ? '#ff6600' : '#4d4d4d' ?>;"></div>
                            </div>
            <?php }
        } ?>





		<div style="margin-left:350px; margin-top:-3px; position:absolute; color:#7b7b7b;">
		<?php
                if (isset($characterID)){


					if ($b->rankName == 'Basic'){
						echo '<span style="color:#ff6600; font-weight:600;">Basic</span> <span class="orange">//</span> Medium <span class="orange">//</span> Expert';
					}else if ($b->rankName == 'Medium'){
						echo 'Basic <span class="orange">//</span> <span style="color:#ff6600; font-weight:600;">Medium</span> <span class="orange">//</span> Expert';
					}else if ($b->rankName == 'Expert'){
						echo 'Basic <span class="orange">//</span> Medium <span class="orange">//</span> <span style="color:#ff6600; font-weight:600;">Expert</span>';
					}else{
						echo 'Basic <span class="orange">//</span> Medium <span class="orange">//</span> Expert';
					}




				}


        ?>
        </div><br />

		<?php
		        }
	        }
		    else
		    {
		?>
			    <div style="margin-bottom:5px;">
				    <a href="badges/show/<?= $b->badgeID ?>">
					    <img src="<?= base_url() . $this->evelib->getIcon($b->iconFile) ?>" height="24px" style="vertical-align: bottom;"/>
				    </a>
				    <a class="link_invisble" href="badges/<?= strtolower($b->badgeName) ?>">
					    <?= $b->badgeName ?>
					    <?php if($b->level == 3) {?>
						    <span class="orange">//</span> <?=$b->categoryName;?>
					    <?php } ?>
				    </a>
			    </div>



			    <?php if (isset($characterID)) {
			    foreach ($b->ranks as $r) { ?>
				    <div class="progress" style="float:left; border:1px solid #ccc; height:15px; width: <?= 40 * $r->rankID ?>px; margin-top:0px;
					    padding:0px;">
					    <div style="float:left; margin: 0px; height:15px;
						    width: <?php
					    if ($b->skillpoints >= $r->maxSP) {
						    echo 39 * $r->rankID + $r->rankID - 0;
					    } else if ($b->maxSP == 0 || $r->maxSP == $r->lastMaxSP) {
						    echo '0';
					    } else {
						    echo round(($b->skillpoints - $r->lastMaxSP) / ($r->maxSP - $r->lastMaxSP ) * 39 * $r->rankID);
					    }
					    ?>px;
						    background-color: <?= ($b->skillpoints >= $r->maxSP) ? '#ff6600' : '#4d4d4d' ?>;"></div>
				    </div>
			    <?php }
		    } ?>





			    <div style="margin-left:350px; margin-top:-3px; position:absolute; color:#7b7b7b;">
				    <?php
				    if (isset($characterID)){


					    if ($b->rankName == 'Basic'){
						    echo '<span style="color:#ff6600; font-weight:600;">Basic</span> <span class="orange">//</span> Medium <span class="orange">//</span> Expert';
					    }else if ($b->rankName == 'Medium'){
						    echo 'Basic <span class="orange">//</span> <span style="color:#ff6600; font-weight:600;">Medium</span> <span class="orange">//</span> Expert';
					    }else if ($b->rankName == 'Expert'){
						    echo 'Basic <span class="orange">//</span> Medium <span class="orange">//</span> <span style="color:#ff6600; font-weight:600;">Expert</span>';
					    }else{
						    echo 'Basic <span class="orange">//</span> Medium <span class="orange">//</span> Expert';
					    }




				    }


				    ?>
			    </div><br />

		<?php
		    }
		?>

		<?php

		echo '</div>';


		}

        $lastCategory = $b->categoryID;
	    $lastLevel = $b->level;
    }
    ?>

</div>
<?php
if(!$blAsAjax) {
?>
</div>
</div>
<?php
}
else {
 echo '</div>';
}
?>

<script type="text/javascript">
    $(function() {
        $('.toggle').click(function() {
           $(this).children('span').toggleClass( 'ui-icon-triangle-1-e ui-icon-triangle-1-s');
           $(this).next().slideToggle();
           toggleMask = toggleMask ^ $(this).attr('id');
           $.address.value(toggleMask.toString(16));
           $.address.update();
        });
		$('#toggleAll').button({icons: {primary: 'ui-icon-carat-2-n-s'}}).click(function() {
			$('.toggle + div').slideToggle();
			toggleMask = toggleMask ^ <?= pow(2,$i)-1?>;
			$.address.value(toggleMask.toString(16));
			$.address.update();
		});
		$.address.autoUpdate(false).change(function(event) {
			toggleMask = parseInt(event.value.substring(1), 16);
			$('.toggle').each(function() {
				id = $(this).attr('id');
				if((toggleMask & id) == id) {
					$(this).next().show();
				} else {
					$(this).next().hide();
				}
			});
        });
    });
    $(document).ready(function(){
        $('select[name="badgeRankSelect"]').change(function(){
            $.ajax({
                url: '/badges/' + $(this).val() + '/1',
                success: function(data) {
                    $('.badges-wrapper .badges').html(data);
                    $(this).val($(this).val());
                }
            });
        });
    });
</script>