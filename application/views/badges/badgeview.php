<div class="badges">
	<h3 style="color:#e2e2e2; font-size:20px;"><img src="img/pas-dot.png" /><?= $ApiUpdate ?><?= $badge->badgeName ?></h3>
	<?php if (isset($badge->typeID) && $badge->typeID > 0) { ?>
		<div class="imagelink"><?= $this->evelib->linkShowInfo($badge->typeID) ?><img src="<?= $this->evelib->getImagePath($badge->typeID, 128) ?>"/></span>
			<?php

			$blSkillNameSet = false;
			$sLastSkillID = '0000';

			foreach($traits as $oTrait)
			{

				if($oTrait->skillID != $sLastSkillID)
				{

					if($oTrait->skillID == '-1')
					{
						$sSkillName = 'Role Bonus';
					}
					else
					{
						$sSkillName = $this->badgemodel->getSkillName($oTrait->skillID);
						$sSkillName = $sSkillName->typeName;

					}
					$blSkillNameSet = true;
					if($blSkillNameSet)
					{
						echo '</ul>';
					}
					echo '<b>'.$sSkillName.'</b><ul>';
				}
				else
				{
					$blSkillNameSet = false;
				}
				/**
				 * Normale showinfo Links ändern zu dem CCP Ingame Browser Pendant
				 */
				if(!empty($oTrait->bonusText))
				{
					$aResults = array();
					$sRegex = "/<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>/siU";
					$blTraitLink = preg_match_all($sRegex, $oTrait->bonusText, $aResults, PREG_SET_ORDER);
					if($blTraitLink)
					{
						foreach($aResults as $sIndex => $aSingleLink)
						{
							$sReplacement = '<a style="cursor:pointer;" onclick="CCPEVE.showInfo('.str_replace('showinfo:', '', $aSingleLink[2]).');">'.$aSingleLink[3].'</a>';
							$oTrait->bonusText = preg_replace_nth($sRegex, $sReplacement, $oTrait->bonusText, $sIndex+1);
						}
					}
				}
				if(is_numeric($oTrait->bonus))
				{
					echo '<li>'.$oTrait->bonus.'% '.ucfirst($oTrait->bonusText).'</li><br>';
				}
				else
				{
					if(!$oTrait->bonus)
					{
						echo '<li>'.ucfirst($oTrait->bonusText).'</li><br>';
					}
					else
					{
						echo '<li>'.$oTrait->bonus.$oTrait->bonusText.'</li><br>';
					}
				}

				$sLastSkillID = $oTrait->skillID;

			}
			?>
		</div>
	<?php } ?>
		<hr style="display: block; height: 4px;
    border: 0; border-top: 4px solid #ff6600;
    margin: 1em 0; padding: 0; " />
		<?php if (isset($characterID) && $characterID > 0) { ?>
			<p>Your progress:  <span class="orange"><?= ($badge->rankID == 0) ? 'None' : $badge->rankName; ?></span></p>

			<?php foreach ($ranks as $r) { ?>
				<div class="progress" style="float:left; border:1px solid #ccc; height:14px; width: <?= 51 * $r->rankID ?>px; margin: 5px 0 5px 0;padding:0px;">
					<div style="float:left; margin: 0px; height:14px;
						width: <?php
					if ($badge->skillpoints >= $r->maxSP) {
						echo 50 * $r->rankID + $r->rankID - 0;
					} else if ($r->maxSP == $r->lastMaxSP) {
						echo '0';
					} else {
						echo round(($badge->skillpoints - $r->lastMaxSP) / ($r->maxSP - $r->lastMaxSP ) * 50 * $r->rankID);
					}
					?>px;
						background-color: <?= ($badge->skillpoints >= $r->maxSP) ? '#ff6600' : '#4d4d4d' ?>;"></div>
				</div>
			<?php } ?>
			<div style="clear:left;"></div>
		<?php } else { ?>

		<?php } ?>


	<hr style="display: block; height: 4px;
    border: 0; border-top: 4px solid #ff6600;
    margin: 1em 0; padding: 0; " />


	<?php foreach ($ranks as $r) { ?>

		<h3 class="toggle" data-rank="<?= $r->rankName ?>">
			<img src="<?= base_url(); ?>badgeIcons/45_64_<?= 13 + $r->rankID ?>.png" width="32" height="32" style="vertical-align:-40%;"/>
			<?= $r->rankName ?>

			<a style="margin-left:10px;" href="<?= base_url() ?>badges/evemon/<?= $badge->badgeID ?>/<?= $r->rankID ?>" class="trigger">
				<img style="vertical-align: -10%;" src="<?= base_url() ?>/img/evemonSmaller.png"/>
			</a>
			<div class="tooltip"><span class="text_red">Note: </span>Evemon export XML: The plan contains all &QUOT;or&QUOT;-alternatives that might be included in the badge or subbadges.</div>
		</h3>

		<hr style="display: block; height: 1px;
    border: 0; border-top: 1px solid #ff6600;
    margin: 1em 0; padding: 0; " />
        <div data-rank="<?= $r->rankName ?>">
		<?= $r->view ?>
        </div>
	<?php } ?>
</div>
<div id="comments">

<?= (isset($comments)) ? $comments : '' ?>

</div>
<div id="dialog-skillinfo"></div>
<style type="text/css">
	.expand:hover {
		color:#FFCE0E;
		cursor: pointer;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('.trigger').tooltip({relative:true, position:'bottom right'});
		$('.expand').click(function () {
            $(this).children('span').toggleClass( 'ui-icon-triangle-1-e ui-icon-triangle-1-s');
			var title = $(this).attr('title');
			if ($('tr#' + title).length == 0) {
				var tr = $(this).closest('tr');
				$.get('badges/showRank/' + title,function(data) {

					var sub = '<tr id="' + title + '"><td colspan="3">' + data + '</td></tr>';
					tr.after(sub);

				});
			} else {
				$('tr#' + title).toggle();
			}
		});
        /*$('.toggle').on('click', function(){
           $(this).parent().find('div[data-rank="'+ $(this).attr('data-rank') +'"]').children('table').toggle();
           $(this).children('span').toggleClass( 'ui-icon-triangle-1-e ui-icon-triangle-1-s');
        });*/

	});
    function showUrlInDialog(typeID){
        $("#dialog-skillinfo").dialog('destroy');
        $.ajax({
            url: '/ajax_skillinfo/show/' + typeID,
            success: function(data) {
                $("#dialog-skillinfo").attr('title', $(document).find('a[data-id="'+typeID+'"]').attr('data-title'));
                $("#dialog-skillinfo").html(data)
                .dialog({
                    resizable: false,
                    height:230,
                    modal: true,
                    autoOpen: false
                }).dialog('open');
            }
        });
    }
</script> 