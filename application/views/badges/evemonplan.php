<?php 
header('Content-Type: text/xml'); 
header('Content-Disposition: attachment; filename="' . $name . '.xml"');
?>
<?= '<?xml version="1.0"?>' ?>
<plan xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="<?= $name ?>" revision="4016">
    <?php foreach ($skills as $s) { ?>
        <entry skillID="<?= $s->typeID ?>" skill="<?= $s->typeName ?>" level="<?= $s->level ?>"></entry>
    <?php } ?>
</plan>