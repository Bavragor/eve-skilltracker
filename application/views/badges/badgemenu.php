<h3><img src="img/pas-dot.png" />Awards:</h3>
<?php if (isset($badgeOverview)) {?>
<ul>
	<?php $i = 0;?>
	<li style="margin-bottom:20px;"><img src="badgeIcons/45_64_16.png" align="absmiddle" alt="EXPERT" height="32" width="32" />
		<?php
		if ($badgeOverview[$i]->rankID == 3) {
			echo $badgeOverview[$i]->c;
			$i++;
		} else {
			echo 0;
		}
		?><a href="/badges/expert" title="Medium Badges">&nbsp;Expert Badges</a></li>
	<li style="margin-bottom:20px;"><img src="badgeIcons/45_64_15.png" align="absmiddle" alt="MEDIUM"
		height="32" width="32" /> <?php
		if ($badgeOverview[$i]->rankID == 2) {
			echo $badgeOverview[$i]->c;
			$i++;
		} else {
			echo 0;
		}
		?><a href="/badges/medium" title="Medium Badges">&nbsp;Medium Badges</a></li>
	<li style="margin-bottom:20px;"><img src="badgeIcons/45_64_14.png" align="absmiddle" alt="BASIC"
		height="32" width="32" /> <?php
		if ($badgeOverview[$i]->rankID == 1) {
			echo $badgeOverview[$i]->c;
			$i++;
		} else {
			echo 0;
		}
		?><a href="/badges/basic" title="Medium Badges">&nbsp;Basic Badges</a></li>

</ul>
<?php } else {?>
<p class="text_block">If you are logged in, you will see how many badges of each rank you have reached yet.</p>
<?php }?>