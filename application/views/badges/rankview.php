<table style="table-layout: fixed; margin-left:37px;" width="450">
    <colgroup>
        <col width="250"/>
        <col width="100"/>
        <col width="100"/>
    </colgroup>
    <tbody>
        <?php
        $lastGroup = 0;
        foreach ($r->skills as $s) {
            ?>
            <tr>
                <td>
                    <?php if ($s->skillGroup == $lastGroup) { ?>
                        <span class='or'>or:</span>
                    <?php } ?>
                    <?php
                        if(stripos($_SERVER['HTTP_USER_AGENT'], 'EVE-IGB'))
                        {
                            echo $this->evelib->linkShowInfo($s->typeID).$s->typeName.'</span>';
                        }
                        else
                        {
                            echo '<a id="show-skillinfo" data-id="'.$s->typeID.'" data-title="'.$s->typeName.'" onclick="showUrlInDialog('.$s->typeID.');">'.$s->typeName.'</a>';
                        }
                    ?>
                    </span>
                </td>
                <td>
                    <img src="<?= base_url() ?>/img/skillbar/l<?= $s->level ?>.png"/>
                </td>

                <td>
                    <?php if (isset($characterID) && $characterID > 0) { ?>
                        <img src="<?= base_url() ?>/img/skillbar/<?php
                if ($s->characterLevel == 0) {
                    echo 'r0';
                } else if ($s->characterLevel >= $s->level) {
                    echo 'g' . $s->characterLevel;
                } else {
                    echo 'r' . $s->characterLevel;
                }
                        ?>.png"/>
                         <?php } ?>
                </td>

            </tr>
            <?php
            $lastGroup = $s->skillGroup;
        }
        $lastGroup = 0;
        foreach ($r->subbadges as $s) {
            ?>
            <tr>
                <td>
                    <?php if ($s->badgeGroup == $lastGroup) { ?>
                        <span class='or'>or:</span>
                    <?php } ?>

                    <span class="link_fake expand" title="<?= $s->badgeID . '-' . $s->rankID ?>">
                        <span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e" style="display: inline-block;"></span>&nbsp;
                        <?= $s->badgeName ?>
                    </span>
                </td>
                <td>
                    <span><?= $s->rankName ?></span></div>
                </td>
                <td>
                    <?php if (isset($characterID) && $characterID > 0) { ?>
                        <span style="color: <?= ($s->characterRankID >= $s->rankID) ? '#ff6600' : '#7b7b7b' ?>">
                            <?= ($s->characterRankID == 0) ? 'None' : $s->characterRank; ?>
                        </span>
                    <?php } ?>

                </td>
            </tr>
            <?php
            $lastGroup = $s->badgeGroup;
        }
        ?>
    </tbody>
</table>