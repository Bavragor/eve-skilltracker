<h3><img src="img/pas-dot.png" />Overview</h3>

<a href="/overview/skills"><img src="<?= $this->evelib->getCharacterImage($characterID,64)?>" style="float:left; margin-right:10px; border: 1px solid #ff6600;"/></a>
<span style="color:#ff6600; font-weight:600; text-transform:uppercase;"><?= $characterName ?></span><br>
<span style="color: #7b7b7b; font-size: smaller;"><?= $char->corporationName ?></span><br /><div style="margin-top:30px;">
<span class="orange"><?= $sum->skillFiveCount?></span> skills at level V, total <span class="orange"><?= number_format($sum->skillpoints,0,'','.') ?></span> skillpoints and <span class="orange"><?= $sum->skillCount?></span> skills.
</div>


   

<h3><img src="img/pas-dot.png" />Ranking</h3>
<p>

<ul style="list-style:none">
    <li>
    <b style="color:#ff6600;"># <?= $rank->skilltrackerPoints?>.</b> is the overall <a href="overview/r/skilltrackerPoints#<?= $characterName ?>">EVE Skilltracker Rank</a> of <?= $characterName ?> (out of <?=$total?> registered pilots).<br />
    
        <ul style="list-style:none; margin-top:20px;">
            <li>
            <b style="color:#7b7b7b;"># <?= $rank->badgeRankPoints?>.</b> is the Skilltracker <a href="overview/r/badgeRankPoints#<?= $characterName ?>">Badge Rank</a> of <?= $characterName ?>.
            
                <ul style="list-style:none; margin-top:20px; font-size:14px;">
                    <li>
                    <b># <?=$rank->expertBadges?></b> is your rank with <?=$sum->expertBadges?> expert badges in the <a href="overview/r/expertBadges#<?= $characterName ?>">Expert Ranking</a>.
                    </li>
                    <li>
                    <b># <?=$rank->mediumBadges?></b> is your rank with <?=$sum->mediumBadges?> medium badges in the <a href="overview/r/mediumBadges#<?= $characterName ?>">Medium Ranking</a>.
                    </li>
                    <li>
                    <b># <?=$rank->basicBadges?></b> is your rank with <?=$sum->basicBadges?> basic badges in the <a href="overview/r/basicBadges#<?= $characterName ?>">Basic Ranking</a>.
                    </li>
                </ul>
            </li>
        </ul>
    
        <ul style="list-style:none; margin-top:20px;">
        	<li>
            <b style="color:#7b7b7b;"># <?= $rank->skillRankPoints?>.</b> is the Skilltracker <a href="overview/r/skillRankPoints#<?= $characterName ?>">Skill Rank</a> of <?= $characterName ?>.
            
            	<ul style="list-style:none; margin-top:20px;  font-size:14px;">
                	<li>
                    # <?= $rank->skillFiveCount?> is your rank with <?= $sum->skillFiveCount?> skills at Level 5 in the <a href="overview/r/skillFiveCount#<?= $characterName ?>">Level 5 Ranking</a>.
                    </li>
                    <li>
                    # <?= $rank->skillpoints?></a> is your rank with <?= number_format($sum->skillpoints,0,'','.') ?> SP in the <a href="overview/r/skillpoints#<?= $characterName ?>">Skillpoint Ranking</a>.
                    </li>
                    <li>
                    # <?= $rank->skillCount?> is your rank with <?= $sum->skillCount?> skills in the <a href="overview/r/skillCount#<?= $characterName ?>">Total Skills Ranking</a>.
                    </li>
                </ul>
            </li>
        </ul>
    
    
    </li>
</ul>



<b><span style="color: #555555; font-size: smaller;">Ranking Formula:<br></span></b>
<span style="color: #555555; font-size: smaller;">&nbsp; EVE Skilltracker Rank = (Badge Rank Points) + (Skill Rank Points)<br>
&nbsp; Badge Rank Points = (Expert Ranking x5) + (Medium Ranking x2) + (Basic Ranking x1)<br>
&nbsp; Skill Rank Points = (Level 5 Skills x10) + (Skillpoints /1.000.000x5) + (Total Skills x1)<br><br></span>

<script type="text/javascript">
    $(function() {
        $('tr.hoverable').dblclick(function() {
            window.location.href = '<?= base_url(); ?>users/changeCharacter/' + $('input',this).val();
        });
    });
</script>