<h3><?= $sRestriction ?></h3>
<hr style="display: block; height: 4px;
    border: 0; border-top: 4px solid #ff6600;
    margin: 1em 0; padding: 0;clear:both; " />
<h3><img src="img/pas-dot.png" />Skills of <?= $characterName ?></h3>
<div style="float: left;margin:10px;">
	<div style="display: inline-block;">
        <a href="/overview/skills"><img style="margin-right:10px; border: 2px solid #ff6600;" src="<?= $this->evelib->getCharacterImage($characterID, 128) ?>" /></a>
	</div>
	<div style="display: inline-block;width:170px;vertical-align: top;">
	<span class="hl-class-small">Total Skills:</span> <?= $totalSkills ?>
		<span class="hl-class-small">Total Skillpoints:</span> <?= number_format($totalSkillPoints, 0, '', ',') ?>
	</div>
</div>
<div style="float: right;margin-top:-1em;">
	<?= $totalBadges ?>
</div>
<hr style="display: block; height: 4px;
    border: 0; border-top: 4px solid #ff6600;
    margin: 1em 0; padding: 0;clear:both; " />
<?php
$lastGroup = -1;

foreach ($skills as $skill) {
if ($skill->groupID != $lastGroup) {
if ($lastGroup != -1) {
	?>
	</table>
	<br><?php } ?>
<b style="font-size:14px; color:#ff6600; ">
	<?= $skill->groupName ?>
</b>
<table width="100%" class="skills">
	<?php }
	if ($skill->typeName == "") { ?>
		<tr style="color:#7b7b7b;">
			<td colspan="2">
				<?= $skill->count ?> <?= $skill->groupName ?> skills trained, for a total of <?= number_format($skill->sum, 0, '', ',') ?> skillpoints.
			</td>
		</tr>
	<?php } else { ?>
		<tr <?= ($skill->level == 5) ? 'class="yellow"' : ''; ?>>
			<td><?= $skill->typeName ?> <span class="orange">//</span> Rank <?= $skill->trainingTimeMultiplier ?>
				<span class="orange">//</span> Level <?= $skill->level ?>
			</td>
			<td align="right"><img src="<?=base_url();?>/img/l<?= $skill->level ?>.gif" />
		</tr>
	<?php } ?>
	<?php
	$lastGroup = $skill->groupID;
	}
	?>
</table>