<h3>
	<img src="img/pas-dot.png" /><?=$title?>
</h3>
<table class="rankinglist" width="100%">
	<?php $i=1; foreach ($table as $r) {?>
	<tr <?= ($r->characterID == $characterID) ? 'class="orange"' : '';?>>
		<td width="15">
			<?=$i?>
		</td>
		<td>
			 <?= (getPrivacy($r, 'A')) ? '--- Anonymous ---' : '<a href="users/'.strtolower($r->characterName).'/skills" name="'.$r->characterName.'">'.$r->characterName.'</a>'; ?>
		</td>
		<td align="right">
			<?= number_format($r->$field,0,'',',') ?>
		</td>
	</tr>
	<?php $i++;} ?>
</table>