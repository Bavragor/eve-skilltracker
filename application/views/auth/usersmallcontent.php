<?php if (!isset($userID) || $userID == 0) { ?>

    <script type="text/javascript">
        $(function() {
            $('input:submit').button();
            $('#loginform').submit(function(e) {
                username = $('#username').val();
                password = $('#password').val();
                $.post('users/loginSubmit',{username: username, password: password},function(data) {
                    if(data == 1) {
                        $('#loginform').attr('action','users/loginSubmit');

	                    window.location = "<?= base_url() . 'overview/skills' ?>"
                    } else {
                        $('.error').html(data);
                        $('#username').val('');
                        $('#password').val('');
                    }
                });
            });
            $('.loginlink').click(function() {
                $('#text').slideToggle();
                $('#login').slideToggle();
                $('#username').focus();
            });
        });
    </script>

    <div id="text"
         <?= (validation_errors()) ? 'style="display:none;"' : ''; ?>>
        <h3> <img src="img/pas-dot" />You are logged out</h3>
        <p class="text_block">
            <span class="link_fake_underline loginlink" style="color:#9a9a9a;">Login</span> with your
            username and password or <a href="users/signup">Sign Up</a> for a new
            Skilltracker account.
        </p>
    </div>


    <div id="login"
         <?= (validation_errors()) ? '' : 'style="display:none;"'; ?>>

        <h3><img src="img/pas-dot.png" />LOGIN</h3>
        <iframe src="dummy.html" id="dummy" name="dummy" style="display: none"></iframe>
        <form id="loginform" target="dummy" action="" method="post">
            <p class="error"></p>
            <fieldset>
                <table>
                    <tr>
                        <td><label for="username">Username: &nbsp;</label>
                        </td>
                        <td><input type="text" name="username" id="username"
                                   style="width: 110px" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="password">Password: &nbsp;</label>
                        </td>
                        <td><input type="password" name="password" id="password"
                                   style="width: 110px" />
                        </td>
                    </tr>

                </table>
                <input type="submit" name="submit" id="submit" value="Login" />
            </fieldset>
        </form>
    </div>


<?php } else { ?>
    <script type="text/javascript">
        $(function() {
            $('.changeCharacter').button({
                icons: {
                    primary:"ui-icon-transferthick-e-w"
                },
                text: false}).click(function() {
                $('.characterSelection').load('users/characterSelection', function() {
                    $('.characterSelection').slideToggle();
                });
                                                                					
            });
            $('.updateButton').button({
                icons: {
                    primary:"ui-icon-circle-triangle-e"
                }
            }).click(function() {
                var keyID = $(this).attr('data-keyID');
                var button = $(this);
                button.after('<img class="ajaxload" src="img/ajax-loader.gif">');
                $.post('<?= base_url() ?>updating/key',{keyid: keyID},function(data) {
                    $('.ajaxload').remove();
                    button.button("option", "disabled","true");
                    if(data == 1) {
                        button.next('span').html('Done!').fadeIn().delay(2500).fadeOut();
                        window.location.reload();
                    } else {
                        button.next('span').html(data).fadeIn();
                    }
                });
            });
        });
    </script>
    <h3 style="margin-bottom:20px;;"><img src="img/pas-dot.png"/>LOGGED IN</h3>
    <div style="float: left; text-align:left; min-height:180px;">

        <?php if (isset($char)) { ?>
            <a href="/overview/skills"><img class="loggedIn" style="margin-right:10px; border: 2px solid #ff6600;" src="<?= $this->evelib->getCharacterImage($characterID, 128); ?>" /></a><br />
        <?php } ?>
        <button class='changeCharacter' style="margin-right:10px; width:132px;">Select Character</button>

    </div>
    <?php if (isset($char) && $char != FALSE) { ?>

        <span class="nhl-class-small">ACTIVE CHAR:</span> <span class="hl-class-small"><?= $char->characterName ?></span>
        <br /> <span class="nhl-class-small">ACTIVE CORP:</span> <span class="hl-class-small"><?= $char->corporationName ?></span>
        </span>



        <?php if ($char->valid == 1) { ?>
            <p>
                <span class="nhl-class-small">Cached until:</span> <br/>
                <span class="hl-class-small"><?= ($char->cachedUntil > 0) ? date('d-m-Y H:i', $char->cachedUntil) . ' </span><span class="nhl-class-small"> EVE Time' : 'never' ?></span>
                <?php if (isset($updatePossible) && $updatePossible) { ?>
                    <button class="updateButton" style="margin-right:10px;" data-keyID="<?= $char->keyID; ?>">Update!</button>
                    <span class="nh1-class-small"></span>
                <?php } ?>
            </p>
        <?php } else { ?>
            <p style="color:red; font-size:14px;">
                Your API-Key is wrong, expired or has a wrong access mask.<br/>Click <a href="<?= base_url() ?>users//settings#keys">here</a> to change your API-Key.
            </p>
        <?php } ?>


        <h3 style="clear:both;"><img src="img/pas-dot.png"/>CURRENTLY TRAINING</h3>

        <?php if ($char->skillInTraining == 0) { ?>
            <p>This character is not training at the moment</p>
        <?php } else { ?>

            <span class="hl-class-small"><?= $char->typeName ?></span><span class="nhl-class-small"> to </span><span class="hl-class-small">Level <?= $char->trainingToLevel ?></span><br />
			<span class="hl-class-small">Skill will be ready at: </span><?= $char->trainingEndTime ?><br />
            <img style="float: left;" src="img/skillbar/<?= $char->trainingToLevel ?>.gif" /><br style="line-height:50px;" />
        <?php } ?>
    <?php } else { ?>
        <p class="hl-class-small" style="font-size:12px;">You have no character selected.</br>If you have just registered it may take a few minutes until your characters are available.</br></br>
            Click the "Select Character" button to change your character.</p>
    <?php } ?>
    <div class="characterSelection" style="display:none;"></div>
    <?php if ($igb) { ?>
        <p>
            You are flying a
            <?= $this->evelib->linkShowInfo($shipTypeID) ?>
            <?= $shipTypeName ?>
        </span>.</br> Click <a href="badges/show/<?= $badgeID ?>">here</a> to
        get to the badge.
        </p>
    <?php } ?>
<?php } ?>
