<?php

function addPart($prevCat) {
    return '<input type="text" name="categoryName" value="" class="categoryName"/><button class="addCategory">' . $prevCat->categoryID . '-' . $prevCat->rgt . '- ' . $prevCat->lft . '</button>';
}

//Here we store the level of the last item we printed out
$lastLevel = -1;

//Stack
$stack = array();

$categories2 = $categories;

array_push($categories, (object) array('level' => 0, 'categoryID' => 0));

//Iterating tree
foreach ($categories as $c) {

    //If we are on the item of the same level, closing <li> tag before printing item
    if (($c->level == $lastLevel) && ($lastLevel >= 0)) {
        $prevCat = array_pop($stack);
        ?>
        <ul style="list-style:none;"><li><?= addPart($prevCat) ?></button></li></ul>
        </li>
        <?php
    }

    //If we are printing a next-level item, starting a new <ul>
    if ($c->level > $lastLevel) {
        ?>
        <ul style="list-style:none;">
            <?php
        }

        //If we are going to return back by several levels, closing appropriate tags 
        if ($c->level < $lastLevel) {
            $prevCat = array_pop($stack);
            ?>
            <ul style="list-style:none"><li><?= addPart($prevCat) ?></button></li></ul>

            <?php
            for ($i = 0; $i < ($lastLevel - $c->level); $i++) {

                $prevCat = array_pop($stack);
                ?>
            </li><li><?= addPart($prevCat) ?></button></li></ul>
        <?php } ?>
        </li>
        <?php
    }

    if ($c->categoryID != 0) {
        //Priting item without closing tag
        ?>
        <li>
            <span class="cat">
                <span class="editCategoryName"><?= $c->categoryName ?></span>
                <input style="display:none" class="editCategoryName" value="<?= $c->categoryName ?>" />
                <button class="editCategory"><?= $c->categoryID ?></button>
                
                <span class="moveCategorySelect" style="display:none;">Move in front of: </span>
                <select class="moveCategorySelect" style="display:none;">
                    <?php foreach ($categories2 as $c2) { ?>
                        <option value="<?= $c2->lft?>" >
                            <?php
                            for ($i = 0; $i < $c2->level; $i++) {
                                echo '-';
                            }
                            ?>
                            <?= $c2->categoryName ?>
                        </option>
                    <?php } ?>
                </select>
                <button class="moveCategory"><?= $c->categoryID . '-' . $c->lft . '-' . $c->rgt ?></button>
                <button class="deleteCategory"><?= $c->categoryID . '-' . $c->rgt . '-' . $c->lft ?></button>
            </span>

            <?php
            array_push($stack, $c);

            //Refreshing last level of the item
            $lastLevel = $c->level;
        }
    }
    ?>