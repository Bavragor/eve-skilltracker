<script type="text/javascript">
    $(function() {
        $('img.iconchange').click(function() {
            $('img.iconchange').removeClass('active');
            $(this).addClass('active');
            fn = $(this).attr('id');
            $.post('<?= base_url() ?>admin/setIcon',{badgeID: <?= $b->badgeID ?>,filename: fn},function(x) {
                if(x) {
                    $('img.active').css('border-color','#ff6600');

                }
            });
        }); 
    });
</script>

<h3><img src="img/act-dot.png" />Choose icon for badge <?= $b->badgeName ?></h3>
<p><img src="img/pas-dot.png" /><a href="<?= base_url() ?>admin/editBadge/<?= $b->badgeID ?>">back to badge</a></p>
<?php foreach ($icons as $i) { ?>

    <img 
        src="<?= $this->evelib->getIcon($i) ?>" 
        class="iconchange <?= ($i == $b->iconFile) ? 'active' : ''; ?>"
        id="<?= $i ?>"
        height="64" width="64"
        />

<?php } ?>

<p><img src="img/pas-dot.png" /><a href="<?= base_url() ?>admin/editBadge/<?= $b->badgeID ?>">back to badge</a></p>