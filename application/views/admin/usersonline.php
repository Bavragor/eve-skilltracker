<h1><?= $title ?></h1>
<h2>Who is online?</h2>
<p>Actual online: <?= count($aCurUsersOnline) ?></p>
<hr style="display: block; height: 4px;
    border: 0; border-top: 4px solid #ff6600;
    margin: 1em 0; padding: 0; " />
<table cellpadding="6">
	<thead>
		<th>Users:</th>
		<th>Visit:</th>
		<th>Online since:</th>
	</thead>
	<tbody>
		<?php foreach ($aCurUsersOnline as $oSingleUser) { ?>
			<?php
				$aUserData = unserialize($oSingleUser->user_data);
			?>
			<tr>
				<td><?= isset($aUserData['username']) ? '<a href="admin/user/'.$aUserData['userID'].'" title="'.$aUserData['username'].'">'.$aUserData['username'].'</a>' : 'Guest User' ?></td>
				<td><?= $aUserData['last_request'] ?></td>
                <td><?= isset($aUserData['userID']) ? getLastLogin($aUserData['userID']) : $oSingleUser->last_activity ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<hr style="display: block; height: 4px;
    border: 0; border-top: 4px solid #ff6600;
    margin: 1em 0; padding: 0; " />
<h2>Last online? (24 hours)</h2>
<hr style="display: block; height: 4px;
    border: 0; border-top: 4px solid #ff6600;
    margin: 1em 0; padding: 0; " />
<table cellpadding="6">
	<thead>
		<th>Users:</th>
		<th>Visit:</th>
		<th>Last online:</th>
	</thead>
	<tbody>
		<?php foreach ($aLastUsersOnline as $oSingleUser) { ?>
			<?php
				$aUserData = unserialize($oSingleUser->user_data);
			?>
			<tr>
				<td><?= isset($aUserData['username']) ? '<a href="admin/user/'.$aUserData['userID'].'" title="'.$aUserData['username'].'">'.$aUserData['username'].'</a>' : 'Guest User' ?></td>
				<td><?= $aUserData['last_request'] ?></td>
				<td><?= $oSingleUser->last_activity ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>