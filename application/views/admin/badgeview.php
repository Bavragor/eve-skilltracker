<script type="text/javascript">
    function loadBadges() {
        $('#badges').html('<img src="img/load.gif"/>');
        $('#badges').load('<?= base_url() ?>admin/badgeList', function() {
            $('.deleteBadge').button({
                icons: {
                    primary: "ui-icon-trash"
                },
                text: false
            });
            $('.deleteBadge').click(function() {
                par = $(this).attr('title');
                if (window.confirm("Delete badge?")) {
                    $.post('<?= base_url() ?>admin/deleteBadge',{params: par}, function() {
                        loadBadges();
                    });
                }
            });
            $('.editBadge').button({
                icons: {
                    primary: "ui-icon-pencil"
                },
                text: false
            });
            $('.editBadge').click(function() {
                window.location = "<?= base_url() . 'admin/editBadge/' ?>" + $(this).attr('title') ;
            });
            $('.addBadge').button({
                icons: {
                    primary: "ui-icon-circle-plus"
                },
                text: false       
            });
            
            function submit(badgeNameInput) {
                if(badgeNameInput.is(':visible')) {
                    badgeName = badgeNameInput.val();
                    categoryID = badgeNameInput.attr('data-categoryID');
                    $.post('<?= base_url() ?>admin/saveBadge',{bn: badgeName,ci: categoryID}, function() {
                        loadBadges();
                    });
                    badgeNameInput.animate({width:'0'},'fast');
                    badgeNameInput.fadeOut('fast');
                } else {
                    badgeNameInput.show();
                    badgeNameInput.animate({width:'165'},'fast');
                    badgeNameInput.focus();
                }
            }
            
            $('.addBadge').click(function() {
                submit($(this).prev('.badgeName'));
            });
            
            $('.badgeName').keypress(function(e) {
                if(e.which == 13) {
                    submit($(this));
                    return false;
                }
            });
            
            $('.badgeName').css('width','0');
            $('.badgeName').hide();
        });
    }
    
    $(function() {
        loadBadges();
    });
   
</script>
<h3><img src="img/act-dot.png" />Badges</h3>
<div id="badges"></div>