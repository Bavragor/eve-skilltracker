<script type="text/javascript">
    function loadCategories() {
        $('#categories').html('<img src="img/load.gif"/>');
        $('#categories').load('<?= base_url() ?>admin/categoryList', function() {
            $('.deleteCategory').button({
                icons: {
                    primary: "ui-icon-trash"
                },
                text: false
            });
            $('.deleteCategory').click(function() {
                par = $(this).attr('title');
                if (window.confirm("Delete category?")) {
                    $.post('<?= base_url() ?>admin/deleteCategory',{params: par}, function() {
                        loadCategories();
                    });
                }
            });
            $('.addCategory').button({
                icons: {
                    primary: "ui-icon-circle-plus"
                },
                text: false       
            });
            
            function submit(catNameInput,catAdd) {
                if(catNameInput.is(':visible')) {
                    categoryName = catNameInput.val();
                    par = catAdd.attr('title');
                    $.post('<?= base_url() ?>admin/saveCategory',{cn: categoryName,params: par}, function() {
                        loadCategories();
                    });
                    catNameInput.animate({width:'0'},'fast');
                    catNameInput.fadeOut('fast');
                } else {
                    catNameInput.show();
                    catNameInput.animate({width:'165'},'fast');
                    catNameInput.focus();
                }
            }
            
            $('.addCategory').click(function() {
                submit($(this).prev('.categoryName'),$(this));
            });
            
            $('.categoryName').keypress(function(e) {
                if(e.which == 13) {
                    submit($(this),$(this).next('.addCategory'));
                    return false;
                }
            });
            
            $('.editCategory').button({
                icons: {
                    primary: "ui-icon-pencil"
                },
                text: false
            });
       
            $('.editCategory').click(function() {
                input = $(this).closest('span.cat').find('input.editCategoryName');
                span = $(this).closest('span.cat').find('span.editCategoryName');
                
                if(input.css('display') == 'none') {
                    span.fadeOut('fast',function() {
                        input.fadeIn('fast');
                        input.focus();
                    });
                } else {
                    cid = $(this).attr('title');
                    catname = input.val();
                    $.post('<?= base_url() ?>admin/editCategoryName',{categoryID: cid,categoryName: catname},function($success) {
                        if($success) {
                            span.html(input.val());
                        } else {
                            alert('Error');
                        }
                    
                        input.fadeOut('fast',function() {
                            span.fadeIn('fast');  
                        });
                    });
                }
            });
            
            $('.moveCategory').button({
                icons: {
                    primary: "ui-icon-arrow-4"
                },
                text: false
            });
       
            $('.moveCategory').click(function() {
                select = $(this).closest('span.cat').find('select.moveCategorySelect');
                span = $(this).closest('span.cat').find('span.moveCategorySelect');
                
                if(select.css('display') == 'none') {
                    span.fadeIn('fast');
                    select.fadeIn('fast');
                } else {
                    par = $(this).attr('title');
                    tlft = select.val();
                    $.post('<?= base_url() ?>admin/moveCategory',{params: par,targetLft: tlft},function($success) {
                        loadCategories();
                    });
                }
            });
            
            $('.categoryName').css('width','0');
            $('.categoryName').hide();
        });
    }
    
    $(function() {
        loadCategories();
    });
   
</script>
<h3><img src="img/act-dot.png" /> Categories</h3>
<div id="categories"></div>