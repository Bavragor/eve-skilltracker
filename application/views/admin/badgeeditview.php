<script type="text/javascript">
    function isNumber (o) {
        return ! isNaN (o-0);
    }
    function loadSkills() {
        //$('.rankContainer').fadeToggle('fast');
        $('.skillsContainer').load('<?= base_url() ?>admin/skillsList/<?= $b->badgeID ?>/',function() {
            $('.deleteSkill').button({
                icons: {
                    primary: "ui-icon-trash"
                },
                text: false
            });
            $('.deleteSkill').click(function() {
                par = $(this).attr('title');
                $.post('<?= base_url() ?>admin/deleteSkill',{params: par}, function() {
                    loadSkills();
                });
            });
            $('#badges').fadeToggle('fast');
            $('.addSkill').button({
                icons: {
                    primary: "ui-icon-circle-plus"
                },
                text: false       
            });
            $('.addSkill').click(function() {
                skillName = $(this).closest('tr').find('.addSkillName').val();
                skillLevel = $(this).closest('tr').find('.addSkillLevel').val();
                skillOr = $(this).closest('tr').find('.addSkillOr').attr('checked');
                par = $(this).attr('title');
                $.post('<?= base_url() ?>admin/typeID', {typeName: skillName}, function(response) {
                    if(!isNumber(response)) {
                        alert("Not a skill");
                    } else {
                        skillTypeID = response;
                        $.post('<?= base_url() ?>admin/saveSkill',{tid: skillTypeID,lvl: skillLevel,params: par,or:skillOr}, function() {
                            loadSkills();
                        });
                    }
                });
                   
                    
            });
        });
    }
    function loadSubBadges() {
        //$('.rankContainer').fadeToggle('fast');
        $('.subBadgeContainer').load('<?= base_url() ?>admin/subBadgeList/<?= $b->badgeID ?>/',function() {
            $('.deleteSubBadge').button({
                icons: {
                    primary: "ui-icon-trash"
                },
                text: false
            });
            $('.deleteSubBadge').click(function() {
                par = $(this).attr('title');
                $.post('<?= base_url() ?>admin/deleteSubBadge',{params: par}, function() {
                    loadSubBadges();
                });
            });
            $('.addSubBadge').button({
                icons: {
                    primary: "ui-icon-circle-plus"
                },
                text: false       
            });
            $('.addSubBadge').click(function() {
                badgeID = $(this).closest('tr').find('.addSubBadgeID').val();
                subBadgeRank = $(this).closest('tr').find('.addSubBadgeRank').val();
                subBadgeOr = $(this).closest('tr').find('.addSubBadgeOr').attr('checked');
                par = $(this).attr('title');
                    
                $.post('<?= base_url() ?>admin/saveSubBadge',{bid: badgeID,rank: subBadgeRank,params: par,or:subBadgeOr}, function() {
                    loadSubBadges();
                });
                        
            });
                   
                    
        });
      
    }
    $(function() {
        loadSkills();
        loadSubBadges();
        
        $('#shipSuccess').button({
            icons: {
                primary: "ui-icon-check"
            },
            text: false
        });
        
        $('#typeID').change(function() {
            $('#shipSuccess').fadeIn();
            bid = <?= $b->badgeID ?>;
            tid = $(this).val();
                    
            $.post('<?= base_url() ?>admin/setShipTypeID',{badgeID: bid, typeID: tid}, function($success) {
                if($success) {
                    $('#shipSuccess').fadeIn();
                } else {
                    $('#shipSuccess').fadeOut();
                }
            });
        });
       
        $('#editCategory').button({
            icons: {
                primary: "ui-icon-pencil"
            },
            text: false
        });
       
        $('#editCategory').click(function() {
            if($('select.categoryName').css('display') == 'none') {
                $('span.categoryName').fadeOut('fast',function() {
                    $('select.categoryName').fadeIn('fast');
                    $('select.categoryName').focus();
                });
            } else {
                bid = $(this).attr('title');
                cid = $('select.categoryName').val();
                $.post('<?= base_url() ?>admin/editBadgeCategory',{badgeID: bid,categoryID: cid},function($success) {
                    if($success) {
                        $('span.categoryName').html($('select.categoryName option[value=' + cid + ']').html().replace('-', '','g'));
                    } else {
                        alert('Error');
                    }
                    
                    $('select.categoryName').fadeOut('fast',function() {
                        $('span.categoryName').fadeIn('fast');  
                    });
                });
            }
        });
       
        $('#editName').button({
            icons: {
                primary: "ui-icon-pencil"
            },
            text: false
        });
       
        $('#editName').click(function() {
            if($('input.badgeName').css('display') == 'none') {
                $('span.badgeName').fadeOut('fast',function() {
                    $('input.badgeName').fadeIn('fast');
                    $('input.badgeName').focus();
                });
            } else {
                bid = $(this).attr('title');
                bnm = $('input.badgeName').val();
                $.post('<?= base_url() ?>admin/editBadgeName',{badgeID: bid,badgeName: bnm},function($success) {
                    if($success) {
                        $('span.badgeName').html($('input.badgeName').val());
                    } else {
                        alert('Error');
                    }
                    
                    $('input.badgeName').fadeOut('fast',function() {
                        $('span.badgeName').fadeIn('fast');  
                    });
                });
            }
        });
        
        $('#sortName').button({
            icons: {
                primary: "ui-icon-disk"
            },
            text:false
        });
        
        $('#sortName').click( function() {
            bid = <?= $b->badgeID ?>;
            sN = $('.sortName').val();
            $.post('<?= base_url() ?>admin/editSortName',{badgeID: bid,sortName: sN},function($success) {
                
            });
        });
    });
</script>
<div id="admin">
<p><img src="img/pas-dot.png" /> <a href="<?= base_url() ?>/admin/badges">Back to badges</a> | <img src="img/pas-dot.png" /><a href="<?= base_url() ?>admin/iconChange/<?= $b->badgeID ?>">Change icon</a></p>
<h3>
    <img style="vertical-align:middle;" src="<?= base_url() . $this->evelib->getIcon($b->iconFile) ?>" height="64px"/>
    Edit badge: <span class="badgeName"><?= $b->badgeName ?></span><input style="display:none;" value="<?= $b->badgeName ?>" class="badgeName"/>
    <button id="editName"><?= $b->badgeID ?></button>
</h3>
<p>
    <span class="orange">Category: </span>
    <span class="categoryName"><?= $b->categoryName ?></span>
    <select style="display:none" class="categoryName" value="<?= $b->categoryID ?>">
        <?php foreach ($categories as $c) { ?>
            <option value="<?= $c->categoryID ?>" <?= ($b->categoryID == $c->categoryID) ? 'selected="selected"' : ''; ?>>
                <?php
                for ($i = 0; $i < $c->level; $i++) {
                    echo '-';
                }
                ?>
                <?= $c->categoryName ?>
            </option>
        <?php } ?>
    </select>
    <button id="editCategory"><?= $b->badgeID ?></button>
</p>
<p><span class="orange">Badge is related with this ship:</span></p>
<select name="typeID" id="typeID">
    <option value="0">no ship</option>
    <?php foreach ($ships as $s) { ?>
        <option value="<?= $s->typeID ?>" <?= ($b->typeID == $s->typeID) ? 'selected="selected"' : ''; ?> > <?= $s->typeName ?> </option>
    <?php } ?>
</select>
<button id="shipSuccess" class="ui-state-hover" style="display:none;">&nbsp;</button>
<p style="margin-top:15px;">
    <span class="orange">Name for sorting:</span> <input name="sortName" class="sortName" value="<?= $b->sortName ?>"/>
    <button id="sortName">Save</button>
</p>
<h3><img src="img/pas-dot.png" />Skills</h3>
<div class="skillsContainer"></div>
<h3><img src="img/pas-dot.png" />Sub-Badges</h3>
<div class="subBadgeContainer"></div>
</div>