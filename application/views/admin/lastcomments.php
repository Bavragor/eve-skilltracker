<h1><?= $title ?></h1>
<hr style="display: block; height: 4px;
    border: 0; border-top: 4px solid #ff6600;
    margin: 1em 0; padding: 0; " />
<?php foreach ($aLastComments as $c) { ?>
	<div class="comment-header">
		<p class="comment-header-date">
			<img src="img/pas-dot.png" /> <?= date('d.m.Y G:i', $c->date) ?>:
		</p>
		<p class="comment-header-right">

			<span style="margin-right: 145px;">Kommentar im Badge: <a href="/badges/<?= $this->badgemodel->getBadgeName($c->badgeID)  ?>#<?= $c->commentID ?>" title="<?= $this->badgemodel->getBadgeName($c->badgeID) ?>"><?= $this->badgemodel->getBadgeName($c->badgeID) ?></a></span>
			<?php if ($this->session->userdata('admin') == 1) { ?>
				<img src="img/act-dot.png" /><span style="margin-right:20px;" class="link_fake_underline delete" title="<?= $c->commentID ?>">delete</span>
			<?php } ?>
		</p>
	</div>
	<div class="comment-body">

		<div class="comment-left">
			<span style="color:#ff6600; font-weight:300;"><?= $c->characterName ?></span><hr style="display: block; height: 1px;
    border: 0; border-top: 1px solid #ff6600;
    margin: 1em 0; padding: 0;" />
			<img src="<?= $this->evelib->getCharacterImage($c->mainCharacter,64)?>"/>
		</div>
		<div class="comment-right">
			<?= nl2br($c->text) ?>
		</div>

	</div>
	<div style="clear:both;"></div>
<?php } ?>

<script type="text/javascript">
	$(function() {

		<?php if ($this->session->userdata('admin')) { ?>
		$('span.delete').click(function() {
			id = $(this).attr('title');
			$("#dialogConfirmDeleteComment").dialog("open");
		});

		$("#dialogConfirmDeleteComment").dialog({
			resizable: false,
			height:230,
			modal: true,
			autoOpen: false,
			buttons: {
				"Delete comment": function() {
					$.post('forum/deleteComment',{commentID: id}, function(data) {
						window.location.reload();
					});
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			}
		});
		<?php } ?>
	});
</script>