<h3><img src="img/act-dot.png" /> Usermanagement</h3>
<span class="orange"><img src="img/pas-dot.png" />Search</span>
<p>You can use '_' for single letters and '%' for multiple letters.</p>
<?= form_open('admin/usersWithName');?>
<fieldset>
    <legend>Search by username</legend>
    <p><input type="text" id="username" name="username"/>&nbsp;<input id="usernameSearch" type="submit" value="Search"/></p>
</fieldset>
<?=form_close();?>
<?= form_open('admin/usersWithCharacter');?>
<fieldset>
    <legend>Search by character</legend>
    <p><input type="text" id="characterName" name="characterName"/>&nbsp;<input id="characterSearch" type="submit" value="Search"/></p>
</fieldset>
<?=form_close();?>
<br />
<span class="orange"><img src="img/pas-dot.png" />User list</span>
<p class="pagination">
    <a <?= ($previous) ? 'href="admin/users/' . ($page - 1) . '/'. $field . '/' . $order . '"' : '' ?>>&laquo;</a>
    <?php for ($i = 1; $i <= $pageCount; $i++) { ?>
        <a <?= ($i == $page) ? 'class="text_red"' : 'href="admin/users/' . $i . '/'. $field . '/' . $order . '"' ?>><?= $i ?></a>
    <?php } ?>
    <a <?= ($next) ? 'href="admin/users/' . ($page + 1) . '/'. $field . '/' . $order . '"' : '' ?>>&raquo;</a>
</p>
<?=$userList?>
<p class="pagination">
    <a <?= ($previous) ? 'href="admin/users/' . ($page - 1) . '/'. $field . '/' . $order . '"' : '' ?>>&laquo;</a>
    <?php for ($i = 1; $i <= $pageCount; $i++) { ?>
        <a <?= ($i == $page) ? 'class="text_red"' : 'href="admin/users/' . $i . '/'. $field . '/' . $order . '"' ?>><?= $i ?></a>
    <?php } ?>
    <a <?= ($next) ? 'href="admin/users/' . ($page + 1) . '/'. $field . '/' . $order . '"' : '' ?>>&raquo;</a>
</p>
<script type="text/javascript">
    $(function() {
       $('#usernameSearch').button();
       $('#characterSearch').button();
    });
 </script>