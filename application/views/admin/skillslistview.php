
<h3><?= $rank->rankName ?></h3>
<table>
    <?php
    $lastGroup = 0;
    foreach ($skills as $s) {
        ?>
        <tr>
            <td>
                <?php if ($s->skillGroup == $lastGroup) { ?>
                    <b>Or:</b>
                <?php } ?>
                <?= $s->typeName ?></td>
            <td><?= $s->level ?></td>
            <td><button class="deleteSkill"><?= $badgeID . '-' . $rank->rankID . '-' . $s->typeID ?></button>
        </tr>
        <?php
        $lastGroup = $s->skillGroup;
    }
    ?>
    <tr>
        <td>
            Or: <input type="checkbox" class="addSkillOr" name="addSkillOr" value="1"/>
            <input type="text" class="addSkillName" name="addSkillName"/>
        </td>
        <td><input type="text" class="addSkillLevel" name="addSkillLevel" style="width:20px"/></td>
        <td><button class="addSkill"><?= $badgeID . '-' . $rank->rankID ?></button></td>
    </tr>
</table>
