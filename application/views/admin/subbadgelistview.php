<h3><?= $rank->rankName ?></h3>
<table>
    <?php
    $lastGroup = 0;
    foreach ($subBadges as $s) {
        ?>
        <tr>
            <td>
                <?php if ($s->badgeGroup== $lastGroup) { ?>
                    <b>Or:</b>
                <?php } ?>
                <?= $s->badgeName ?></td>
            <td><?= $s->rankName ?></td>
            <td><button class="deleteSubBadge"><?= $badgeID . '-' . $rank->rankID . '-' . $s->badgeID ?></button>
        </tr>
        <?php
        $lastGroup = $s->badgeGroup;
    }
    ?>
    <tr>
        <td>
            Or: <input type="checkbox" class="addSubBadgeOr" name="addSubBadgeOr" value="1"/>
            <select class="addSubBadgeID" name="addSubBadgeID">
                <option value="0">Please choose</option>
                <?php foreach($badges as $b) { ?>
                <option value="<?=$b->badgeID?>"><?=$b->badgeName?></option>
                <?php } ?>
            </select>
        </td>
        <td>
            <select class="addSubBadgeRank" name="addSubBadgeRank">
                <option value="0">Please choose</option>
                <?php foreach($ranks as $r) { ?>
                <option value="<?=$r->rankID?>"><?=$r->rankName?></option>
                <?php } ?>
            </select>
        </td>
        <td><button class="addSubBadge"><?= $badgeID . '-' . $rank->rankID ?></button></td>
    </tr>
</table>
