<?php

$lastCategory = 1;
$lastLevel = 0;

function addPart($categoryID,$level) {
    echo '<p  style="margin-left:' . $level * 15 . 'px">';
    echo '<input type="text" name="badgeName" value="" class="badgeName" data-categoryID="' . $categoryID . '"/><button class="addBadge" title="Add">&nbsp;</button>';
    echo '</p>';
}

foreach ($badges as $b) {
    if ($b->categoryID != $lastCategory) {
        if ($b->level <= $lastLevel) {
            echo '<p>';
            addPart($lastCategory,$lastLevel);
            echo '</p>';
        }
        ?>
        <h<?= $b->level + 1 ?> style="margin-left:<?=$b->level * 15?>px; margin-bottom:0px;"><?= $b->categoryName ?></h<?= $b->level + 1; ?>>
        <?php
    }
    if (isset($b->badgeID)) {
        ?>
        <p style="margin-left:<?=$b->level * 15?>px; margin-top:0px; margin-bottom:0px;">
            <img src="<?= base_url() . $this->evelib->getIcon($b->iconFile)?>" height="24px" style="vertical-align: bottom;"/>
                    <?= $b->badgeName ?>
            <button class="editBadge"><?=$b->badgeID?></button>
            <button class="deleteBadge"><?=$b->badgeID?></button>
        </p>
        <?php
    }
    $lastCategory = $b->categoryID;
    $lastLevel = $b->level;
}
addPart($lastCategory,$lastLevel);
?>