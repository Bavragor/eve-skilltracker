<h3><img src="img/act-dot.png" />User: <?= $username ?></h3>
<a href="admin/users/">&lArr; Back to usermanagement</a>
<h3><img src="img/pas-dot.png" />General Information:</h3>
<br />
<b>Creation Date:</b> <?= date('d.m.Y H:i', $creation) ?>
<br />
<b>Last Login:</b> <?= date('d.m.Y H:i', $lastLogin) ?>



<h3><img src="img/pas-dot.png" />Characters:</h3>
<table class="bordered">
    <thead>
        <tr>
            <th/><center><img src="img/act-dot.png" /></center>
            <th>
                <img src="img/pas-dot.png" /> Character Name
            </th>
            <th/><center><img src="img/act-dot.png" /></center>
            <th>
                <img src="img/pas-dot.png" /> Corporation Name
            </th>
            <th>
                <img src="img/pas-dot.png" /> Anonymous
            </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($characters as $c) { ?>
            <tr>
                <td><img
                        src="<?= $this->evelib->getCharacterImage($c->characterID, 32) ?>" />
                </td>
                <td style="padding-left:20px;">
                    <a href="users/changeCharacter/<?=$c->characterID;?>/badges"><?= $c->characterName; ?></a>
                
                </td>
                <td><img
                        src="<?= $this->evelib->getCorporationImage($c->corporationID, 32) ?>" />
                </td>
                <td style="padding-left:20px;"><?= $c->corporationName; ?>
                </td>
                <td style="padding-left:20px;">
                    <?= (getPrivacy($c, 'A')) ? 'yes' : 'no' ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<h3>Operations:</h3>
<script type="text/javascript">
    $(function() {
        $("#dialogConfirmDeleteAccount").dialog({
            resizable: false,
            height:230,
            modal: true,
            autoOpen: false,
            buttons: {
                "Delete account": function() {
                    window.location.href = 'admin/deleteAccount/<?= $userID ?>'
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                } 
            }
        });
        $('#deleteAccount').button({
            icons: {
                primary: "ui-icon-alert"
            }
        }).click(function() {
            $("#dialogConfirmDeleteAccount").dialog("open");
        });
    });
</script>
<div id="dialogConfirmDeleteAccount" title="Delete the account?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>You will permanently delete this account together with all options, characters and keys. Are you sure?</p>
</div>
<button id="deleteAccount">Delete Account<b><span class="text_red"> ! </span></b></button>

<script type="text/javascript">
    $(function() {
        $("#dialogConfirmResetPassword").dialog({
            resizable: false,
            height:230,
            modal: true,
            autoOpen: false,
            buttons: {
                "Reset password": function() {
                    window.location.href = 'admin/resetPassword/<?= $userID ?>'
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                } 
            }
        });
        $('#resetPassword').button({
            icons: {
                primary: "ui-icon-info"
            }
        }).click(function() {
            $("#dialogConfirmResetPassword").dialog("open");
        });
    });
</script>
<div id="dialogConfirmResetPassword" title="Reset password?" style="display:none;">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>You will reset the user's password to his account name. Are you sure?</p>
</div>
<button id="resetPassword">Reset Password</button>
<p><br><a href="admin/users/">&lArr; Back to usermanagement</a></p>