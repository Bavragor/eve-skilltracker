<table class="bordered" width="640">
    <colgroup>
        <col width="160"/>
        <col width="160"/>
        <col width="190"/>
        <col width="130"/>
    </colgroup>
    <?php if (isset($field)) { ?>
        <thead>

            <tr>
                <th>
                    <?= ($field == 'u') ? '<span class="orange">' : ''; ?>
                    <img src="img/pas-dot.png" /> Username
                    <?= ($field == 'u') ? '</span>' : ''; ?>

                    <?php if (isset($page)) { ?>
                        <a href="admin/users/<?= $page ?>/u/a">&uArr;</a>
                        <a href="admin/users/<?= $page ?>/u/d">&dArr;</a>
                    <?php } ?>
                </th>
                <th>
                    <?= ($field == 'l') ? '<span class="orange">' : ''; ?>
                    <img src="img/pas-dot.png" /> Last Login
                    <?= ($field == 'l') ? '</span>' : ''; ?>

                    <?php if (isset($page)) { ?>
                        <a href="admin/users/<?= $page ?>/l/a">&uArr;</a>
                        <a href="admin/users/<?= $page ?>/l/d">&dArr;</a>
                    <?php } ?>
                </th>
                <th>
                    <?= ($field == 'cr') ? '<span class="orange">' : ''; ?>
                    <img src="img/pas-dot.png" /> Creation Date
                    <?= ($field == 'cr') ? '</span>' : ''; ?>

                    <?php if (isset($page)) { ?>
                        <a href="admin/users/<?= $page ?>/cr/a">&uArr;</a>
                        <a href="admin/users/<?= $page ?>/cr/d">&dArr;</a>
                    <?php } ?>
                </th>
                <th>
                    <?= ($field == 'cc') ? '<span class="orange">' : ''; ?>
                    <img src="img/pas-dot.png" /> Chars
                    <?= ($field == 'cc') ? '</span>' : ''; ?>

                    <?php if (isset($page)) { ?>
                        <a href="admin/users/<?= $page ?>/cc/a">&uArr;</a>
                        <a href="admin/users/<?= $page ?>/cc/d">&dArr;</a>
                    <?php } ?>
                </th>
            </tr>
        </thead>
    <?php } ?>
    <tbody>
        <?php foreach ($users as $u) { ?>
            <tr >
                <td style="padding-left:30px;">
                    <a href="admin/user/<?= $u->userID; ?>" <?= ($u->admin) ? 'class="orange"' : '' ?>><?= $u->username ?></a>
                </td>
                <td style="padding-left:30px;">
                    <?= date('d.m.Y H:i', $u->lastLogin) ?>
                </td>
                <td style="padding-left:30px;">
                    <?= date('d.m.Y', $u->creation) ?>
                </td>
                <td style="padding-left:30px;">
                    <?= (isset($u->count)) ? $u->count : '' ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>