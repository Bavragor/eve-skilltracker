<script type="text/javascript">
    function loadRanks() {
        $('#ranks').html('<img src="img/load.gif"/>');
        $('#ranks').load('<?= base_url() ?>admin/rankList', function() {
            $('.deleteRank').button({
                icons: {
                    primary: "ui-icon-trash"
                },
                text: false
            });
            $('.deleteRank').click(function() {
                var rankID = $('.ui-button-text',this).html();
                if (window.confirm("Delete rank?")) {
                    $.post('<?= base_url() ?>admin/deleteRank',{rid: rankID}, function() {
                        loadRanks();
                    });
                }
            });
        });
    }
    
    function submit() {
        if($('#rankName').is(':visible')) {
            rankName = $('#rankName').val();
            $.post('<?= base_url() ?>admin/saveRank',{rn: rankName}, function() {
                loadRanks();
            });
            $('#rankName').animate({width:'0'},'fast');
            $('#rankName').fadeOut('fast');
        } else {
            $('#rankName').show();
            $('#rankName').animate({width:'165'},'fast');
            $('#rankName').focus();
        }
    }
    
    $(function() {
        $('#addRank').button({
            icons: {
                primary: "ui-icon-circle-plus"
            }       
        });
        $('#addRank').click(submit);
        $('#rankName').keypress(function(e) {
            if(e.which == 13) {
                submit();
                return false;
            }
        });
        $('#rankName').css('width','0');
        $('#rankName').hide();
        
        loadRanks();
    });
   
</script>
<h3><img src="img/act-dot.png" /> Ranks</h3>
<div id="ranks"></div>
<input type="text" name="rankName" value="" id="rankName"/>
<button id="addRank">Add new rank</button>