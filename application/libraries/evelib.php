<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EVELib {

    function getImagePath($typeID, $size=64) {
        if ($size <= 64) {
            return 'http://image.eveonline.com/InventoryType/' . $typeID . '_' . $size . '.png';
        } else {
            return 'http://image.eveonline.com/Render/' . $typeID . '_' . $size . '.png';
        }
    }

    function getCharacterImage($charID, $size=64) {
        return 'http://image.eveonline.com/Character/' . $charID . '_' . $size . '.jpg';
    }

    function getCorporationImage($charID, $size=64) {
        return 'http://image.eveonline.com/Corporation/' . $charID . '_' . $size . '.png';
    }

    function linkCharacter($id) {
        return '<span class="igblink" onclick="CCPEVE.showInfo(1377,' . $id . ');">';
    }

    function linkCorporation($id) {
        return '<span class="igblink" onclick="CCPEVE.showInfo(2,' . $id . ');">';
    }

    function linkMarketDetails($id) {
        return '<span class="igblink" onclick="CCPEVE.showMarketDetails(' . $id . ');">';
    }
    
    function linkShowInfo($id) {
        return '<span class="igblink" onclick="CCPEVE.showInfo(' . $id . ');">';
    }

    function getIcon($iconFileName) {
        return 'badgeIcons/' . $iconFileName;
    }

}

?>