<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth {

    function smallContent() {
        $CI = & get_instance();
        $CI->load->helper('form');
        $CI->load->library('form_validation');
        $CI->load->model('usermodel');
        $data = $CI->session->all_userdata();
        if (isset($data['userID']) && isset($data['characterID']) && $data['characterID'] > 0) {
            if ($CI->session->userdata('admin') == 1) {
                $data['char'] = $CI->usermodel->getCharacterAdmin($data['characterID']);
            } else {
                $data['char'] = $CI->usermodel->getCharacter($data['userID'], $data['characterID']);
            }
            $data['updatePossible'] = ($data['char']->cachedUntil <= (time()-3600));
        }
        $data['igb'] = FALSE;
        if ($CI->input->server('HTTP_EVE_SHIPTYPENAME') != FALSE) {
            $data['igb'] = TRUE;
            $data['shipTypeName'] = $CI->input->server('HTTP_EVE_SHIPTYPENAME');
            $data['shipTypeID'] = $CI->input->server('HTTP_EVE_SHIPTYPEID');

            $CI->load->model('badgemodel');
            $data['badgeID'] = $CI->badgemodel->getBadgeForShip($data['shipTypeID']);
        }

        $CI->output->enable_profiler($CI->config->item('profiling'));
        return $CI->load->view('auth/usersmallcontent', $data, TRUE);
    }

    function login($username, $password) {
        $CI = & get_instance();

        $username = $CI->security->xss_clean($username);
        $password = $CI->security->xss_clean($password);


        $CI->load->model('usermodel');
        $query = $CI->usermodel->getLoginMatch($username, $password);

        if ($query->num_rows == 1) {
            $userRow = $query->row();
            $charRow = $CI->usermodel->getCharacter($userRow->userID, $userRow->lastCharacter);
            $data = array();
            $data['userID'] = $userRow->userID;
            $data['username'] = $userRow->username;
            $data['admin'] = $userRow->admin;
            $data['mainCharacter'] = $userRow->mainCharacter;
            if ($charRow != false) {
                $data['characterID'] = $userRow->lastCharacter;
                $data['characterName'] = $charRow->characterName;
                $data['race'] = $charRow->race;
	            $data['corporationID'] = $charRow->corporationID;
	            $data['allianceID'] = $charRow->allianceID;
            }
            $CI->session->set_userdata($data);

            $CI->usermodel->updateLogin($userRow->userID);

            return true;
        }
        return false;
    }

    function changeCharacter($characterID) {
        $CI = & get_instance();
        $userID = $CI->session->userdata('userID');

        $CI->load->model('usermodel');
        if ($characterID != 0) {
            if ($CI->session->userdata('admin') == 1) {
                $c = $CI->usermodel->getCharacterAdmin($characterID);
                $CI->usermodel->updateLastCharacter($userID, $characterID);
            } else {
                $c = $CI->usermodel->getCharacter($userID, $characterID);
                $CI->usermodel->updateLastCharacter($userID, $characterID);
            }

            $data = $CI->session->all_userdata();
            $data = array(
                'characterID' => $characterID,
                'characterName' => $c->characterName,
                'race' => $c->race,
            );
            $CI->session->set_userdata($data);
        } else {
            $CI->usermodel->updateLastCharacter($userID, $characterID);
            $data = $CI->session->all_userdata();
            $data['characterID'] = 0;
            $data['race'] = '';
            $data['characterName'] = '';
            $CI->session->set_userdata($data);
        }

        return TRUE;
    }

    function logout() {
        $CI = & get_instance();
        $CI->session->sess_destroy();
    }

    function signup($username, $password, $keyID, $vCode, $characterID, $accessMask, $expires, $type, $characters) {
        $CI = & get_instance();

        $CI->load->model('usermodel');
        $CI->load->model('apimodel');
        $CI->load->model('badgemodel');

        $CI->db->trans_start();

        $CI->usermodel->addUser($username, $password, $characterID);

	    $aUsers = $CI->usermodel->getUsersWithName($username);
        $userID = $aUsers[0]->userID;
        $CI->usermodel->addKey($userID, $keyID, $vCode, $accessMask, $expires, $type, time() + 3600);

        foreach ($characters as $c) {
            $CI->usermodel->addRawCharacter($keyID, $c->characterID);
            $CI->apimodel->updateCharacter($keyID, $vCode->vCode, $c->characterID);
            $CI->badgemodel->calculateAllBadges($c->characterID);
            $CI->usermodel->updateCharacterSummary($c->characterID);
            /**
             * Clear Cache after Update for each User
             */
            $CI->cache->delete_group($c->characterID);
        }

        $CI->db->trans_complete();

        if($this->login($username, $password))
        {
            $CI->load->library('pushover');
            $CI->pushover->setMessage('New User: '.$username.' just registered at '.date('Y/m/d H:i:s'));
            $CI->pushover->setTitle('New User registered: '.$username);
            $CI->pushover->setOption('priority', 0);
            $CI->pushover->sendMessage();
        }
    }

}

?>