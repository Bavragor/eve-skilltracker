<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller {

    public function loginSubmit() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        if ($this->_loginSubmitValidation() === FALSE) {
            echo validation_errors();
        } else {
            echo '1';
        }
    }

    private function _loginSubmitValidation() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_authenticate');
        $this->form_validation->set_message('authenticate', 'Invalid login. Please try again.');
        return $this->form_validation->run();
    }

    public function authenticate() {
        return $this->auth->login($this->input->post('username'), $this->input->post('password'));
    }

    public function logout() {
        $this->auth->logout();
        redirect($this->agent->referrer());
    }

    public function smallContent() {
        echo $this->auth->smallContent();
    }

    public function signup() {
        $this->load->model('bannermodel');
        $this->load->helper('form');

        $data2['title'] = "Sign Up";
        $data2['mainContent'] = $this->load->view('users/signup', null, TRUE);
        $data2['smallContent'] = $this->auth->smallContent();
        $data2['navigation'] = $this->load->view('banner/banner', $this->bannermodel->getRandomBanner(), TRUE);
        $this->load->view('home', $data2);
    }

    public function signupCharacters() {
        $keyID = $this->input->post('keyID', TRUE);
        $vCode = $this->input->post('vCode', TRUE);

        $return = $this->_checkKey($keyID, $vCode);

        if (isset($return->error)) {
            $data['error'] = $return->error;
            $this->load->view('users/signuperror', $data);
        } else {
            $data['characters'] = $return->result->characters;
            $this->load->view('users/signupcharacters', $data);
        }
    }

    function _checkKey($keyID, $vCode) {
        $returnValue = new stdClass();
        if (!is_numeric($keyID)) {
            $returnValue->error = 'KeyID has to be numeric';
        } else {
            try {
                $this->load->model('apimodel');
                $result = $this->apimodel->getAPIKeyInfo($keyID, $vCode);
                if (($result->accessMask && 131080) != 131080) {
                    $returnValue->error = 'AccessMask incorrect. (' . $result->accessMask . ')';
                }
                if ($result->type == 'Corporation') {
                    $returnValue->error = "Don't use a corporation key.";
                }
                $returnValue->result = $result;
            } catch (Pheal\Exceptions\APIException $ex) {
                $returnValue->error = 'API-Error ' . $ex->code . ': ' . $ex->getMessage() . ' <br>CachedUntil: ' . $ex->cached_until;
            } catch (Pheal\Exceptions\PhealException $ex) {
                $returnValue->error = 'PhealException: ' . get_class($ex) . ' Error: ' . $ex->getMessage();
            } catch (Exception $ex) {
                $returnValue->error = 'Exception: ' . get_class($ex) . ' Error: ' . $ex->getMessage();
            }
        }

        return $returnValue;
    }

    function signupSubmit() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        if ($this->_signupSubmitValidation() === FALSE) {
            $this->signup();
            return;
        } else {
            $username = $this->input->post('signupUsername', TRUE);
            $password = $this->input->post('signupPassword', TRUE);
            $keyID = $this->input->post('keyID', TRUE);
            $vCode = $this->input->post('vCode', TRUE);
            $character = $this->input->post('character', TRUE);

            $return = $this->_checkKey($keyID, $vCode);

            if (isset($return->error)) {
                $this->session->set_flashdata('error', $return->error);
                $this->signup();
                return;
            } else {
                $this->auth->signup($username, $password, $keyID, $vCode, $character, $return->result->accessMask, $return->result->expires, $return->result->type, $return->result->characters);
                redirect('overview/success');
            }
        }
    }

    private function _signupSubmitValidation() {
        $this->form_validation->set_rules('signupUsername', 'Username', 'trim|required|callback_unique|max_length[50]|xss_clean');
        $this->form_validation->set_message('unique', 'Username not available.');
        $this->form_validation->set_rules('signupPassword', 'Password', 'trim|required');
        $this->form_validation->set_rules('signupPasswordConfirm', 'Password Confirmation', 'trim|required|matches[signupPassword]');
        $this->form_validation->set_rules('keyID', 'KeyID', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('vCode', 'vCode', 'trim|required|alpha_numeric');
        $this->form_validation->set_rules('character', 'Character', 'trim|required');


        return $this->form_validation->run();
    }

    public function unique($username) {
        $sql = "SELECT COUNT(*) AS c FROM st_users WHERE username = ?";
        $count = $this->db->query($sql, $username)->row()->c;
        return $count == 0;
    }

    public function deleteAccount() {
        $userID = $this->session->userdata('userID');
        $this->load->model('usermodel');
        $this->usermodel->deleteAccount($userID);
        redirect('users/logout');
    }

    public function settings() {
        if ($this->session->userdata('userID') == 0) {
            redirect('content/info');
        }
        $this->load->model('usermodel');
        $this->load->model('apimodel');

        $userID = $this->session->userdata('userID');
        $keys = $this->usermodel->getKeys($userID);

        foreach ($keys as &$k) {
            $k->characters = $this->usermodel->getCharactersForKey($k->keyID);
        }
        $data['userID'] = $userID;
        $data['keys'] = $keys;

        $data['characters'] = $this->usermodel->getCharacters($userID);

        $data2['title'] = "Settings";
        $data2['mainContent'] = $this->load->view('users/settings', $data, TRUE);
        $data2['smallContent'] = $this->auth->smallContent();
        $data2['navigation'] = $this->load->view('users/settingsmenu', NULL, TRUE);
        $this->load->view('home', $data2);
    }

    public function addKey() {
        $this->load->model('usermodel');
        $this->load->model('apimodel');
        $this->load->model('badgemodel');
        $keyID = $this->input->post('keyID', TRUE);
        $vCode = $this->input->post('vCode', TRUE);



        $userID = $this->session->userdata('userID');
        $return = $this->_checkKey($keyID, $vCode);

        if (isset($return->error)) {
            echo $return->error;
            return;
        } else {

            $this->db->trans_start();
            $this->usermodel->addKey($userID, $keyID, $vCode, $return->result->accessMask, $return->result->expires, $return->result->type, time() + 3600);
            foreach ($return->result->characters as $c) {
                $this->usermodel->addRawCharacter($keyID, $c->characterID);
                $this->apimodel->updateCharacter($keyID, $vCode, $c->characterID);
                $this->badgemodel->calculateAllBadges($c->characterID);
                $this->usermodel->updateCharacterSummary($c->characterID);
                /**
                 * Clear Cache after Update for each User
                 */
                $this->cache->delete_group($c->characterID);
            }
        }

        $this->db->trans_complete();
        echo 1;
    }

    public function deleteKey() {
        $keyID = $this->input->post('keyID', TRUE);
        if (is_numeric($keyID)) {
            $this->load->model('usermodel');
            $this->usermodel->deleteKey($keyID);
            $this->auth->changeCharacter(0, '', FALSE);
            echo 1;
        }
    }

    public function changeCharacter($characterID, $target ='', $redirect = TRUE) {
        $this->auth->changeCharacter($characterID);
        if ($redirect) {
            if ($target != '') {
                redirect($target);
            } else {
                redirect($this->agent->referrer());
            }
        }
    }

    public function characterSelection() {
        $userID = $this->session->userdata('userID');
        $this->load->model('usermodel');

        $data['characters'] = $this->usermodel->getCharacters($userID);
        $this->load->view('users/characterSelection', $data);
    }

    public function changePassword() {
        $oldPassword = $this->input->post('oldPassword', TRUE);
        $newPassword = $this->input->post('newPassword', TRUE);
        $newPassword2 = $this->input->post('newPassword2', TRUE);
        $userID = $this->session->userdata('userID');


        $sql = "SELECT SHA1(?) AS hash, password FROM st_users WHERE userID = ?";
        $query = $this->db->query($sql, array($oldPassword, $userID));
        $hashes = $query->row();
        if ($newPassword == $newPassword2) {
            if ($hashes->password == $hashes->hash) {
                $sql = "UPDATE st_users SET password = SHA1(?) WHERE userID = ?";
                $this->db->query($sql, array($newPassword, $userID));
                echo 1;
            } else {
                echo 'Old password incorrect';
            }
        } else {
            echo 'Passwords not identical.';
        }
    }

    public function setMainCharacter() {
        $this->load->model('usermodel');
        $userID = $this->session->userdata('userID');
        $characterID = $this->input->post('characterID');

        if ($characterID > 0) {
            $count = $this->usermodel->setMainCharacter($userID, $characterID);
            $this->session->set_userdata('mainCharacter', $characterID);
            echo $count;
        }
    }

    /**
     * @deprecated
     * @see Usermodel::setUserPrivacy
     */
    public function setCharacterAnonymous() {
        $this->load->model('usermodel');
        $characterID = $this->input->post('characterID', TRUE);

        echo $this->usermodel->setCharacterAnonymous($characterID);
    }

    public function setUserPrivacy() {
        $this->load->model('usermodel');
        $characterID = $this->input->post('characterID', TRUE);
        $sPrivacyType = $this->input->post('sPrivacyType', TRUE);

        echo $this->usermodel->setUserPrivacy($characterID, $sPrivacyType);
    }

}

?>