<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {
    
    public function index() {
        $data['mainContent'] = $this->load->view('welcome',null,TRUE);
        $data['smallContent'] = $this->auth->smallContent();
        $this->load->view('home', $data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */