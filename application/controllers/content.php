<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Badges
 * @author Kevin Mauel <kevin.mauel2@gmail.com>
 */
class Content extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index()
    {
	    if ($this->session->userdata('admin') == 0) {
		    $this->session->set_userdata('prev_url', current_url());
		    redirect('content/info');
	    }

        $this->load->model('contentmodel');
	    $this->load->model('bannermodel');

        $data['mAllPages'] = $mAllPages = $this->contentmodel->getAllPages();

        $data3[] = '';

        $data2['title'] = 'All Content Pages';

	    if($this->cache->get('mainContent-content'))
	    {
		    $data2['mainContent'] = $this->cache->get('mainContent-content');
	    }
	    else
	    {
		    $this->cache->write($this->load->view('content/contentview', $data, TRUE), 'mainContent-content');
		    $data2['mainContent'] = $this->cache->get('mainContent-content');
	    }

        $data2['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
	    $data2['navigation'] = $this->load->view('admin/submenu', null, TRUE);

        //$this->output->enable_profiler($this->config->item('profiling'));

        $this->load->view('home', $data2);
    }

    public function show($sContentName)
    {


        $this->load->model('contentmodel');
	    $this->load->model('bannermodel');

	    $sContentID = $this->contentmodel->getPageID($sContentName);

        $data['oPage'] = $oPage = $this->contentmodel->getPage($sContentID);

        $data3[] = '';

        $data2['title'] = $oPage->contentName;

	    if($this->cache->get('mainContent-'.$data2['title']))
	    {
		    $data2['mainContent'] = $this->cache->get('mainContent-'.$data2['title']);
	    }
	    else
	    {
		    $this->cache->write($this->load->view('content/singlecontentview', $data, TRUE), 'mainContent-'.$data2['title']);
		    $data2['mainContent'] = $this->cache->get('mainContent-'.$data2['title']);
	    }

        if(!$oPage->longversion)
        {
            $data2['smallContent'] = $this->auth->smallContent();
            $data2['navigation'] = $this->load->view('banner/banner', $this->bannermodel->getRandomBanner(), TRUE);
        }

        //$this->output->enable_profiler($this->config->item('profiling'));

        $this->load->view('home', $data2);
    }

    public function create()
    {
	    if ($this->session->userdata('admin') == 0) {
		    $this->session->set_userdata('prev_url', current_url());
		    redirect('content/info');
	    }

        $this->load->model('contentmodel');

        $sContentID = $this->contentmodel->createPage();

        if($sContentID)
        {
            redirect('/content/edit/'.$sContentID.'');
        }
        else
        {
            redirect('/content');
        }
    }

    public function delete($sContentID)
    {
	    if ($this->session->userdata('admin') == 0) {
		    $this->session->set_userdata('prev_url', current_url());
		    redirect('content/info');
	    }

        $this->load->model('contentmodel');

        $this->contentmodel->deletePage($sContentID);

        redirect('/content');
    }

    public function edit($sContentID)
    {
	    if ($this->session->userdata('admin') == 0) {
		    $this->session->set_userdata('prev_url', current_url());
		    redirect('content/info');
	    }

        $this->load->model('contentmodel');
        $this->load->helper('form');
	    $this->load->model('bannermodel');

        $data['oPage'] = $oPage = $this->contentmodel->getPage($sContentID);

        $data3[] = '';

        $data2['title'] = $oPage->contentName;

        $data2['mainContent'] = $this->load->view('content/editcontentview', $data, TRUE);
        $data2['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
	    $data2['navigation'] = $this->load->view('admin/submenu', null, TRUE);

        //$this->output->enable_profiler($this->config->item('profiling'));

        $this->load->view('home', $data2);
    }

    public function savePage()
    {
	    if ($this->session->userdata('admin') == 0) {
		    $this->session->set_userdata('prev_url', current_url());
		    redirect('content/info');
	    }

        $this->load->model('contentmodel');

        $iContentID = $this->input->post('contentID',TRUE);
        $sContentName = $this->input->post('contentName',TRUE);
        $sContentLongtext = $this->input->post('contentLongtext',TRUE);

        $this->contentmodel->editPage($iContentID, $sContentName, $sContentLongtext);

        redirect('content/edit/'.$iContentID.'');
    }
}