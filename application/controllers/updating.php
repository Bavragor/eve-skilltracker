<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class SkilltrackerException extends Exception {
    
}

class Updating extends CI_Controller {

    function character($characterID) {
        $this->load->model('apimodel');
        $this->load->model('badgemodel');
        $this->load->model('usermodel');
        $this->apimodel->saveSkills($characterID);
        $this->badgemodel->calculateAllBadges($characterID);
        $this->apimodel->skillInTraining($characterID);
        $this->usermodel->updateCharacterSummary($characterID);
//echo '<br/>SkillInTraining updated.';
    }

    function key() {
        $keyID = $this->input->post('keyid');
        $this->load->model('apimodel');
        $this->load->model('usermodel');
        $this->load->model('badgemodel');

        $key = $this->usermodel->getKey($keyID);
        $this->db->trans_start();
        $error = '';
        $apiKeyResult;
        try {
            $apiKeyResult = $this->apimodel->getAPIKeyInfo($keyID, $key->vCode);
            if (($apiKeyResult->accessMask && 131080) != 131080) {
                throw new SkilltrackerException('AccessMask incorrect. (' . $result->accessMask . ')');
            }
            if ($apiKeyResult->type == 'Corporation') {
                throw new SkilltrackerException("Don't use a corporation key.");
            }
            $this->usermodel->updateKey($keyID, $apiKeyResult->accessMask, $apiKeyResult->expires, $apiKeyResult->type, time() + 3600, 1, '');
            foreach ($apiKeyResult->characters as $c) {
                $this->usermodel->addRawCharacter($keyID, $c->characterID);
                $this->apimodel->updateCharacter($keyID, $key->vCode, $c->characterID);
                $this->badgemodel->calculateAllBadges($c->characterID);
                $this->usermodel->updateCharacterSummary($c->characterID);
                /**
                 * Clear Cache after Update for each User
                 */
                $this->cache->delete_group($c->characterID);
            }
        } catch (Pheal\Exceptions\APIException $ex) {
            $error = 'API-Error ' . $ex->code . ': ' . $ex->getMessage() . ' <br>CachedUntil: ' . $ex->cached_until;
            $this->usermodel->updateKey($keyID, $key->accessMask, $key->expires, $key->type, time() + 3600, 0, $error);
        } catch (Pheal\Exceptions\PhealException $ex) {
            $error = 'PhealException: ' . get_class($ex) . ' Error: ' . $ex->getMessage();
            $this->usermodel->updateKey($keyID, $key->accessMask, $key->expires, $key->type, time() + 3600, 1, $error);
        } catch (SkilltrackerException $ex) {
            $error = 'Key invalid: ' . $ex->getMessage();
            $this->usermodel->updateKey($keyID, $apiKeyResult->accessMask, $apiKeyResult->expires, $apiKeyResult->type, time() + 3600, 0, $error);
        } catch (Exception $ex) {
            $error = 'Error: ' . $ex->getMessage();
            $this->usermodel->updateKey($keyID, $key->accessMask, $key->expires, $key->type, time() + 3600, 1, $error);
        }
        $this->db->trans_complete();
        echo ($error != '') ? $error : 1;
    }

}

?>