<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Info extends CI_Controller {

    public function index() {
        $this->load->model('bannermodel');
        $data['mainContent'] = $this->load->view('info/about', null, TRUE);
        $data['navigation'] = $this->load->view('banner/banner', $this->bannermodel->getRandomBanner(), TRUE);
        $data['smallContent'] = $this->auth->smallContent();
        $this->load->view('home', $data);
    }

    public function manual() {
        $data['mainContent'] = $this->load->view('info/manual', null, TRUE);
        $data['smallContent'] = $this->auth->smallContent();
        $data['navigation'] = $this->load->view('info/manualmenu', null, TRUE);
        $this->load->view('home', $data);
    }

    public function contact() {
        $this->load->model('bannermodel');
        $data['mainContent'] = $this->load->view('info/contact', null, TRUE);
        $data['smallContent'] = $this->auth->smallContent();
        $data['navigation'] = $this->load->view('banner/banner', $this->bannermodel->getRandomBanner(), TRUE);
        $this->load->view('home', $data);
    }

    public function donate() {
        $this->load->model('bannermodel');
        $data['mainContent'] = $this->load->view('info/donate', null, TRUE);
        $data['smallContent'] = $this->auth->smallContent();
        $data['navigation'] = $this->load->view('banner/banner', $this->bannermodel->getRandomBanner(), TRUE);
        $this->load->view('home', $data);
    }
    
    public function offline() {
        $data['mainContent'] = $this->load->view('info/offline', null, TRUE);
        $data['smallContent'] = '';
        $data['navigation'] = '';
        $this->load->view('home', $data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */