<?php

class Ajax_SkillInfo extends CI_Controller
{
    public function show($sTypeID)
    {
        $this->load->model('evemodel');

        $data['oSkillInfo'] = $this->evemodel->getSkillInfo($sTypeID);

        return $this->load->view('badges/ajax_skillinfoview', $data);
    }

}