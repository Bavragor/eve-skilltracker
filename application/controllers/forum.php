<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Forum extends CI_Controller {

    function index() {
        $this->load->model('forummodel');
        $this->load->model('bannermodel');
        $this->load->helper('form');
        
        $topics = $this->forummodel->getTopics();
		foreach($topics as &$t) {
			$t->last = $this->forummodel->getLastPost($t->topicID);
		}
        $data['topics'] = $topics;
		
        $data2['title'] = "Forum";
        $data2['mainContent'] = $this->load->view('forum/forumview', $data, TRUE);
	$data2['navigation'] = $this->load->view('banner/banner', $this->bannermodel->getRandomBanner(), TRUE);
        $data2['smallContent'] = $this->auth->smallContent();
        $this->load->view('home', $data2);
    }
    function topic($topicID) {
        $this->load->model('forummodel');
        $this->load->model('bannermodel');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $data['topic'] = $this->forummodel->getTopic($topicID);
        $data['comments'] = $this->forummodel->getComments($topicID);
        
        $data2['title'] = "Forum";
        $data2['mainContent'] = $this->load->view('forum/forumtopicview', $data, TRUE);
	$data2['navigation'] = $this->load->view('banner/banner', $this->bannermodel->getRandomBanner(), TRUE);
        $data2['smallContent'] = $this->auth->smallContent();
        $this->load->view('home', $data2);
    }
    
    function addTopic() {
        $this->load->model('forummodel');
        
        $userID = $this->session->userdata('userID');
        $topic = $this->input->post('topic',TRUE);
        $text = strip_tags($this->input->post('text',TRUE));
        
        $topicID = $this->forummodel->addTopic($userID,$topic);
	    if($topicID)
	    {
		    if(!$this->session->userdata('admin'))
		    {
			    $this->load->library('pushover');
			    $this->pushover->setMessage('New Topic added: '.$topic.' at '.date('Y/m/d H:i:s'));
			    $this->pushover->setTitle('New Topic: '.$topic);
			    $this->pushover->setOption('priority', 0);
			    $this->pushover->sendMessage();
		    }
	    }
        $this->forummodel->addComment($userID, $topicID, $text);
        
        redirect('forum');
    }
    
    function stickyTopic() {
        if($this->session->userdata('admin') != 1) {
            echo 0;
            return;
        } else {
            $topicID = $this->input->post('topicID');
            $this->load->model('forummodel');
            echo $this->forummodel->stickyTopic($topicID);
            return;
        }
    }
    function addComment() {
        $this->load->model('forummodel');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('text', 'Text', 'required');
        
        $userID = $this->session->userdata('userID');
        $topicID = $this->input->post('topicID',TRUE);
        $sTopicName = $this->forummodel->getTopicName($topicID);
        $text = strip_tags($this->input->post('text',TRUE));

        if ($this->form_validation->run() == FALSE)
        {
            //redirect('forum/topic/' . $topicID);
            $this->topic($topicID);
            return false;
        }
        
        if($this->forummodel->addComment($userID,$topicID, $text))
        {
            if(!$this->session->userdata('admin'))
            {
                $this->load->library('pushover');
                $this->pushover->setMessage('New Comment: '.substr($text, 0 , 200).' in Topic '.$sTopicName.' at '.date('Y/m/d H:i:s'));
                $this->pushover->setTitle('New Comment in Topic: '.$sTopicName);
                $this->pushover->setOption('priority', 0);
                $this->pushover->sendMessage();
            }
        }

        redirect('forum/topic/' . $topicID);
    }
	public function addCommentForBadge()
	{
		$this->load->model('forummodel');
        $this->load->model('badgemodel');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('text', 'Text', 'required');

		$userID = $this->session->userdata('userID');
		$badgeID = $this->input->post('badgeID',TRUE);
		$text = strip_tags($this->input->post('text',TRUE));
        $sBadgeName = $this->badgemodel->getBadgeName($badgeID);
        if ($this->form_validation->run() == FALSE)
        {
            /*$this->load->controller('badges');
            $this->badges->show($badgeID);
            return false;*/
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('badges/' . strtolower($sBadgeName));
        }

		if($this->forummodel->addCommentForBadge($userID,$badgeID, $text))
        {
            if(!$this->session->userdata('admin'))
            {
                $this->load->library('pushover');
                $this->pushover->setMessage('New Comment: '.substr($text, 0 , 200).' in Badge '.$sBadgeName.' at '.date('Y/m/d H:i:s'));
                $this->pushover->setTitle('New Comment in Badge: '.$sBadgeName);
                $this->pushover->setOption('priority', 0);
                $this->pushover->sendMessage();
            }
        }

		redirect('badges/' . strtolower($sBadgeName));
	}
    function deleteTopic() {
         if($this->session->userdata('admin') != 1) {
            echo 0;
            return;
        } else {
            $topicID = $this->input->post('topicID');
            $this->load->model('forummodel');
            echo $this->forummodel->deleteTopic($topicID);
            return;
        }
    }
    function deleteComment() {
         if($this->session->userdata('admin') != 1) {
            echo 0;
            return;
        } else {
            $commentID = $this->input->post('commentID');
            $this->load->model('forummodel');
            echo $this->forummodel->deleteComment($commentID);
            return;
        }
    }

}

?>