<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Badges extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model('badgemodel');
        $this->load->model('rankmodel');


        $characterID = $this->session->userdata('characterID');

	    if($this->cache->get('badges-badgesData-'.$characterID))
	    {
		    $data['badges'] = $this->cache->get('badges-badgesData-'.$characterID);
	    }
	    else
	    {
	        // Load ranks (Basic, Medium, Expert)
	        $ranks = $this->rankmodel->getRanks();

	        if ($characterID > 0) {
	            $badges = $this->badgemodel->getBadgesForUser($characterID);
	            foreach ($badges as $badge) {
	                if ($badge->badgeID > 0) {


	                    // Declare variables
	                    $lastMaxSP = 0;
	                    foreach ($ranks as $rank) {
	                        // Get maximum SP possible for this badge+rank
	                        $r = new stdClass();

	                        $r->rankID = $rank->rankID;
	                        $r->maxSP = $this->badgemodel->getMaximumSkillpoints($badge->badgeID, $rank->rankID);

	                        $r->lastMaxSP = $lastMaxSP;

	                        $badge->ranks[$r->rankID] = $r;

	                        $lastMaxSP = $r->maxSP;
	                    } // end foreach ranks
	                }
	            }
	            $data['characterID'] = $characterID;
	        } else {
	            $badges = $this->badgemodel->getBadges();
	            $data3 = null;
	        }


		    $this->cache->write($badges, 'badges-badgesData-'.$characterID);
		    $data['badges'] = $this->cache->get('badges-badgesData-'.$characterID);
	    }
        $data['characterID'] = $characterID;
        $data2['title'] = 'Badges';

        $data2['mainContent'] = $this->load->view('badges/badgesview', $data, TRUE);


        $data2['smallContent'] = $this->auth->smallContent();

        if($characterID)
        {
            if($this->cache->get('badges-badgesForUser-'.$characterID))
            {
                $data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
            }
            else
            {
                $this->cache->write($this->badgemodel->getCharacterBadgeOverview($characterID), 'badges-badgesForUser-'.$characterID);
                $data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
            }
        }
        $data2['navigation'] = $this->load->view('badges/badgemenu', $data3, TRUE);

        $this->load->view('home', $data2);
    }

    public function show($sBadgeName) {

        // Load models
        $this->load->model('badgemodel');
	    $this->load->model('forummodel');
        $this->load->model('rankmodel');
	    $this->load->helper('form');
        $this->load->library('form_validation');

        $badgeID = $this->badgemodel->getBadgeID($sBadgeName);

        $characterID = $this->session->userdata('characterID');

	    if($this->cache->get('badges-'.$sBadgeName.'BadgeForUser-'.$characterID) && $this->cache->get('badges-'.$sBadgeName.'RankForUser-'.$characterID))
	    {
			$data['badge'] = $this->cache->get('badges-'.$sBadgeName.'BadgeForUser-'.$characterID);
		    $data['ranks'] = $this->cache->get('badges-'.$sBadgeName.'RankForUser-'.$characterID);
	    }
		else
		{
	        // Load ranks (Basic, Medium, Expert)
	        $ranks = $this->rankmodel->getRanks();

	        if ($characterID > 0) {
	            $badge = $this->badgemodel->getBadgeForUser($badgeID, $characterID);
	            $data['characterID'] = $characterID;
	            // Declare variables
	            $lastMaxSP = 0;

	            if ($badge->maxSP > 0) {
	                $badge->percent = $badge->skillpoints / $badge->maxSP * 100;
	            } else {
	                $badge->percent = 0;
	            }
	        } else {

	            $badge = $this->badgemodel->getBadge($badgeID);
	        }
	        $lastMaxSP = 0;
	        foreach ($ranks as &$rank) {
	            $r = $this->calulateRank($badgeID, $rank->rankID, $characterID, $lastMaxSP);
	            $r->rankName = $rank->rankName;
	            $rank = $r;
	        } // end foreach ranks

		    if(!$badge->badgeName)
		    {
			    $this->session->set_flashdata('ApiUpdate', 'We added an new Badge, so your API Update is needed');
		    }

			$this->cache->write($badge, 'badges-'.$sBadgeName.'BadgeForUser-'.$characterID);
			$this->cache->write($ranks, 'badges-'.$sBadgeName.'RankForUser-'.$characterID);

			$data['ranks'] = $this->cache->get('badges-'.$sBadgeName.'RankForUser-'.$characterID);
			$data['badge'] = $this->cache->get('badges-'.$sBadgeName.'BadgeForUser-'.$characterID);

		}

	    if($this->cache->get('badges-'.$sBadgeName))
	    {
			$data['traits'] = $this->cache->get('badges-'.$sBadgeName);
	    }
	    else
	    {
			$this->cache->write($this->badgemodel->getTraitsForBadge($data['badge']->typeID), 'badges-'.$sBadgeName);
		    $data['traits'] = $this->cache->get('badges-'.$sBadgeName);
	    }

        $data2['title'] = $data['badge']->badgeName;

	    $comment_data['comments'] = $this->forummodel->getCommentsByBadge($data['badge']->badgeID);
	    $comment_data['badge'] = $data['badge'];
	    $comment_data['blFromBadge'] = true;
	    $data['comments'] = $this->load->view('forum/forumtopicview', $comment_data, TRUE);

        $data2['mainContent'] = $this->load->view('badges/badgeview', $data, TRUE);

        $data2['smallContent'] = $this->auth->smallContent();

        if($characterID)
        {
            if($this->cache->get('badges-badgesForUser-'.$characterID))
            {
                $data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
            }
            else
            {
                $this->cache->write($this->badgemodel->getCharacterBadgeOverview($characterID), 'badges-badgesForUser-'.$characterID);
                $data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
            }
        }
        $data2['navigation'] = $this->load->view('badges/badgemenu', $data3, TRUE);

        $this->load->view('home', $data2);

    }

    public function calulateRank($badgeID, $rankID, $characterID = 0, &$lastMaxSP = 0) {
        $rank = new stdClass();
        $rank->rankID = $rankID;
        if ($characterID > 0) {
            // Get maximum SP possible for this badge+rank
            $rank->maxSP = $this->badgemodel->getMaximumSkillpoints($badgeID, $rank->rankID);


            // Get all skills plus skills of current character
            $rank->skills = $this->badgemodel->getSkillsForUserWithSkillpoints($badgeID, $rank->rankID, $characterID);
            // Get all subbadges plus current progression
            $rank->subbadges = $this->badgemodel->getSubBadgesForUser($badgeID, $rank->rankID, $characterID);

            $rank->lastMaxSP = $lastMaxSP;
            $lastMaxSP = $rank->maxSP;
        } else {
            // Get all skills
            $rank->skills = $this->badgemodel->getSkills($badgeID, $rank->rankID);
            // Get all subbadges 
            $rank->subbadges = $this->badgemodel->getSubBadges($badgeID, $rank->rankID);
        }
        $rankData['r'] = $rank;
        $rankData['characterID'] = $characterID;
        $rank->view = $this->load->view('badges/rankview', $rankData, TRUE);
        return $rank;
    }

    public function showRank($s) {
        $this->load->model('badgemodel');
        $characterID = $this->session->userdata('characterID');
        $parts = explode('-', $s);

        echo $this->calulateRank($parts[0], $parts[1], $characterID)->view;
    }

    public function calculateAllBadges($characterID) {
        // Load models
        $this->load->model('badgemodel');

        $this->badgemodel->calculateAllBadges();

        $this->load->view('home');
    }

    public function evemon($badgeID, $rankID) {
        $this->load->model('badgemodel');
        if ($rankID < 1 || $rankID > 3) {
            $sBadgeName = strtolower($this->badgemodel->getBadgeName($badgeID));
            redirect('badges/'.$sBadgeName);
        } else {
            $characterID = $this->session->userdata('characterID');
            if ($characterID != 0) {
                $data['name'] = $this->session->userdata('characterName') . ' - ' . $this->badgemodel->getBadgeTitle($badgeID, $rankID);
                $data['skills'] = $this->badgemodel->getEvemonPlan($badgeID, $rankID, $characterID);
            } else {
                $data['name'] = $this->badgemodel->getBadgeTitle($badgeID, $rankID);
                $data['skills'] = $this->badgemodel->getEvemonPlan($badgeID, $rankID);
            }
            $this->load->view('badges/evemonplan', $data);
        }
    }

    public function rewrite($badgeID)
    {
        $this->load->model('badgemodel');

        $sBadgeName = $this->badgemodel->getBadgeName($badgeID);

        redirect('badges/' . $sBadgeName);
    }

	public function nobadges($blAsAjax = 1)
	{
		$this->load->model('badgemodel');
		$this->load->model('rankmodel');


		$characterID = $this->session->userdata('characterID');

		if($this->cache->get('badges-nobadgesBadgesData-'.$characterID))
		{
			$data['badges'] = $this->cache->get('badges-nobadgesBadgesData-'.$characterID);
		}
		else
		{
			// Load ranks (Basic, Medium, Expert)
			$ranks = $this->rankmodel->getRanks();

			if ($characterID > 0) {
				$badges = $this->badgemodel->getNoBadgesForUser($characterID);
				foreach ($badges as $badge) {
					if ($badge->badgeID > 0) {


						// Declare variables
						$lastMaxSP = 0;
						foreach ($ranks as $rank) {
							// Get maximum SP possible for this badge+rank
							$r = new stdClass();

							$r->rankID = $rank->rankID;
							$r->maxSP = $this->badgemodel->getMaximumSkillpoints($badge->badgeID, $rank->rankID);

							$r->lastMaxSP = $lastMaxSP;

							$badge->ranks[$r->rankID] = $r;

							$lastMaxSP = $r->maxSP;
						} // end foreach ranks
					}
				}
				$data['characterID'] = $characterID;
			} else {
				$badges = $this->badgemodel->getBadges();
				$data3 = null;
			}


			$this->cache->write($badges, 'badges-nobadgesBadgesData-'.$characterID);
			$data['badges'] = $this->cache->get('badges-nobadgesBadgesData-'.$characterID);
		}
		$data['characterID'] = $characterID;
		$data['badgeRankType'] = 0;
		$data2['title'] = 'Badges';

		$data['blAsAjax'] = $blAsAjax;

		$data2['mainContent'] = $this->load->view('badges/badgesview', $data, TRUE);

		if($data['blAsAjax'])
		{
			echo $data2['mainContent'];
			exit;
		}

		$data2['smallContent'] = $this->auth->smallContent();

		if($this->cache->get('badges-badgesForUser-'.$characterID))
		{
			$data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
		}
		else
		{
			$this->cache->write($this->badgemodel->getCharacterBadgeOverview($characterID), 'badges-badgesForUser-'.$characterID);
			$data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
		}

		$data2['navigation'] = $this->load->view('badges/badgemenu', $data3, TRUE);

		$this->load->view('home', $data2);
	}

	public function basic($blAsAjax = 0)
	{
		$this->load->model('badgemodel');
		$this->load->model('rankmodel');


		$characterID = $this->session->userdata('characterID');

		if($this->cache->get('badges-basicBadgesData-'.$characterID))
		{
			$data['badges'] = $this->cache->get('badges-basicBadgesData-'.$characterID);
		}
		else
		{
			// Load ranks (Basic, Medium, Expert)
			$ranks = $this->rankmodel->getRanks();

			if ($characterID > 0) {
				$badges = $this->badgemodel->getBasicBadgesForUser($characterID);
				foreach ($badges as $badge) {
					if ($badge->badgeID > 0) {


						// Declare variables
						$lastMaxSP = 0;
						foreach ($ranks as $rank) {
							// Get maximum SP possible for this badge+rank
							$r = new stdClass();

							$r->rankID = $rank->rankID;
							$r->maxSP = $this->badgemodel->getMaximumSkillpoints($badge->badgeID, $rank->rankID);

							$r->lastMaxSP = $lastMaxSP;

							$badge->ranks[$r->rankID] = $r;

							$lastMaxSP = $r->maxSP;
						} // end foreach ranks
					}
				}
				$data['characterID'] = $characterID;
			} else {
				$badges = $this->badgemodel->getBadges();
				$data3 = null;
			}


			$this->cache->write($badges, 'badges-basicBadgesData-'.$characterID);
			$data['badges'] = $this->cache->get('badges-basicBadgesData-'.$characterID);
		}
		$data['characterID'] = $characterID;
		$data['badgeRankType'] = 1;
		$data2['title'] = 'Badges';

        $data['blAsAjax'] = $blAsAjax;

        $data2['mainContent'] = $this->load->view('badges/badgesview', $data, TRUE);

        if($data['blAsAjax'])
        {
            echo $data2['mainContent'];
            exit;
        }

		$data2['smallContent'] = $this->auth->smallContent();

		if($this->cache->get('badges-badgesForUser-'.$characterID))
		{
			$data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
		}
		else
		{
			$this->cache->write($this->badgemodel->getCharacterBadgeOverview($characterID), 'badges-badgesForUser-'.$characterID);
			$data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
		}

		$data2['navigation'] = $this->load->view('badges/badgemenu', $data3, TRUE);

		$this->load->view('home', $data2);
	}

	public function medium($blAsAjax = 0)
	{
		$this->load->model('badgemodel');
		$this->load->model('rankmodel');


		$characterID = $this->session->userdata('characterID');

		if($this->cache->get('badges-mediumBadgesData-'.$characterID))
		{
			$data['badges'] = $this->cache->get('badges-mediumBadgesData-'.$characterID);
		}
		else
		{
			// Load ranks (Basic, Medium, Expert)
			$ranks = $this->rankmodel->getRanks();

			if ($characterID > 0) {
				$badges = $this->badgemodel->getMediumBadgesForUser($characterID);
				foreach ($badges as $badge) {
					if ($badge->badgeID > 0) {


						// Declare variables
						$lastMaxSP = 0;
						foreach ($ranks as $rank) {
							// Get maximum SP possible for this badge+rank
							$r = new stdClass();

							$r->rankID = $rank->rankID;
							$r->maxSP = $this->badgemodel->getMaximumSkillpoints($badge->badgeID, $rank->rankID);

							$r->lastMaxSP = $lastMaxSP;

							$badge->ranks[$r->rankID] = $r;

							$lastMaxSP = $r->maxSP;
						} // end foreach ranks
					}
				}
				$data['characterID'] = $characterID;
			} else {
				$badges = $this->badgemodel->getBadges();
				$data3 = null;
			}


			$this->cache->write($badges, 'badges-mediumBadgesData-'.$characterID);
			$data['badges'] = $this->cache->get('badges-mediumBadgesData-'.$characterID);
		}
		$data['characterID'] = $characterID;
		$data['badgeRankType'] = 2;
		$data2['title'] = 'Badges';

        $data['blAsAjax'] = $blAsAjax;

        $data2['mainContent'] = $this->load->view('badges/badgesview', $data, TRUE);

        if($data['blAsAjax'])
        {
            echo $data2['mainContent'];
            exit;
        }

		$data2['smallContent'] = $this->auth->smallContent();

		if($this->cache->get('badges-badgesForUser-'.$characterID))
		{
			$data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
		}
		else
		{
			$this->cache->write($this->badgemodel->getCharacterBadgeOverview($characterID), 'badges-badgesForUser-'.$characterID);
			$data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
		}

		$data2['navigation'] = $this->load->view('badges/badgemenu', $data3, TRUE);

		$this->load->view('home', $data2);
	}

	public function expert($blAsAjax = 0)
	{
		$this->load->model('badgemodel');
		$this->load->model('rankmodel');


		$characterID = $this->session->userdata('characterID');

		if($this->cache->get('badges-expertBadgesData-'.$characterID))
		{
			$data['badges'] = $this->cache->get('badges-expertBadgesData-'.$characterID);
		}
		else
		{
			// Load ranks (Basic, Medium, Expert)
			$ranks = $this->rankmodel->getRanks();

			if ($characterID > 0) {
				$badges = $this->badgemodel->getExpertBadgesForUser($characterID);
				foreach ($badges as $badge) {
					if ($badge->badgeID > 0) {


						// Declare variables
						$lastMaxSP = 0;
						foreach ($ranks as $rank) {
							// Get maximum SP possible for this badge+rank
							$r = new stdClass();

							$r->rankID = $rank->rankID;
							$r->maxSP = $this->badgemodel->getMaximumSkillpoints($badge->badgeID, $rank->rankID);

							$r->lastMaxSP = $lastMaxSP;

							$badge->ranks[$r->rankID] = $r;

							$lastMaxSP = $r->maxSP;
						} // end foreach ranks
					}
				}
				$data['characterID'] = $characterID;
			} else {
				$badges = $this->badgemodel->getBadges();
				$data3 = null;
			}


			$this->cache->write($badges, 'badges-expertBadgesData-'.$characterID);
			$data['badges'] = $this->cache->get('badges-expertBadgesData-'.$characterID);
		}
		$data['characterID'] = $characterID;
		$data['badgeRankType'] = 3;
		$data2['title'] = 'Badges';

        $data['blAsAjax'] = $blAsAjax;

		$data2['mainContent'] = $this->load->view('badges/badgesview', $data, TRUE);

        if($data['blAsAjax'])
        {
            echo $data2['mainContent'];
            exit;
        }

		$data2['smallContent'] = $this->auth->smallContent();

		if($this->cache->get('badges-badgesForUser-'.$characterID))
		{
			$data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
		}
		else
		{
			$this->cache->write($this->badgemodel->getCharacterBadgeOverview($characterID), 'badges-badgesForUser-'.$characterID);
			$data3['badgeOverview'] = $this->cache->get('badges-badgesForUser-'.$characterID);
		}

		$data2['navigation'] = $this->load->view('badges/badgemenu', $data3, TRUE);

		$this->load->view('home', $data2);
	}
}

?>