<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Overview extends CI_Controller {

    function index() {
        if ($this->session->userdata('userID') == 0 || $this->session->userdata('characterID') == 0) {
            redirect('content/info');
        }
        $this->load->model('badgemodel');
        $this->load->model('usermodel');

        $characterID = $this->session->userdata('characterID');

        $data = $this->session->all_userdata();

	    if($this->cache->get('overview-'.$characterID))
	    {
		    $data += $this->cache->get('overview-'.$characterID);
	    }
	    else
	    {
	        if ($this->session->userdata('admin') == 1) {
	            $data['char'] = $this->usermodel->getCharacterAdmin($data['characterID']);
	        } else {
	            $data['char'] = $this->usermodel->getCharacter($data['userID'], $data['characterID']);
	        }

	        $sum = $this->usermodel->getCharacterSummary($characterID);
	        $rank = new stdClass();
	        foreach ($sum as $key => $value) {
	            $rank->$key = $this->usermodel->getCharacterRank($key, $value);
	        }
            $dataTmp['rank'] = $data['char'];
	        $dataTmp['rank'] = $rank;
	        $dataTmp['sum'] = $sum;

	        $dataTmp['total'] = $this->usermodel->getCharacterCount();

		    $this->cache->write($dataTmp, 'overview-'.$characterID);

		    $data += $this->cache->get('overview-'.$characterID);
	    }


        $data2['title'] = 'Overview';
        $data2['mainContent'] = $this->load->view('overview/overview', $data, TRUE);
        $data2['navigation'] = $this->load->view('overview/logo', null, TRUE);
        $data2['smallContent'] = $this->auth->smallContent();
        $this->load->view('home', $data2);
    }

    public function success()
    {
        if ($this->session->userdata('userID') == 0 || $this->session->userdata('characterID') == 0) {
            redirect('content/info');
        }
        $this->load->model('badgemodel');
        $this->load->model('usermodel');

        $characterID = $this->session->userdata('characterID');

        $data = $this->session->all_userdata();

        if($this->cache->get('overview-'.$characterID))
        {
            $data += $this->cache->get('overview-'.$characterID);
        }
        else
        {
            if ($this->session->userdata('admin') == 1) {
                $data['char'] = $this->usermodel->getCharacterAdmin($data['characterID']);
            } else {
                $data['char'] = $this->usermodel->getCharacter($data['userID'], $data['characterID']);
            }

            $sum = $this->usermodel->getCharacterSummary($characterID);
            $rank = new stdClass();
            foreach ($sum as $key => $value) {
                $rank->$key = $this->usermodel->getCharacterRank($key, $value);
            }
            $dataTmp['rank'] = $rank;
            $dataTmp['sum'] = $sum;

            $dataTmp['total'] = $this->usermodel->getCharacterCount();

            $this->cache->write($dataTmp, 'overview-'.$characterID);

            $data += $this->cache->get('overview-'.$characterID);
        }


        $data2['title'] = 'Overview';
        $data2['mainContent'] = $this->load->view('overview/overview', $data, TRUE);
        $data2['navigation'] = $this->load->view('overview/logo', null, TRUE);
        $data2['smallContent'] = $this->auth->smallContent();
        $this->load->view('home', $data2);
    }

    function skills($sParamCharacterName) {
        $this->load->model('bannermodel');
        if ($this->session->userdata('userID') == 0) {
            redirect('content/info');
        }
        $this->load->model('badgemodel');
        $this->load->model('usermodel');

	    if(!$sParamCharacterName)
	    {
            $characterID = $this->session->userdata('characterID');
		    $data = $this->session->all_userdata();
		    $data['skills'] = $this->usermodel->getSkills($characterID);
		    $data3['badgeOverview'] = $this->badgemodel->getCharacterBadgeOverview($characterID);
		    $data['totalBadges'] = $this->load->view('badges/badgemenu', $data3, TRUE);
		    $data['totalSkillPoints'] = $this->usermodel->getTotalSkillpoints($characterID);
		    $data['totalSkills'] = $this->usermodel->getSkillCount($characterID);
            $data['sRestriction'] = $this->session->flashdata('sRestriction');
	    }
	    else
	    {
		    $characterID = $this->usermodel->getCharacterID($sParamCharacterName);
            if(!isAllowed2View($characterID))
            {
                $this->session->set_flashdata('sRestriction', 'User '.ucwords(urldecode($sParamCharacterName)).' has not allowed to see his/her skills!<br /><br />');
                redirect('overview/skills');
            }
		    $data = $this->usermodel->getCharacterAdmin($characterID);
		    $data->characterID = $characterID;
		    $data->skills = $this->usermodel->getSkills($characterID);
		    $data3['badgeOverview'] = $this->badgemodel->getCharacterBadgeOverview($characterID);
		    $data->totalBadges = $this->load->view('badges/badgemenu', $data3, TRUE);
		    $data->totalSkillPoints = $this->usermodel->getTotalSkillpoints($characterID);
		    $data->totalSkills = $this->usermodel->getSkillCount($characterID);
	    }

        $data2['title'] = 'Skills';
	    if($sParamCharacterName)
	    {
		    $data2['mainContent'] = $this->load->view('overview/foreign_skills', $data, TRUE);
	    }
	    else
	    {
		    $data2['mainContent'] = $this->load->view('overview/foreign_skills', $data, TRUE);
	    }

	    $data2['navigation'] = $this->load->view('banner/banner', $this->bannermodel->getRandomBanner(), TRUE);
        $data2['smallContent'] = $this->auth->smallContent();
        $this->load->view('home', $data2);
    }

    function r($field) {
        $this->load->model('bannermodel');
        $fields = array('basicBadges' => 'Basic Badge Ranking', 'mediumBadges' => 'Medium Badge Ranking', 'expertBadges' => 'Expert Badge Ranking',
            'skillCount' => 'Total Skill Ranking', 'skillpoints' => 'Skillpoint Ranking', 'skillFiveCount' => 'Level V Skill Ranking',
            'badgeRankPoints' => 'Skilltracker Badge Rank', 'skillRankPoints' => 'Skilltracker Skill Ranking', 'skilltrackerPoints' => 'EVE Skilltracker Rank');

        if (array_key_exists($field, $fields)) {
            $this->load->model('usermodel');
            $data['table'] = $this->usermodel->getRankTable($field);

            $title = $fields[$field];

            $data['characterID'] = $this->session->userdata('characterID');
            $data['title'] = $title;
            $data['field'] = $field;

            $data2['title'] = $title . ' :: Ranking List';
            $data2['mainContent'] = $this->load->view('overview/rankinglist', $data, TRUE);
            $data2['navigation'] = $this->load->view('banner/banner', $this->bannermodel->getRandomBanner(), TRUE);
            $data2['smallContent'] = $this->auth->smallContent();
            $this->load->view('home', $data2);
        } else {
            echo 'Not a valid field.';
        }
    }

}

?>