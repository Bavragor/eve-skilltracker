<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('admin') == 0) {
            $this->session->set_userdata('prev_url', current_url());
            redirect('content/info');
        }
    }

    public function index() {
        $data['title'] = 'Admin';
        $data['mainContent'] = $this->load->view('admin/start', null, TRUE);
        $data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
        $this->load->view('home', $data);
    }

    public function ranksAndCategories() {
        $data['title'] = 'Admin';
        $data['mainContent'] = $this->load->view('admin/ranksview', null, TRUE);
        $data['mainContent'] .= $this->load->view('admin/categoriesview', null, TRUE);
        $data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
        $this->load->view('home', $data);
    }

    public function rankList() {
        $this->load->model('rankmodel');
        $data['ranks'] = $this->rankmodel->getRanks();
        $this->load->view('admin/rankslistview', $data);
    }

    public function saveRank() {
        $this->load->model('rankmodel');
        $rn = $this->input->post('rn');
        $data['success'] = $this->rankmodel->saveRank($rn);
    }

    public function deleteRank() {
        $this->load->model('rankmodel');
        $rid = $this->input->post('rid');
        $data['success'] = $this->rankmodel->deleteRank($rid);
    }

    public function categoryList() {
        $this->load->model('categorymodel');
        $data['categories'] = $this->categorymodel->getCategories();
        $this->load->view('admin/categorieslistview', $data);
    }

    public function saveCategory() {
        $this->load->model('categorymodel');
        $cn = $this->input->post('cn');
        $params = explode('-', $this->input->post('params'));
        $data['success'] = $this->categorymodel->saveCategory($cn, $params[1], $params[2]);
    }

    public function deleteCategory() {
        $this->load->model('categorymodel');
        $params = explode('-', $this->input->post('params'));
        $data['success'] = $this->categorymodel->deleteCategory($params[0], $params[1], $params[2]);
    }

    public function editCategoryName() {
        $this->load->model('categorymodel');
        $categoryID = $this->input->post('categoryID');
        $categoryName = $this->input->post('categoryName');
        echo $this->categorymodel->editCategoryName($categoryID, $categoryName);
    }

    public function moveCategory() {
        $this->load->model('categorymodel');

        $params = explode('-', $this->input->post('params'));
        $targetLft = $this->input->post('targetLft');

        echo $this->categorymodel->moveCategory($params[1], $params[2], $targetLft);
    }

    public function badgeList() {
        $this->load->model('badgemodel');
        $data['badges'] = $this->badgemodel->getBadges();
        $this->load->view('admin/badgeslistview', $data);
    }

    public function badges() {
        $data['title'] = 'Admin';
        $data['mainContent'] = $this->load->view('admin/badgeview', null, TRUE);
        $data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
        $this->load->view('home', $data);
    }

    public function saveBadge() {
        $this->load->model('badgemodel');
        $bn = $this->input->post('bn');
        $ci = $this->input->post('ci');
        echo $data['success'] = $this->badgemodel->saveBadge($bn, $ci);
    }

    public function deleteBadge() {
        $this->load->model('badgemodel');
        $params = $this->input->post('params');
        $data['success'] = $this->badgemodel->deleteBadge($params);
    }

    public function editBadgeName() {
        $this->load->model('badgemodel');
        $badgeID = $this->input->post('badgeID');
        $badgeName = $this->input->post('badgeName');
        echo $this->badgemodel->editBadgeName($badgeID, $badgeName);
    }

    public function editSortName() {
        $this->load->model('badgemodel');
        $badgeID = $this->input->post('badgeID');
        $sortName = $this->input->post('sortName');
        echo $this->badgemodel->editSortName($badgeID, $sortName);
    }

    public function editBadgeCategory() {
        $this->load->model('badgemodel');
        $badgeID = $this->input->post('badgeID');
        $categoryID = $this->input->post('categoryID');
        echo $this->badgemodel->editBadgeCategory($badgeID, $categoryID);
    }

    public function editBadge($badgeID) {
        $this->load->model('badgemodel');
        $this->load->model('evemodel');
        $this->load->model('categorymodel');

        $data2['b'] = $this->badgemodel->getBadge($badgeID);
        $data2['ships'] = $this->evemodel->ships();
        $data2['categories'] = $this->categorymodel->getCategories();
        $data['title'] = 'Admin';
        $data['mainContent'] = $this->load->view('admin/badgeeditview', $data2, TRUE);
        $data['smallContent'] = $this->load->view('admin/adminsmallcontent_d', null, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
        $this->load->view('home', $data);
    }

    public function typeID() {
        $this->load->model('evemodel');
        echo $this->evemodel->typeID($this->input->post('typeName'));
    }

    public function skillsList($badgeID) {
        $this->load->model('badgemodel');
        $this->load->model('rankmodel');
        $ranks = $this->rankmodel->getRanks();

        $data = array();
        foreach ($ranks as $r) {
            $data['badgeID'] = $badgeID;
            $data['rank'] = $r;
            $data['skills'] = $this->badgemodel->getSkills($badgeID, $r->rankID);
            $this->load->view('admin/skillslistview', $data);
        }
    }

    public function saveSkill() {
        $this->load->model('badgemodel');
        $tid = $this->input->post('tid');
        $lvl = $this->input->post('lvl');
        $params = explode('-', $this->input->post('params'));
        echo $this->input->post('or');
        echo $or = ($this->input->post('or') === FALSE) ? 1 : 0;
        echo $this->badgemodel->saveSkill($params[0], $params[1], $tid, $lvl, $or);
    }

    public function deleteSkill() {
        $this->load->model('badgemodel');
        $params = explode('-', $this->input->post('params'));
        $data['success'] = $this->badgemodel->deleteSkill($params[0], $params[1], $params[2]);
    }

    public function subBadgeList($badgeID) {
        $this->load->model('badgemodel');
        $this->load->model('rankmodel');
        $ranks = $this->rankmodel->getRanks();
        $badges = $this->badgemodel->getBadgeList();
        $data = array();
        foreach ($ranks as $r) {
            $data['badgeID'] = $badgeID;
            $data['rank'] = $r;
            $data['subBadges'] = $this->badgemodel->getSubBadges($badgeID, $r->rankID);
            $data['ranks'] = $ranks;
            $data['badges'] = $badges;
            $this->load->view('admin/subbadgelistview', $data);
        }
    }

    public function saveSubBadge() {
        $this->load->model('badgemodel');
        $bid = $this->input->post('bid');
        $rank = $this->input->post('rank');
        $params = explode('-', $this->input->post('params'));
        echo $this->input->post('or');
        echo $or = ($this->input->post('or') === FALSE) ? 1 : 0;
        echo $this->badgemodel->saveSubBadge($params[0], $params[1], $bid, $rank, $or);
    }

    public function deleteSubBadge() {
        $this->load->model('badgemodel');
        $params = explode('-', $this->input->post('params'));
        $data['success'] = $this->badgemodel->deleteSubBadge($params[0], $params[1], $params[2]);
    }

    public function iconChange($badgeID) {
        $this->load->helper('file');
        $this->load->model('badgemodel');

        $icons = get_filenames('badgeIcons/');

        sort($icons);

        $data['icons'] = $icons;
        $data['b'] = $this->badgemodel->getBadge($badgeID);

        $data2['title'] = 'Admin - Icons';
        $data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
        $data2['mainContent'] = $this->load->view('admin/iconchange', $data, TRUE);
        $this->load->view('home', $data2);
    }

    public function setIcon() {
        $this->load->model('badgemodel');
        $badgeID = $this->input->post('badgeID');
        $filename = $this->input->post('filename');

        echo $this->badgemodel->setIcon($badgeID, $filename);
    }

    public function setShipTypeID() {
        $this->load->model('badgemodel');
        $badgeID = $this->input->post('badgeID');
        $typeID = $this->input->post('typeID');

        echo $this->badgemodel->setShipTypeID($badgeID, $typeID);
    }

    public function autoRelateShips() {
        $this->load->model('badgemodel');
        echo $this->badgemodel->autoRelateShips() . ' records updated.';
    }

    public function buildMaximumSkillpointTable() {
        $this->load->model('badgemodel');
        $i = $this->badgemodel->buildMaximumSkillpointTable();
        $data['mainContent'] = "<h1>&#8801; Calculate Max-SP-Table </h1><b>Success!</b><br> Maximum skillpoints for $i badges and ranks updated.";
        $data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);

        $this->load->view('home.php', $data);
    }

	function buildCharacterCorps()
	{
		$this->load->model('corpmodel');
		if($this->corpmodel->buildCharacterCorps())
		{
			echo 'hat geklappt';
		}
		else
		{
			echo 'hat nicht geklappt';
		}
	}

    public function test() {
        $this->load->model('badgemodel');
        $this->badgemodel->test();
    }

    public function deleteAccount($userID) {
        $this->load->model('usermodel');
        $this->usermodel->deleteAccount($userID);
        redirect('admin/users');
    }

    public function resetPassword($userID) {
        $this->load->model('usermodel');
        $this->usermodel->resetPassword($userID);
        redirect('admin/user/' . $userID);
    }

    public function users($page = 1,$field = 'cr',$order = 'd') {
        $this->load->model('usermodel');
        $this->load->helper('form');
        
        $pageSize = 30;

        $userCount = $this->usermodel->getUserCount();

        $data2['pageCount'] = ceil($userCount / $pageSize);
        $data2['previous'] = ($page > 1);
        $data2['next'] = ($page < $data2['pageCount']);
        $data2['page'] = $page;
        $data2['field'] = $field;
        $data2['order'] = $order;

        $data3['users'] = $this->usermodel->getUsers($pageSize * ($page - 1), $pageSize,$field,$order);
        $data3['page'] = $page;
        $data3['field'] = $field;
        $data2['userList'] = $this->load->view('admin/userlist',$data3,TRUE);
        $data['title'] = 'Admin';
        $data['mainContent'] = $this->load->view('admin/users', $data2, TRUE);
        $data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
        $this->load->view('home', $data);
    }
    
    public function usersWithName() {
        $this->load->model('usermodel');
        
        $username = $this->input->post('username',TRUE);

        $data3['users'] = $this->usermodel->getUsersWithName($username);
        $data2['userList'] = $this->load->view('admin/userlist',$data3,TRUE);
        $data2['field'] = 'name';
        $data2['value'] = $username;
        $data['title'] = 'Admin';
        $data['mainContent'] = $this->load->view('admin/userssearchresult', $data2, TRUE);
        $data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
        $this->load->view('home', $data);
    }
    
    public function usersWithCharacter() {
        $this->load->model('usermodel');
        
        $characterName = $this->input->post('characterName',TRUE);

        $data3['users'] = $this->usermodel->getUsersWithCharacter($characterName);
        $data2['userList'] = $this->load->view('admin/userlist',$data3,TRUE);
        $data2['field'] = 'character';
        $data2['value'] = $characterName;
        $data['title'] = 'Admin';
        $data['mainContent'] = $this->load->view('admin/userssearchresult', $data2, TRUE);
        $data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
        $this->load->view('home', $data);
    }

    public function user($userID) {
        $this->load->model('usermodel');

        $data2 = $this->usermodel->getUser($userID);
        $data2['characters'] = $this->usermodel->getCharacters($userID);

        $data['title'] = 'Admin';
        $data['mainContent'] = $this->load->view('admin/singleuser', $data2, TRUE);
        $data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
        $this->load->view('home', $data);
    }
    
    function banners() {
        $this->load->model('bannermodel');
        $this->load->helper('form');
        $data2['banners'] = $this->bannermodel->getBanners();
        $data['title'] = "Admin - Bannermanagement";
        $data['mainContent'] = $this->load->view('banner/banneradminview', $data2, TRUE);
        $data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
        $this->load->view('home', $data);
    }

	/**
	 * Dienst zur Austeuerung des Views, welcher die letzten Kommentare anzeigt
	 * @author Kevin Mauel <kevin.mauel2@gmail.com>
	 */
	public function lastComments()
	{
		$this->load->model('forummodel');
		$this->load->model('badgemodel');

		$data2['aLastComments'] = $this->forummodel->getLastComments(14);

		$data['title'] = 'Last Comments';

        $data2['title'] = $data['title'];

		$data['mainContent'] = $this->load->view('admin/lastcomments', $data2, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
		$data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
		$this->load->view('home', $data);
	}

	/**
	 * Dienst zur Austeuerung des Views, welcher die aktuellen User die online sind und die, die in den vergangenen 24 Stunden online waren
	 * @author Kevin Mauel <kevin.mauel2@gmail.com>
	 */
	public function usersOnline()
	{
		$this->load->model('usermodel');
		$this->load->helper('user');

		$data2['aCurUsersOnline'] = $this->usermodel->getCurrentUsersOnline();
		$data2['aLastUsersOnline'] = $this->usermodel->getLastUsersOnline();

		$data['title'] = 'Users Online';

		$data2['title'] = $data['title'];

		$data['mainContent'] = $this->load->view('admin/usersonline', $data2, TRUE);
        $data['navigation'] = $this->load->view('admin/submenu', null, TRUE);
		$data['smallContent'] = $this->load->view('admin/adminsmallcontent', null, TRUE);
		$this->load->view('home', $data);
	}
    
    function addBanner() {
        $this->load->model('bannermodel');
        
        $name = $this->input->post('name', TRUE);
        $filename = $this->input->post('filename');
        $link = $this->input->post('link');
        $text = $this->input->post('text', TRUE);
        
        $this->bannermodel->addBanner($name,$filename,$link,$text);
        redirect('admin/banners');
    }
    
    function deleteBanner() {
        $this->load->model('bannermodel');
        
        
        $bannerID = $this->input->post('bannerID');
        
        echo $this->bannermodel->deleteBanner($bannerID);
    }
    function activateBanner() {
        $this->load->model('bannermodel');
        
        $bannerID = $this->input->post('bannerID');
        echo $this->bannermodel->activateBanner($bannerID);
    }

    public function clearBadgeCache()
    {
        $this->cache->delete_group('badges-');

        redirect('admin');
    }

    public function clearContentCache()
    {
        $this->cache->delete_group('mainContent');

        redirect('admin');
    }

    public function clearAllCache()
    {
        $this->cache->delete_all();

        redirect('admin');
    }
   

}

?>