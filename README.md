The EVE Skilltracker is an web based tool for the game EVE Online, that allows you to see your ability to fly ships and manage fittings.

This tool in general is designed to be a guide with skilling for new and inexperienced pilots. EVE online is so incredibly complex that we want to help you here with finding the adequate Skills for your actual ship.

This website is not oriented to some weird fitting variations or any FOTM PvP-fleetfitting. We designed the badges considering the given attributes and bonuses of the specific ships.

While EFT and EVEMon only show what skills are required as a minimum to be able to fly the ships or use the fittings, this website goes a step further and lists the skills you need stepwise to fly a ship from basic over medium to expert.